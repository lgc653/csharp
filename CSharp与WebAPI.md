# CSharp与WebAPI

## 什么是WebAPI

WebAPI 是一种用来开发系统间接口、设备接口 API 的技术，基于 Http 协议，请求和返 回格式结果默认是 json 格式。比 WCF 更简单、更通用，比 WebService 更节省流量、更简洁。

WebAPI 是开发接口的技术，用户不会直接和 WebAPI 打交道，因此 WebAPI 也不会生成 界面，这是 WebAPI 和普通 ASP.Net MVC 的区别。

虽然完全使用普通 ASP.Net MVC 甚至 HttpHandler 也可以开发这样的接口，但是 WebAPI 是专门做这个的，更专业。

### 特点

因为 ASP.Net WebAPI 专注于接口开发，所有有如下特点：

1. webapi 的 Action 方法返回 值直接返回对象，专注于数据。
2. webapi 更符合 Restful 的风格。
3. 有利于独立于 IIS 部署 （selfhost、winform、windows service、控制台）。
4. Action 可以直接声明为 async。

### 总结

就是给前端提供数据的框架，不管其他任何事情没不管界面，业务，逻辑，有Restful格式的数据提供方式

* Post（增）提交数据

* Get（查）得到数据

* Put（改）推送数据

* Delete（删）删除数据

> ps：在一些情况下Post也算在增删改里面

## 使用C#创建WebAPI

### 新建项目

![img](CSharp与WebAPI.assets/image2.png)



![](CSharp与WebAPI.assets/image3.png)

### 添加模型

*模型*（Model）是表示应用中数据的对象。ASP.NET Web API 可以自动将您的Model序列化为 JSON、XML 或其他格式，然后将序列化数据写入 HTTP 响应消息的正文中。只要客户端可以读取序列化格式，它就可以对对象进行非序列化。大多数客户可以解析 XML 或 JSON。此外，客户端可以通过在 HTTP 请求消息中设置Accept header来指示其想要的格式。

![img](CSharp与WebAPI.assets/image4.png)

```c#
namespace ProductsApp.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public decimal Price { get; set; }
    }
}
```

### 添加控制器

在 Web API 中，*控制器*是处理 HTTP 请求的对象。我们将添加一个控制器，可以返回产品列表或 ID 指定的单个产品。

> 如果您使用过 ASP.NET MVC，您已经熟悉了控制器。Web API 控制器类似于 MVC 控制器，但继承**ApiController** class 而不是继承了**Controller** class.

在**"解决方案资源管理器"**中，右键单击控制器文件夹。选择**"添加**"，然后选择**Controller**。

![img](CSharp与WebAPI.assets/image5.png)

在**Add Scaffold**对话中，选择 **Web API Controller - Empty**。单击**"添加**"。

![img](CSharp与WebAPI.assets/image6.png)

在 **Add Controller**对话中，将控制器命名为"ProductsController"。单击**"添加**"。

![img](CSharp与WebAPI.assets/image7.png)

Controllers 文件夹中创建一个名为ProductsController.cs的文件。

![img](CSharp与WebAPI.assets/image8.png)

> 注意
>
> 您不需要将控制器放入名为"控制器"的文件夹中。文件夹名称只是组织源文件的便捷方式。

```c#
using ProductsApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace ProductsApp.Controllers
{
    public class ProductsController : ApiController
    {
        Product[] products = new Product[] 
        { 
            new Product { Id = 1, Name = "Tomato Soup", Category = "Groceries", Price = 1 }, 
            new Product { Id = 2, Name = "Yo-yo", Category = "Toys", Price = 3.75M }, 
            new Product { Id = 3, Name = "Hammer", Category = "Hardware", Price = 16.99M } 
        };

        public IEnumerable<Product> GetAllProducts()
        {
            return products;
        }

        public IHttpActionResult GetProduct(int id)
        {
            var product = products.FirstOrDefault((p) => p.Id == id);
            if (product == null)
            {
                return NotFound();
            }
            return Ok(product);
        }
    }
}
```

 controller两种返回products的接口:

- 返回所有的产品 **IEnumerable<Product>** type.`GetAllProducts`
- 根据输入的ID返回指定产品`GetProduct`

| Controller Method | URI                |
| :---------------- | :----------------- |
| GetAllProducts    | /api/products      |
| GetProduct        | /api/products/*id* |

## 序列化

序列化是指将对象转换成字节流，从而存储对象或将对象传输到内存、数据库或文件的过程。 它的主要用途是保存对象的状态，以便能够在需要时重新创建对象。 反向过程称为“反序列化”。

### 序列化的工作原理

下图展示了序列化的整个过程：

![图：序列化](CSharp与WebAPI.assets/serialization-process.gif)

将对象序列化为带有数据的流。 该流还可能包含有关对象类型的信息，例如其版本、区域性和程序集名称。 可以将此流中的对象存储在数据库、文件或内存中。

### 序列化的用途

通过序列化，开发人员可以保存对象的状态，并能在需要时重新创建对象，同时还能存储对象和交换数据。 通过序列化，开发人员可以执行如下操作：

- 使用 Web 服务将对象发送到远程应用程序
- 将对象从一个域传递到另一个域
- 将对象通过防火墙传递为 JSON 或 XML 字符串
- 跨应用程序维护安全或用户特定的信息

### 什么是JSON

Json，全称 JavaScript Object Notation，即 JavaScript 对象标记法，是一种轻量级的数据交换格式。

采用完全独立于编程语言的文本格式来存储和表示数据，不仅容易阅读和编写，而且也容易解析和生成，有效的提升了网络传输效率，在与后端的数据交互中有较为广泛的应用。

通俗的说，Json 就是按照一定规则排列的字符串。

JSON是一种取代XML的数据结构，和xml相比，它更小巧但描述能力却不差,由于它的小巧所以网络传输数据将减少更多流量从而加快速度。

* `{}` 双括号表示对象
* `[]` 中括号表示数组
* `""` 双引号内是属性或值
* `:` 冒号表示后者是前者的值(这个值可以是字符串、数字、也可以是另一个数组或对象)

所以 `{"name": "Michael"}` 可以理解为是一个包含name为Michael的对象

而`[{"name": "Michael"},{"name": "Jerry"}]`就表示包含两个对象的数组

当然了，你也可以使用`{"name":["Michael","Jerry"]}`来简化上面一部,这是一个拥有一个name数组的对象

```json
{
    	"students": [
               {"name": "小王","age": 15}, 
               {"name": "小李","age": 16},
               {"name": "小苏","age": 18}
         ]
}
```

### JSON 序列化

#### Newtonsoft.Json

Newtonsoft.Json，是.Net中开源的Json序列化和反序列化工具，官方地址：http://www.newtonsoft.com/json。

定义相关类

```c#
/// <summary>
/// 学生信息实体
/// </summary>
public class Student
{
    public int ID { get; set; }
    public string Name { get; set; }
    public Class Class { get; set; }
}
/// <summary>
/// 学生班级实体
/// </summary>
public class Class
{
    public int ID { get; set; }
    public string Name { get; set; }
}
```

序列化，反序列化 实体对象，实体集合，匿名对象：

```csharp
Student stu = new Student();
stu.ID = 1;
stu.Name = "张三";
stu.Class = new Class() { ID = 0121, Name = "CS0121" };
//使用方法1
//实体序列化、反序列化
//结果：{"ID":1,"Name":"张三","Class":{"ID":121,"Name":"CS0121"}}
string json1 = JsonConvert.SerializeObject(stu);
Console.WriteLine(json1);
Student stu2 = JsonConvert.DeserializeObject<Student>(json1);
Console.WriteLine(stu2.Name + "---" + stu2.Class.Name);
//实体集合，序列化和反序列化
List<Student> stuList = new List<Student>() { stu, stu2 };
string json2 = JsonConvert.SerializeObject(stuList);
Console.WriteLine(json2);
List<Student> stuList2 = JsonConvert.DeserializeObject<List<Student>>(json2);
foreach (var item in stuList2)
{
    Console.WriteLine(item.Name + "----" + item.Class.Name);
}
//匿名对象的解析,
//匿名独享的类型  obj.GetType().Name： "<>f__AnonymousType0`2"
var obj = new { ID = 2, Name = "李四" };
string json3 = JsonConvert.SerializeObject(obj);
Console.WriteLine(json3);
object obj2 = JsonConvert.DeserializeAnonymousType(json3, obj);
Console.WriteLine(obj2.GetType().GetProperty("ID").GetValue(obj2));
object obj3 = JsonConvert.DeserializeAnonymousType(json3, new { ID = default(int), Name = default(string) });
Console.WriteLine(obj3.GetType().GetProperty("ID").GetValue(obj3));
//匿名对象解析，可以传入现有类型，进行转换
Student stu3 = new Student();
stu3 = JsonConvert.DeserializeAnonymousType(json3, new Student());
Console.WriteLine(stu3.Name);
```

#### System.Text.Json

[System.Text.Json](https://docs.microsoft.com/zh-cn/dotnet/api/system.text.json) 命名空间包含用于 JavaScript 对象表示法 (JSON) 序列化和反序列化的类。 JSON 是一种常用于在 Web 上共享数据的开放标准。

`System.Text.Json` 命名空间提供用于序列化和反序列化 JavaScript 对象表示法 (JSON) 的功能。

- 该库是作为 .NET Core 3.0 及更高版本共享框架的一部分内置的。
- 对于早期版本的框架，请安装 [System.Text.Json](https://www.nuget.org/packages/System.Text.Json) NuGet 包。 包支持以下框架：
  - .NET Standard 2.0 及更高版本
  - .NET Framework 4.7.2 及更高版本
  - .NET Core 2.0、2.1 和 2.2

```c#
jsonString = JsonSerializer.Serialize(Student);
student = JsonSerializer.Deserialize<Student>(jsonString);
```

## WebAPI设计规范

[RESTful](http://www.ruanyifeng.com/blog/2011/09/restful.html) 是目前最流行的 API 设计规范，用于 Web 数据接口的设计。

它的大原则容易把握，但是细节不容易做对。本文总结 RESTful 的设计细节，介绍如何设计出易于理解和使用的 API。

### URL 设计

#### 动词 + 宾语

RESTful 的核心思想就是，客户端发出的数据操作指令都是"动词 + 宾语"的结构。比如，`GET /articles`这个命令，`GET`是动词，`/articles`是宾语。

动词通常就是五种 HTTP 方法，对应 CRUD 操作。

> - GET：读取（Read）
> - POST：新建（Create）
> - PUT：更新（Update）
> - PATCH：更新（Update），通常是部分更新
> - DELETE：删除（Delete）

根据 HTTP 规范，动词一律大写。

#### 动词的覆盖

有些客户端只能使用`GET`和`POST`这两种方法。服务器必须接受`POST`模拟其他三个方法（`PUT`、`PATCH`、`DELETE`）。

这时，客户端发出的 HTTP 请求，要加上`X-HTTP-Method-Override`属性，告诉服务器应该使用哪一个动词，覆盖`POST`方法。

```http
POST /api/Person/4 HTTP/1.1  
X-HTTP-Method-Override: PUT
```

上面代码中，`X-HTTP-Method-Override`指定本次请求的方法是`PUT`，而不是`POST`。

#### 宾语必须是名词

宾语就是 API 的 URL，是 HTTP 动词作用的对象。它应该是名词，不能是动词。比如，`/articles`这个 URL 就是正确的，而下面的 URL 不是名词，所以都是错误的。

> - /getAllCars
> - /createNewCar
> - /deleteAllRedCars

#### 复数 URL

既然 URL 是名词，那么应该使用复数，还是单数？

这没有统一的规定，但是常见的操作是读取一个集合，比如`GET /articles`（读取所有文章），这里明显应该是复数。

为了统一起见，建议都使用复数 URL，比如`GET /articles/2`要好于`GET /article/2`。

#### 避免多级 URL

常见的情况是，资源需要多级分类，因此很容易写出多级的 URL，比如获取某个作者的某一类文章。

```http
GET /authors/12/categories/2
```

这种 URL 不利于扩展，语义也不明确，往往要想一会，才能明白含义。

更好的做法是，除了第一级，其他级别都用查询字符串表达。

```http
GET /authors/12?categories=2
```

下面是另一个例子，查询已发布的文章。你可能会设计成下面的 URL。

```http
GET /articles/published
```

查询字符串的写法明显更好。

```http
GET /articles?published=true
```

### 状态码

#### 状态码必须精确

客户端的每一次请求，服务器都必须给出回应。回应包括 HTTP 状态码和数据两部分。

HTTP 状态码就是一个三位数，分成五个类别。

> - `1xx`：相关信息
> - `2xx`：操作成功
> - `3xx`：重定向
> - `4xx`：客户端错误
> - `5xx`：服务器错误

这五大类总共包含[100多种](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes)状态码，覆盖了绝大部分可能遇到的情况。每一种状态码都有标准的（或者约定的）解释，客户端只需查看状态码，就可以判断出发生了什么情况，所以服务器应该返回尽可能精确的状态码。

API 不需要`1xx`状态码，下面介绍其他四类状态码的精确含义。

#### 2xx 状态码

`200`状态码表示操作成功，但是不同的方法可以返回更精确的状态码。

> - GET: 200 OK
> - POST: 201 Created
> - PUT: 200 OK
> - PATCH: 200 OK
> - DELETE: 204 No Content

上面代码中，`POST`返回`201`状态码，表示生成了新的资源；`DELETE`返回`204`状态码，表示资源已经不存在。

此外，`202 Accepted`状态码表示服务器已经收到请求，但还未进行处理，会在未来再处理，通常用于异步操作。下面是一个例子。

```http
HTTP/1.1 202 Accepted

{
  "task": {
    "href": "/api/company/job-management/jobs/2130040",
    "id": "2130040"
  }
}
```

#### 3xx 状态码

API 用不到`301`状态码（永久重定向）和`302`状态码（暂时重定向，`307`也是这个含义），因为它们可以由应用级别返回，浏览器会直接跳转，API 级别可以不考虑这两种情况。

API 用到的`3xx`状态码，主要是`303 See Other`，表示参考另一个 URL。它与`302`和`307`的含义一样，也是"暂时重定向"，区别在于`302`和`307`用于`GET`请求，而`303`用于`POST`、`PUT`和`DELETE`请求。收到`303`以后，浏览器不会自动跳转，而会让用户自己决定下一步怎么办。下面是一个例子。

```http
HTTP/1.1 303 See Other
Location: /api/orders/12345
```

#### 4xx 状态码

`4xx`状态码表示客户端错误，主要有下面几种。

`400 Bad Request`：服务器不理解客户端的请求，未做任何处理。

`401 Unauthorized`：用户未提供身份验证凭据，或者没有通过身份验证。

`403 Forbidden`：用户通过了身份验证，但是不具有访问资源所需的权限。

`404 Not Found`：所请求的资源不存在，或不可用。

`405 Method Not Allowed`：用户已经通过身份验证，但是所用的 HTTP 方法不在他的权限之内。

`410 Gone`：所请求的资源已从这个地址转移，不再可用。

`415 Unsupported Media Type`：客户端要求的返回格式不支持。比如，API 只能返回 JSON 格式，但是客户端要求返回 XML 格式。

`422 Unprocessable Entity` ：客户端上传的附件无法处理，导致请求失败。

`429 Too Many Requests`：客户端的请求次数超过限额。

#### 5xx 状态码

`5xx`状态码表示服务端错误。一般来说，API 不会向用户透露服务器的详细信息，所以只要两个状态码就够了。

`500 Internal Server Error`：客户端请求有效，服务器处理时发生了意外。

`503 Service Unavailable`：服务器无法处理请求，一般用于网站维护状态。

### 服务器回应

####  不要返回纯本文

API 返回的数据格式，不应该是纯文本，而应该是一个 JSON 对象，因为这样才能返回标准的结构化数据。所以，服务器回应的 HTTP 头的`Content-Type`属性要设为`application/json`。

客户端请求时，也要明确告诉服务器，可以接受 JSON 格式，即请求的 HTTP 头的`ACCEPT`属性也要设成`application/json`。下面是一个例子。

```http
GET /orders/2 HTTP/1.1 
Accept: application/json
```

####  发生错误时，不要返回 200 状态码

有一种不恰当的做法是，即使发生错误，也返回`200`状态码，把错误信息放在数据体里面，就像下面这样。

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "status": "failure",
  "data": {
    "error": "Expected at least two items in list."
  }
}
```

上面代码中，解析数据体以后，才能得知操作失败。

这张做法实际上取消了状态码，这是完全不可取的。正确的做法是，状态码反映发生的错误，具体的错误信息放在数据体里面返回。下面是一个例子。

```http
HTTP/1.1 400 Bad Request
Content-Type: application/json

{
  "error": "Invalid payoad.",
  "detail": {
     "surname": "This field is required."
  }
}
```

####  提供链接

API 的使用者未必知道，URL 是怎么设计的。一个解决方法就是，在回应中，给出相关链接，便于下一步操作。这样的话，用户只要记住一个 URL，就可以发现其他的 URL。这种方法叫做 HATEOAS。

举例来说，GitHub 的 API 都在 [api.github.com](https://api.github.com/) 这个域名。访问它，就可以得到其他 URL。

```http
{
  ...
  "feeds_url": "https://api.github.com/feeds",
  "followers_url": "https://api.github.com/user/followers",
  "following_url": "https://api.github.com/user/following{/target}",
  "gists_url": "https://api.github.com/gists{/gist_id}",
  "hub_url": "https://api.github.com/hub",
  ...
}
```

上面的回应中，挑一个 URL 访问，又可以得到别的 URL。对于用户来说，不需要记住 URL 设计，只要从 api.github.com 一步步查找就可以了。

HATEOAS 的格式没有统一规定，上面例子中，GitHub 将它们与其他属性放在一起。更好的做法应该是，将相关链接与其他属性分开。

```http
HTTP/1.1 200 OK
Content-Type: application/json

{
  "status": "In progress",
   "links": {[
    { "rel":"cancel", "method": "delete", "href":"/api/status/12345" } ,
    { "rel":"edit", "method": "put", "href":"/api/status/12345" }
  ]}
}
```

### 参考链接

- [RESTful API Design: 13 Best Practices to Make Your Users Happy](https://blog.florimondmanca.com/restful-api-design-13-best-practices-to-make-your-users-happy), by Florimond Manca
- [API design](https://docs.microsoft.com/en-us/azure/architecture/best-practices/api-design), by MicroSoft Azure
- [RESTful API 最佳实践](http://www.ruanyifeng.com/blog/2018/10/restful-api-best-practices.html), by  [阮一峰](http://www.ruanyifeng.com/)
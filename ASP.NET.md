![](ASP.NET.assets/幻灯片0.png)

# ASP.NET

ASP.NET是新一代的构建现代应用和云端应用的.NET框架，它是微软针对开源社区全新的开源和跨平台框架，对于Web应用提供了更加模块化，易扩展，具有异步特性的Web框架。

你可以在你的应用中更好地集成MVC ,Entity Framework , SignalR和WebAPI等技术。

ASP.NET是一个重新架构的框架，你可以运行在两个不同的运行时中轻量级的Core CLR和Full CLR中。(简单说说Full CLR就是传统的.NET Framework 4.5.x/.NET Framework 4.6运行时配合传统的桌面/服务器进行运行，而轻量级的Core CLR则通过命令行方式模拟IIS信道快速启动页面)。看看下图：

![ASP.NET](ASP.NET.assets/20150316155231481.png) 

## 新的Web结构

说起ASP.NET , 从1.0 beta开始到现在5.0过来有两个重要的历史阶段：

第一是从WebForm过渡到MVC，第二件事情就是ASP.NET 5.0。

这两次转变微软都是采纳了不少新的理念和实现方式。例如ASP.NET MVC就从当年最流行的Ruby on Rails 转变而来。而现在ASP.NET 5.0就参考了不少NodeJS的优点。微软在这方面也是跟得比较时尚的。

## 如何配置你的ASP.NET环境

由于新的ASP.NET可以在不同平台上开发和运行，意味着你可以选择在不同环境上使用。由于官方的文档比较清晰，我这里就省略N个字了。大家可以看这里  [ASP.NET环境配置](https://github.com/aspnet/home#getting-started) ，你可以随时更新你的运行环境，和了解最新的变化。

### 开发工具选取

Visual Studio是一个最原始的选择，但是这种东西太过笨重。

> Visual Studio Installer 有的电脑安装时会出现以下情况，我的笔记本电脑就是这样，明明很好的网络，却一直显示下载0B和0B/秒，进度条没有动，并且过一会显示网络有问题，重新下载下载器也是这样：
>
> ![](ASP.NET.assets/458152-20180506091848391-954118289.png)
>
> 修改DNS服务器地址，选择使用下面的DNS服务器地址
> **在首选DNS服务器输入：114.114.114.114**
> **在备用DNS服务器输入：8.8.8.8**

![Visual Studio](ASP.NET.assets/20150317155408483.png) 

带 C# 扩展的 Visual Studio Code 提供功能强大的编辑体验，完全支持 C# IntelliSense（智能代码填充）和调试。 

> :fire:Visual Studio Code的配置比较简单，当你一打开C#文件，他就会提示你下载相关的插件。后面就不详细介绍了

![打开 Program.cs 文件](ASP.NET.assets/open-program-cs.png) 

可以用轻量级的Sublime +git方式对代码进行管理。ASP.NET(ASP.NET) 对于Sublime 有很好的插件支持。

![Sublime +git](ASP.NET.assets/20150316164445885.png) 

### Sublime Text 开发ASP.NET环境配置

安装成功后你就可以通过Package Control 去安装ASP.NET的支持，这个包叫做Kulture。安装步骤也不细说了，按照这个链接进去一步步来就是了[（点击打开链接）](https://github.com/ligershark/Kulture)，重启Sublime后，把项目文件夹拉进Sublime你就可以通过Mac上Cmd+Shit+P/Windows上Ctrl+Shit+P，之后选择Run K Command 调用Kulture去编译运行你的ASP.NET项目了。  

![Sublime Text 开发ASP.NET环境配置](ASP.NET.assets/20150317154641460.png)

你会怀念Visual Studio 的智能提示，当然Sublime也会有对应的插件－Omnisharp [(点击打开链接)](http://www.omnisharp.net/) 。好了通过Sublime＋Kulture + Omnisharp 这样就搭载好清亮级的ASP.NET运行环境可以轻轻松松地在Sublime下完成你的工作.

## ASP.NET Core 入门

https://docs.microsoft.com/zh-cn/aspnet/

![Web 应用主页](ASP.NET.assets/home-page.png) 

### 系统必备

[.NET Core 3.0 SDK 或更高版本](https://dotnet.microsoft.com/download/dotnet-core/3.0)

### 创建 Web 应用项目

打开命令行界面，然后输入以下命令：

.NET Core CLI复制

```bash
dotnet new webapp -o aspnetcoreapp
```

上面的命令：

- 创建新 Web 应用。
- `-o aspnetcoreapp` 参数使用应用的源文件创建名为 aspnetcoreapp 的目录。

### 信任开发证书

信任 HTTPS 开发证书：

- [Windows](https://docs.microsoft.com/zh-cn/aspnet/core/getting-started/?view=aspnetcore-3.0&tabs=windows#tabpanel_CeZOj-G++Q_windows)
- [macOS](https://docs.microsoft.com/zh-cn/aspnet/core/getting-started/?view=aspnetcore-3.0&tabs=windows#tabpanel_CeZOj-G++Q_macos)
- [Linux](https://docs.microsoft.com/zh-cn/aspnet/core/getting-started/?view=aspnetcore-3.0&tabs=windows#tabpanel_CeZOj-G++Q_linux)

.NET Core CLI复制

```bash
dotnet dev-certs https --trust
```

以上命令会显示以下对话：

![安全警告对话](ASP.NET.assets/cert.png)

如果你同意信任开发证书，请选择“是”。

有关详细信息，请参阅[信任 ASP.NET Core HTTPS 开发证书](https://docs.microsoft.com/zh-cn/aspnet/core/security/enforcing-ssl?view=aspnetcore-3.0#trust-the-aspnet-core-https-development-certificate-on-windows-and-macos)

### 运行应用

运行以下命令：

.NET Core CLI复制

```bash
cd aspnetcoreapp
dotnet watch run
```

在命令行界面指明应用已启动后，转到 [https://localhost:5001](https://localhost:5001/)。

### 编辑 Razor 页面

打开 Pages/Index.cshtml ，并使用以下突出显示标记修改并保存页面：

CSHTML复制

```html
@page
@model IndexModel
@{
    ViewData["Title"] = "Home page";
}

<div class="text-center">
    <h1 class="display-4">Welcome</h1>
    <p>Hello, world! The time on the server is @DateTime.Now</p>
</div>
```

浏览到 [https://localhost:5001](https://localhost:5001/)，刷新页面并验证更改是否显示。

## 使用 ASP.NET Core 创建 Web API

 ASP.NET Core 支持使用 C# 创建 RESTful 服务，也称为 Web API。 若要处理请求，Web API 使用控制器。 Web API 中的*控制器*是派生自 `ControllerBase` 的类。 

https://docs.microsoft.com/zh-cn/aspnet/core/web-api/

```bash
dotnet new webapi -o webapi
cd webapi && dotnet run 
```

`Controllers\WeatherForecastController.cs`

```csharp
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace webapi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }
    }
}
```

> :zap:Web API路径的定义：
>
> ```csharp
> [HttpGet]
> public IEnumerable<WeatherForecast> Get()
> ```

https://localhost:5001/WeatherForecast

## ASP.NET Core 中的 Razor 页面介绍

通过 Razor Pages 对基于页面的场景编码比使用控制器和视图更轻松、更高效。 

大众的初步印象是对于那些只专注于页面的小型应用来说，Razor页面更容易、更快地取代MVC。然而，事实证明，它可能比这更强大。

所有的Razor页面类型和特征都在`Microsoft.AspNetCore.Mvc.RazorPages`程序集中，MVC默认包`Microsoft.AspNetCore.Mvc`已经包含了Razor页面组件，这意味着您可以在MVC应用程序中直接使用Razor页面。

Razor页面的优点之一是设置和创建非常容易。您创建一个新的空项目，添加Pages文件夹，添加页面，只需在 *.cshtml* 文件中编写代码和标记。非常适合新手，是学习ASP.NET Core简单快速的方法！

### 检查项目文件

下面是主项目文件夹和文件的概述，将在后续教程中使用。

#### Pages 文件夹

包含 Razor 页面和支持文件。 每个 Razor 页面都是一对文件：

- 一个 .cshtml 文件，其中包含使用 Razor 语法的 C＃ 代码的 HTML 标记 。
- 一个 .cshtml.cs 文件，其中包含处理页面事件的 C# 代码 。

支持文件的名称以下划线开头。 例如，_Layout.cshtml 文件可配置所有页面通用的 UI 元素 。 此文件设置页面顶部的导航菜单和页面底部的版权声明。 有关详细信息，请参阅 [ASP.NET Core 中的布局](https://docs.microsoft.com/zh-cn/aspnet/core/mvc/views/layout?view=aspnetcore-3.0)。

#### wwwroot 文件夹

包含静态文件，如 HTML 文件、JavaScript 文件和 CSS 文件。 有关详细信息，请参阅 [ASP.NET Core 中的静态文件](https://docs.microsoft.com/zh-cn/aspnet/core/fundamentals/static-files?view=aspnetcore-3.0)。

#### appSettings.json

包含配置数据，如连接字符串。 有关详细信息，请参阅 [ASP.NET Core 中的配置](https://docs.microsoft.com/zh-cn/aspnet/core/fundamentals/configuration/index?view=aspnetcore-3.0)。

#### Program.cs

包含程序的入口点。 有关详细信息，请参阅 [.NET 通用主机](https://docs.microsoft.com/zh-cn/aspnet/core/fundamentals/host/generic-host?view=aspnetcore-3.0)。

#### Startup.cs

包含配置应用行为的代码。 有关详细信息，请参阅 [ASP.NET Core 中的应用启动](https://docs.microsoft.com/zh-cn/aspnet/core/fundamentals/startup?view=aspnetcore-3.0)。

### Razor 页面路由

页面的 URL 路径的关联由页面在文件系统中的位置决定。 下表显示了 Razor 页面路径及匹配的 URL：

| 文件名和路径                  | 匹配的 URL                 |
| :---------------------------- | :------------------------- |
| */Pages/Index.cshtml*         | `/` 或 `/Index`            |
| */Pages/Contact.cshtml*       | `/Contact`                 |
| */Pages/Store/Contact.cshtml* | `/Store/Contact`           |
| */Pages/Store/Index.cshtml*   | `/Store` 或 `/Store/Index` |

注意：

- 默认情况下，运行时在“Pages”文件夹中查找 Razor 页面文件。
- URL 未包含页面时，`Index` 为默认页面。

## ASP.NET Core MVC 概述

### 什么是 MVC 模式？

模型-视图-控制器 (MVC) 架构模式将应用程序分为三个主要组成部分：模型、视图和控制器。 此模式有助于实现[关注点分离](https://docs.microsoft.com/zh-cn/dotnet/standard/modern-web-apps-azure-architecture/architectural-principles#separation-of-concerns)。 使用此模式，用户请求被路由到控制器，后者负责使用模型来执行用户操作和/或检索查询结果。 控制器选择要显示给用户的视图，并为其提供所需的任何模型数据。

下图显示 3 个主要组件及其相互引用关系：

![MVC 模式](ASP.NET.assets/mvc.png)

这种责任划分有助于根据复杂性缩放应用程序，因为这更易于编码、调试和测试包含单一作业的某个组成部分（模型、视图或控制器）。 但这会加大更新、测试和调试代码的难度，该代码在这 3 个领域的两个或多个领域间存在依赖关系。 例如，用户界面逻辑的变更频率往往高于业务逻辑。 如果将表示代码和业务逻辑组合在单个对象中，则每次更改用户界面时都必须修改包含业务逻辑的对象。 这常常会引发错误，并且需要在每次进行细微的用户界面更改后重新测试业务逻辑。

> :zap:备注： 视图和控制器均依赖于模型。 但是，模型既不依赖于视图，也不依赖于控制器。 这是分离的一个关键优势。 这种分离允许模型独立于可视化展示进行构建和测试。

#### 模型责任

MVC 应用程序的模型 (M) 表示应用程序和任何应由其执行的业务逻辑或操作的状态。 业务逻辑应与保持应用程序状态的任何实现逻辑一起封装在模型中。 强类型视图通常使用 ViewModel 类型，旨在包含要在该视图上显示的数据。 控制器从模型创建并填充 ViewModel 实例。

#### 视图责任

视图 (V) 负责通过用户界面展示内容。 它们使用 [Razor 视图引擎](https://docs.microsoft.com/zh-cn/aspnet/core/mvc/overview?view=aspnetcore-3.0#razor-view-engine)在 HTML 标记中嵌入 .NET 代码。 视图中应该有最小逻辑，并且其中的任何逻辑都必须与展示内容相关。 如果发现需要在视图文件中执行大量逻辑以显示复杂模型中的数据，请考虑使用 [View Component](https://docs.microsoft.com/zh-cn/aspnet/core/mvc/views/view-components?view=aspnetcore-3.0)、ViewModel 或视图模板来简化视图。

#### 控制器职责

控制器 (C) 是处理用户交互、使用模型并最终选择要呈现的视图的组件。 在 MVC 应用程序中，视图仅显示信息；控制器处理并响应用户输入和交互。 在 MVC 模式中，控制器是初始入口点，负责选择要使用的模型类型和要呈现的视图（因此得名 - 它控制应用如何响应给定请求）。

> :zap: 备注
>
> 控制器不应由于责任过多而变得过于复杂。 要阻止控制器逻辑变得过于复杂，请将业务逻辑推出控制器并推入域模型。
>
> :notes: 提示
>
> 如果发现控制器操作经常执行相同类型的操作，可将这些常见操作移入[筛选器](https://docs.microsoft.com/zh-cn/aspnet/core/mvc/overview?view=aspnetcore-3.0#filters)。

### 什么是 ASP.NET Core MVC

ASP.NET Core MVC 框架是轻量级、开源、高度可测试的演示框架，并针对 ASP.NET Core 进行了优化。

ASP.NET Core MVC 提供一种基于模式的方式，用于生成可彻底分开管理事务的动态网站。 它提供对标记的完全控制，支持 TDD 友好开发并使用最新的 Web 标准。

### 路由

ASP.NET Core MVC 建立在 [ASP.NET Core 的路由](https://docs.microsoft.com/zh-cn/aspnet/core/fundamentals/routing?view=aspnetcore-3.0)之上，是一个功能强大的 URL 映射组件，可用于生成具有易于理解和可搜索 URL 的应用程序。 它可让你定义适用于搜索引擎优化 (SEO) 和链接生成的应用程序 URL 命名模式，而不考虑如何组织 Web 服务器上的文件。 可以使用支持路由值约束、默认值和可选值的方便路由模板语法来定义路由。

通过基于约定的路由，可以全局定义应用程序接受的 URL 格式以及每个格式映射到给定控制器上特定操作方法的方式 。 接收传入请求时，路由引擎分析 URL 并将其匹配到定义的 URL 格式之一，然后调用关联的控制器操作方法。

```csharp
routes.MapRoute(name: "Default", template: "{controller=Home}/{action=Index}/{id?}");
```

借助属性路由，可以通过用定义应用程序路由的属性修饰控制器和操作来指定路由信息 。 这意味着路由定义位于与之相关联的控制器和操作旁。

```csharp
[Route("api/[controller]")]
public class ProductsController : Controller
{
    [HttpGet("{id}")]
    public IActionResult GetProduct(int id)
    {
      ...
    }
}
```

---

## 使用 ASP.NET Core 创建 Web API

https://docs.microsoft.com/zh-cn/aspnet/core/tutorials/first-web-api?view=aspnetcore-3.0&tabs=visual-studio 

ASP.NET Core 支持使用 C# 创建 RESTful 服务，也称为 Web API。 若要处理请求，Web API 使用控制器。 Web API 中的*控制器*是派生自 `ControllerBase` 的类。 本文介绍了如何使用控制器处理 Web API 请求。 

###  RESTful 

RESTFUL是一种网络应用程序的设计风格和开发方式，基于HTTP，可以使用XML格式定义或JSON格式定义。RESTFUL适用于移动互联网厂商作为业务使能接口的场景，实现第三方OTT调用移动网络资源的功能，动作类型为新增、变更、删除所调用资源。

 ![RESTful](ASP.NET.assets/12924597-e6f35f665a63df45.webp) 

### REST基础概念

- 在REST中的一切都被认为是一种资源。
- 每个资源由URI标识。
- 使用统一的接口。处理资源使用POST，GET，PUT，DELETE操作类似创建，读取，更新和删除（CRUD）操作。
- 无状态。每个请求是一个独立的请求。从客户端到服务器的每个请求都必须包含所有必要的信息，以便于理解。
- 通信都是通过展现。例如XML，JSON

本教程将创建以下 API：

| API                   | 说明             | 请求正文 | 响应正文   |
| :-------------------- | :--------------- | :------- | :--------- |
| GET /api/Blog         | 获取所有待办事项 | 无       | 博客的数组 |
| GET /api/Blog/{id}    | 按 ID 获取项     | 无       | 博客       |
| POST /api/Blog        | 添加新项         | 博客     | 博客       |
| PUT /api/Blog/{id}    | 更新现有项       | 博客     | 无         |
| DELETE /api/Blog/{id} | 删除项           | 无       | 无         |

下图显示了应用的设计。

![右侧的框表示客户端。](ASP.NET.assets/architecture-1574595308385.png)

### Access-Control-Allow-Origin

解决跨域问题，可以先用Microsoft.AspNetCore.Cors

```sh
dotnet add package Microsoft.AspNetCore.Cors --version 2.2.0
```

再修改代码Startup.cs

```diff
diff --git "a/.NET/examples/webapi/Startup.cs" "b/.NET/examples/webapi/Startup.cs"
index ade6eda..5831cc0 100644
--- "a/.NET/examples/webapi/Startup.cs"
+++ "b/.NET/examples/webapi/Startup.cs"
@@ -26,11 +26,15 @@ namespace webapi
         public void ConfigureServices(IServiceCollection services)
         {
             services.AddControllers();
+            services.AddCors();
         }
 
         // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
         public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
         {
+            app.UseCors(
+                options => options.WithOrigins("*").AllowAnyMethod()
+            );
             if (env.IsDevelopment())
             {
                 app.UseDeveloperExceptionPage();

```



![](ASP.NET.assets/幻灯片2.png)
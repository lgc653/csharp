# Visual Studio

## 安装以及使用

### 下载安装包

[下载地址](https://visualstudio.microsoft.com/zh-hans/)，选择Visual Studio 2019的community版本

![](Visual Studio.assets/ea35925e3061403483cf5cfbcd2cf66a.jpeg)

### 下载好后运行

![](Visual Studio.assets/20030861668604624761592769.png)

### 组件的选择

如果是用来学C\C++的话，选择以下两个就够了

![](Visual Studio.assets/20030861668844837003531122.png)

![](Visual Studio.assets/20030861669071727773068556.png)

之后如果还需要其他一些功能的话，可以后续在进行添加。打开visual studio Installer，进入修改

![](Visual Studio.assets/20030861669150534684703858.png)

### 进行安装

如果不需要修改安装位置的话点击安装就可以了。不过由于vs2019占用的空间较大最好不要装在C盘。可以在安装位置修改安装的路径。

![](Visual Studio.assets/20030861669353899029041505.png)

### VS2019的使用创建一个project

![](Visual Studio.assets/20030861669580445429523005.png)

选择创建一个空项目。这里虽然是c++语言，但可以兼容c语言的，所以不用担心。

![](Visual Studio.assets/20030861669843889110122987.png)

![](Visual Studio.assets/20030861672738715001879336.png)

写测试程序

右击源文件添加一个新文件

![](Visual Studio.assets/20030861673066506519457057.png)

![](Visual Studio.assets/20030861673322513626959763.png)

现在就可以开始写代码了。

![](Visual Studio.assets/20030861673398901738322698.png)

编译方法如下图所示

![](Visual Studio.assets/20030861673684946476816241.png)

运行程序如下图所示

![](Visual Studio.assets/20030861673753399685320518.png)

运行结果

![](Visual Studio.assets/20030861673921702310104885.png)

## 创建一个简单的C#控制台应用程序

### 建立项目

首先，我们将创建一个C#应用程序项目。在添加任何内容之前，项目类型将随您需要的所有模板文件一起提供。

1. 打开Visual Studio 2019
2. 在开始窗口中，选择创建一个新项目。

![](Visual Studio.assets/1574130934.png)

3. 在“Create a new project”窗口上，在搜索框中输入console。接下来，从“Language”列表中选择C#，然后从“Platform”列表中选择Windows。

应用语言和平台过滤器后，选择Console App（.NET Core）模板，然后选择Next。

![](Visual Studio.assets/1574131028.png)

> **注意：**如果看不到控制台Console App (.NET Core)模板，则可以从“Create a new project”窗口中进行安装。在“Not finding what you're looking for?”消息页面，选择安装更多工具和功能链接。
>
> ![](Visual Studio.assets/not-finding-what-looking-for.png)

然后在Visual Studio安装程序中，选择.NET Core跨平台开发。

![](Visual Studio.assets/dot-net-core-xplat-dev-workload.png)

之后，在Visual Studio安装程序中选择“Modify”按钮。系统可能会提示您保存工作，选择保存就好了。接下来，选择继续安装工作负载。然后，返回此“Create a project”过程中的步骤2 。

4. 在“Configure your new project”窗口中，在“Project name”框中键入或输入“Calculator”。然后，选择Create。

![](Visual Studio.assets/1574131408.png)

Visual Studio将打开新项目，其中包括默认的“Hello World”代码。

### 创建应用

首先，我们将探索C#中的一些基本整数数学。然后，添加代码来创建基本计算器。之后，调试该应用程序，查找并修复错误。最后，为让应用程序更高效，我们将优化代码。

#### 探索整数数学

1. 在代码编辑器中，删除默认的“Hello World”代码。

![](Visual Studio.assets/csharp-console-calculator-deletehelloworld.png)

具体来说，删除表示的行Console.WriteLine("Hello World!");。

2. 在Hello World的位置上，输入以下代码：

```c#
int a = 42;
int b = 119;
int c = a + b;
Console.WriteLine(c);
Console.ReadKey();
```

注意，在输入时，Visual Studio中的IntelliSense功能为您提供了自动完成输入的选项。

![](Visual Studio.assets/integer-math-intellisense.gif)

3. 选择Calculator运行程序，或按F5。

![](Visual Studio.assets/csharp-console-calculator-button.png)

将打开一个控制台窗口，其中显示42 + 119的总和，即161。

![](Visual Studio.assets/csharp-console-integer-math.png)

4. （可选）您可以更改运算符来更改结果。例如，将代码行中的+运算符更改为减、乘或除。然后，当运行程序时，结果也会改变。int c = a + b;-*/

5. 关闭控制台窗口。

### 添加代码创建计算器

继续向项目添加一组更复杂的计算器代码。

1. 删除在代码编辑器中看到的所有代码。

2. 输入以下新代码或将其粘贴到代码编辑器中：

```c#
using System;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            // Declare variables and then initialize to zero.
            int num1 = 0; int num2 = 0;

            // Display title as the C# console calculator app.
            Console.WriteLine("Console Calculator in C#\r");
            Console.WriteLine("------------------------\n");

            // Ask the user to type the first number.
            Console.WriteLine("Type a number, and then press Enter");
            num1 = Convert.ToInt32(Console.ReadLine());

            // Ask the user to type the second number.
            Console.WriteLine("Type another number, and then press Enter");
            num2 = Convert.ToInt32(Console.ReadLine());

            // Ask the user to choose an option.
            Console.WriteLine("Choose an option from the following list:");
            Console.WriteLine("\ta - Add");
            Console.WriteLine("\ts - Subtract");
            Console.WriteLine("\tm - Multiply");
            Console.WriteLine("\td - Divide");
            Console.Write("Your option? ");

            // Use a switch statement to do the math.
            switch (Console.ReadLine())
            {
                case "a":
                    Console.WriteLine($"Your result: {num1} + {num2} = " + (num1 + num2));
                    break;
                case "s":
                    Console.WriteLine($"Your result: {num1} - {num2} = " + (num1 - num2));
                    break;
                case "m":
                    Console.WriteLine($"Your result: {num1} * {num2} = " + (num1 * num2));
                    break;
                case "d":
                    Console.WriteLine($"Your result: {num1} / {num2} = " + (num1 / num2));
                    break;
            }
            // Wait for the user to respond before closing.
            Console.Write("Press any key to close the Calculator console app...");
            Console.ReadKey();
        }
    }
}
```

3. 选择计算器运行程序，或按F5。

![](Visual Studio.assets/csharp-console-calculator-button.png)

将打开一个控制台窗口。

4. 在控制台窗口中查看应用程序，然后按照提示添加数字42和119。

![](Visual Studio.assets/csharp-console-calculator.png)

### 向计算器添加功能

通过调整代码来添加更多功能。

#### 加小数

计算器应用程序当前接受并返回整数。但是，如果我们添加允许使用小数的代码，它将更加精确。

如下面的屏幕截图所示，如果您运行应用程序，然后将数字42除以119，则结果为0（零），这是不正确的。

![](Visual Studio.assets/csharp-console-calculator-nodecimal.png)

简单修改代码就可以处理小数：

1. 按Ctrl + F打开“Find and Replace”控件。

2. 将int变量的每个实例更改为float。

确保在“Find and Replace”控件中切换“Match case (Alt+C)”和“Match whole word (Alt+W) ” 。

![](Visual Studio.assets/find-replace-control-animation.gif)

3. 再次运行计算器应用，然后将数字42除以119。

请注意，应用程序现在返回的是十进制数字而不是零。

![](Visual Studio.assets/csharp-console-calculator-decimal.png)

但是，该应用程序仅产生十进制结果。现在，继续对代码进行一些调整，让应用程序也可以计算小数。

1. 使用“Find and Replace”控件（Ctrl + F）将float变量的每个实例更改为double，并将Convert.ToInt32方法的每个实例更改为Convert.ToDouble。

2. 运行计算器应用，然后将数字42.5除以119.75。

请注意，该应用程序现在接受十进制值，并返回一个较长的十进制数字作为结果。

![](Visual Studio.assets/csharp-console-calculator-usedecimals.png)

### 调试应用

我们在基本的计算器应用程序上进行了改进，但尚未设置故障保险柜来处理诸如用户输入错误之类的异常。

例如，如果您试图将一个数字除以0，或者在应用程序需要一个数字字符时输入一个alpha字符(反之亦然)，应用程序将停止工作并返回一个错误。

接下来，我们浏览几个常见的用户输入错误，在调试器中找到它们，并在代码中修复错误。

#### 修复“被零除”错误

当您尝试将数字除以零时，控制台应用程序将停止运行。然后，Visual Studio会提示代码编辑器中的问题。

![](Visual Studio.assets/csharp-console-calculator-dividebyzero-error.png)

要处理此错误，只需更改几个代码：

1. 删除直接出现在的代码case "d":和注释之间的代码// Wait for the user to respond before closing。

2. 将其替换为以下代码：

```c#
// Ask the user to enter a non-zero divisor until they do so.
while (num2 == 0)
{
    Console.WriteLine("Enter a non-zero divisor: ");
    num2 = Convert.ToInt32(Console.ReadLine());
}
Console.WriteLine($"Your result: {num1} / {num2} = " + (num1 / num2));
break;
```

添加代码后，带有switch语句的部分应类似于以下屏幕截图：

![](Visual Studio.assets/csharp-console-calculator-switch-code.png)

现在，当您将任何数字除以零时，应用程序将提示您换一个数字，直到您提供非零的数字。

![](Visual Studio.assets/csharp-console-calculator-dividebyzero.png)

#### 修复“格式”错误

如果在应用程序要求输入数字字符时输入字母字符（反之亦然），则控制台应用程序将停止运行。然后，Visual Studio会提示代码编辑器中的问题。

![](Visual Studio.assets/1574134075.png)

要解决此错误，必须重构我们先前输入的代码。

##### 修改代码

我们将应用分为两类：Calculator和Program，而不是依赖程序类来处理所有代码。

Calculator类将处理大量的计算工作，而所述Program类将处理用户界面和捕获错误的工作。

1. 删除以下代码块之后的所有内容：

```c#
using System;

namespace Calculator
{
```

2. 添加一个新Calculator类，如下所示：

```c#
class Calculator
{
    public static double DoOperation(double num1, double num2, string op)
    {
        double result = double.NaN; // Default value is "not-a-number" which we use if an operation, such as division, could result in an error.

        // Use a switch statement to do the math.
        switch (op)
        {
            case "a":
                result = num1 + num2;
                break;
            case "s":
                result = num1 - num2;
                break;
            case "m":
                result = num1 * num2;
                break;
            case "d":
                // Ask the user to enter a non-zero divisor.
                if (num2 != 0)
                {
                    result = num1 / num2;
                }
                break;
            // Return text for an incorrect option entry.
            default:
                break;
        }
        return result;
    }
}
```

3. 然后，添加一个新Program类，如下所示：

```c#
class Program
{
    static void Main(string[] args)
    {
        bool endApp = false;
        // Display title as the C# console calculator app.
        Console.WriteLine("Console Calculator in C#\r");
        Console.WriteLine("------------------------\n");

        while (!endApp)
        {
            // Declare variables and set to empty.
            string numInput1 = "";
            string numInput2 = "";
            double result = 0;

            // Ask the user to type the first number.
            Console.Write("Type a number, and then press Enter: ");
            numInput1 = Console.ReadLine();

            double cleanNum1 = 0;
            while (!double.TryParse(numInput1, out cleanNum1))
            {
                Console.Write("This is not valid input. Please enter an integer value: ");
                numInput1 = Console.ReadLine();
            }

            // Ask the user to type the second number.
            Console.Write("Type another number, and then press Enter: ");
            numInput2 = Console.ReadLine();

            double cleanNum2 = 0;
            while (!double.TryParse(numInput2, out cleanNum2))
            {
                Console.Write("This is not valid input. Please enter an integer value: ");
                numInput2 = Console.ReadLine();
            }

            // Ask the user to choose an operator.
            Console.WriteLine("Choose an operator from the following list:");
            Console.WriteLine("\ta - Add");
            Console.WriteLine("\ts - Subtract");
            Console.WriteLine("\tm - Multiply");
            Console.WriteLine("\td - Divide");
            Console.Write("Your option? ");

            string op = Console.ReadLine();

            try
            {
                result = Calculator.DoOperation(cleanNum1, cleanNum2, op);
                if (double.IsNaN(result))
                {
                    Console.WriteLine("This operation will result in a mathematical error.\n");
                }
                else Console.WriteLine("Your result: {0:0.##}\n", result);
            }
            catch (Exception e)
            {
                Console.WriteLine("Oh no! An exception occurred trying to do the math.\n - Details: " + e.Message);
            }

            Console.WriteLine("------------------------\n");

            // Wait for the user to respond before closing.
            Console.Write("Press 'n' and Enter to close the app, or press any other key and Enter to continue: ");
            if (Console.ReadLine() == "n") endApp = true;

            Console.WriteLine("\n"); // Friendly linespacing.
        }
        return;
    }
}
```

4. 选择Calculator运行程序，或按F5。

5. 按照提示将数字42除以119。应用程序应类似于以下屏幕截图：

![](Visual Studio.assets/csharp-console-calculator-refactored.png)

请注意，在选择关闭控制台应用程序之前，可以选择输入更多方程式。并且，我们还减少了结果中的小数位数。

##### 关闭应用程序

1. 如果尚未执行此操作，请关闭计算器应用程序。

2. 在Visual Studio中关闭“Output”窗格。

![](Visual Studio.assets/csharp-calculator-close-output-pane.png)

3. 在Visual Studio中，按Ctrl + S保存应用程序。

4. 关闭Visual Studio。

### 代码完成

在本教程中，我们对计算器应用程序进行了很多更改。该应用程序现在可以更有效地处理计算资源，并且可以处理大多数用户输入错误。

以下是涉及的所有代码：

```c#
using System;

namespace Calculator
{
    class Calculator
    {
        public static double DoOperation(double num1, double num2, string op)
        {
            double result = double.NaN; // Default value is "not-a-number" which we use if an operation, such as division, could result in an error.

            // Use a switch statement to do the math.
            switch (op)
            {
                case "a":
                    result = num1 + num2;
                    break;
                case "s":
                    result = num1 - num2;
                    break;
                case "m":
                    result = num1 * num2;
                    break;
                case "d":
                    // Ask the user to enter a non-zero divisor.
                    if (num2 != 0)
                    {
                        result = num1 / num2;
                    }
                    break;
                // Return text for an incorrect option entry.
                default:
                    break;
            }
            return result;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            bool endApp = false;
            // Display title as the C# console calculator app.
            Console.WriteLine("Console Calculator in C#\r");
            Console.WriteLine("------------------------\n");

            while (!endApp)
            {
                // Declare variables and set to empty.
                string numInput1 = "";
                string numInput2 = "";
                double result = 0;

                // Ask the user to type the first number.
                Console.Write("Type a number, and then press Enter: ");
                numInput1 = Console.ReadLine();

                double cleanNum1 = 0;
                while (!double.TryParse(numInput1, out cleanNum1))
                {
                    Console.Write("This is not valid input. Please enter an integer value: ");
                    numInput1 = Console.ReadLine();
                }

                // Ask the user to type the second number.
                Console.Write("Type another number, and then press Enter: ");
                numInput2 = Console.ReadLine();

                double cleanNum2 = 0;
                while (!double.TryParse(numInput2, out cleanNum2))
                {
                    Console.Write("This is not valid input. Please enter an integer value: ");
                    numInput2 = Console.ReadLine();
                }

                // Ask the user to choose an operator.
                Console.WriteLine("Choose an operator from the following list:");
                Console.WriteLine("\ta - Add");
                Console.WriteLine("\ts - Subtract");
                Console.WriteLine("\tm - Multiply");
                Console.WriteLine("\td - Divide");
                Console.Write("Your option? ");

                string op = Console.ReadLine();

                try
                {
                    result = Calculator.DoOperation(cleanNum1, cleanNum2, op);
                    if (double.IsNaN(result))
                    {
                        Console.WriteLine("This operation will result in a mathematical error.\n");
                    }
                    else Console.WriteLine("Your result: {0:0.##}\n", result);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Oh no! An exception occurred trying to do the math.\n - Details: " + e.Message);
                }

                Console.WriteLine("------------------------\n");

                // Wait for the user to respond before closing.
                Console.Write("Press 'n' and Enter to close the app, or press any other key and Enter to continue: ");
                if (Console.ReadLine() == "n") endApp = true;

                Console.WriteLine("\n"); // Friendly linespacing.
            }
            return;
        }
    }
}
```

## 使用C#在Visual Studio中创建Windows窗体应用程序

### 创建一个项目

首先，您将创建一个C#应用程序项目。在添加任何内容之前，项目类型提供了所需的所有模板文件。

1. 打开Visual Studio 2019。

2. 在“开始”窗口中，选择“**创建新项目**”。

![](Visual Studio.assets/create-new-project-dark-theme.png)

3. 在“**创建新项目**”窗口上，选择C#的**Windows Forms App（.NET Framework）**模板。

（如果愿意，可以优化搜索以快速找到所需的模板。例如，在搜索框中输入或键入Windows Forms App。接下来，从“语言”列表中选择**C#**，然后从“平台”列表中选择**Windows**。）

![Visual Studio 使用教程：使用C#在Visual Studio中创建Windows窗体应用程序](Visual Studio.assets/csharp-create-new-winforms-project-nonfiltered.png)

> **注意：**
>
> 如果看不到**Windows Forms App（.NET Framework）**模板，则可以从“**创建新项目**”窗口中进行安装。**找不到您要的东西吗**？消息，选择**安装更多工具和功能**链接。
>
> ![](Visual Studio.assets/not-finding-what-looking-for.png)

接下来，在Visual Studio安装程序中，选择“选择**.NET桌面开发工作负载**”。

![](Visual Studio.assets/install-dot-net-desktop-env.png)

之后，在Visual Studio安装程序中选择“**修改**”按钮。可能会提示您保存您的工作。如果是，请就这样做。接下来，选择“**继续**”以安装工作负载。然后，返回此“**创建项目**”过程中的步骤2 。

4. 在“**配置新项目**”窗口中，在“**项目名称**”框中键入或输入HelloWorld。然后，选择**创建**。

![](Visual Studio.assets/csharp-name-your-winform-project-helloworld.png)

Visual Studio将打开您的新项目。

### 创建应用程序

选择C#项目模板并命名文件后，Visual Studio会为您打开一个表单。表单是Windows用户界面。我们将通过向表单添加控件来创建“Hello World”应用程序，然后运行该应用程序。

在表单中添加一个按钮

1. 选择“**工具箱**”以打开“工具箱”弹出窗口。

![](Visual Studio.assets/csharp-toolbox-toolwindow.png)

> 如果看不到“**工具箱**”弹出选项，则可以从菜单栏中打开它。为此，请单击“**视图**” >“**工具箱**”。或者按**Ctrl + Alt + X**。

2. 选择“**固定**”图标以停靠“**工具箱**”窗口。

![](Visual Studio.assets/vb-pin-the-toolbox-window.png)

3. 选择**按钮**控件，然后将其拖动到窗体上。

![](Visual Studio.assets/csharp-add-button-form1.png)

4. 在“**属性**”窗口中，找到“**文本**”，将名称从**Button1**更改为单击它，然后按**Enter**。

![](Visual Studio.assets/vb-button-control-text.png)

> 如果看不到“**属性**”窗口，则可以从菜单栏中打开它。为此，请选择“**视图**” >“**属性窗口**”。或者按**F4键**。

5. 在“**属性**”窗口的“**设计**”部分中，将名称从**Button1**更改为btnClickThis，然后按**Enter**。

![](Visual Studio.assets/vb-button-control-function.png)

> **注意：**如果您按字母顺序排列了**属性**窗口中的列表，则**Button1**会显示在（**DataBindings**）部分中。

### 向表单添加标签

现在，我们已经添加了一个按钮控件来创建一个动作，让我们添加一个标签控件来向其发送文本。

1. 从“**工具箱**”窗口中选择“**标签**”控件，然后将其拖动到窗体上，并将其拖放到“**单击此**按钮”下方。

2. 在“**属性**”窗口的“**设计**”部分或“ **（数据绑定）**”部分中，将**Label1**的名称**更改**为lblHelloWorld，然后按**Enter**。

### 将代码添加到表单

1. 在**Form1.cs [设计]**窗口中，双击“**Click this**按钮”以打开**Form1.cs**窗口。（或者，您可以在**解决方案资源管理器**中展开**Form1.cs**，然后选择**Form1**。）

2. 在**Form1.cs**窗口中，在**专用空白**行之后，键入或输入lblHelloWorld.Text = "Hello World!"；如以下图片所示：

![](Visual Studio.assets/csharp-winforms-add-code.png)

### 运行应用程序

1. 选择**开始**按钮以运行该应用程序。

![](Visual Studio.assets/vb-click-start-hello-world.png)

将会发生几件事。在Visual Studio IDE中，“**诊断工具**”窗口将打开，并且“**输出**”窗口也将打开。但是在IDE外部，将出现一个**Form1**对话框。它将包括您的“单击此按钮”和显示**Label1**的文本。

2. 在“**Form1**”对话框中选择“单击此按钮”。请注意，**Label1**文本更改为**Hello World**！。

![](Visual Studio.assets/vb-form1-dialog-hello-world.png)

3. 关闭**Form1**对话框以停止运行该应用程序。
# WinForm 摄像头人脸检测

##  C# 人脸识别库

### 使用

1. 创建你的 .NET 应用，并且你的 .NET 版本需要满足以下条件：

   - .NET Standard >= 2.0
   - .NET Core >= 2.0
   - .NET Framework >= 4.6.1^2

2. 使用 Nuget 安装 **`ViewFaceCore`**

   - Author : *View*
   - Version : *Latest*

   > 此 Nuget 包会自动添加依赖的 C++ 库，以及最精简的识别模型。(`face_detector.csta`)
   > 请自行下载需要的 [SeetaFace6 模型文件](https://github.com/seetafaceengine/SeetaFace6#百度网盘)。
   > 若没有硬盘要求，建议下载全部模型。

3. 在项目中编写你的代码

   - 一个简单的例子 `ViewFaceTest/Program.cs`

4. 构建

   1. 生成你的项目，此时项目的生成目录中会出现 `model` 文件夹。
   2. 将下载的 ***.csta**模型文件拷贝至`model`文件夹。
      - 也可以使用 生成命令自动复制模型文件至输出目录

### 项目说明

| 项目                | 语言  | 说明                                                       |
| ------------------- | ----- | ---------------------------------------------------------- |
| ViewFace            | `C++` | 基于 `SeetaFace6` 的接口封装，支持 x86、x64                |
| ViewFaceCore        | `C#`  | 基于 `ViewFace` 的 C# 形式的封装，支持 AnyCPU              |
| ViewFaceTest        | `C#`  | 调用 `ViewFaceCore` 实现的简单的图片人脸识别               |
| ViewFaceTestPackage | `C#`  | 调用 Nuget 中的 `ViewFaceCore` 包 实现的简单的图片人脸识别 |

### 编译本项目

1. 开发环境：

   - 开发工具 : Visual Studio 2019 16.7.1
   - 操作系统 : Windows 10 专业版 2004 19041.450

2. 依赖：

   - 下载 [SeetaFace6 开发包](https://github.com/seetafaceengine/SeetaFace6#百度网盘)
   - SeetaFace 开发包头文件存放路径 : `C:\vclib\seeta\include\seeta`
   - SeetaFace 开发包的 x86 和 x64 的类库的存放路径 : `C:\vclib\seeta\lib`

3. 编译流程 (Release) ：

   1. 分别编译 x86 和 x64 模式的 `ViewFace` 项目。
   2. 切换到 AnyCPU ，并编译 `ViewFaceCore` 项目。

   > 或者使用 ReBuild.bat 自动编译。

 ## API

global.json，在该文件所在目录以下均生成这里定义的版本，具体可以在c:\Program Files\dotnet\sdk\查看

```json
{
  "sdk": {
    "version": "3.1.404"
  }
}
```



```sh
dotnet new webapi -o ViewFaceApi
cd ViewFaceApi
dotnet add package ViewFaceCore --version 0.3.4
```


![](WinFrom桌面应用程序.assets/幻灯片0.png)

# WinFrom桌面应用程序

Windows平台上的应用开发，我们通常会选择以下三个技术：

- Windows Forms
- Windows Presentation Foundation
- Universal Windows Platform (UWP)

但事实上, 你真正想瞄准的是 Windows。Windows是平台。 这些选项只是我们针对 Windows 开发应用时所拥有的不同 UI 框架选项。

而且, 这三个UI 框架现在都是开源的：

- Windows Presentation Foundation: https://github.com/dotnet/wpf
- Windows Forms: https://github.com/dotnet/winforms
- Windows 10 XAML: https://github.com/Microsoft/microsoft-ui-xaml

## 使用.NET Core开发WinFrom

可以说，.NET Core 是仅支持 Windows 的 .NET Framework 的一个跨平台分支，自 2016 年 6 月首次发布后，微软一直在保持同时开发这两个版本。当时微软表示，.NET Core 适用于 ASP.NET Web 应用，以及 Win10 UWP 应用。Windows 桌面应用将继续使用 .NET Framework 。

.NET Core 3.0 将改变这一局面，其最大的亮点就是支持 Windows 桌面应用，也就是说届时它将支持 Windows Forms，Windows Presentation Foundation（WPF）和 UWP 等所有主要的 Windows 桌面平台。这将有效减少 .NET 的碎片化和混淆性，并减少 .NET 开发者的传统约束。

此外，[在 .NET 的官方博客中](https://blogs.msdn.microsoft.com/dotnet/2018/05/07/net-core-3-and-support-for-windows-desktop-applications/)，微软还透露 .NET Core 目前没有支持 Mac 或 Linux 上的桌面应用的计划。

###  前提 

前提是安装好.netCore开发环境，以及最新的.netCore3.0以上

### 创建项目

使用命令创建,打开CMD,输入：`dotnet new winforms -o TestWFCore`

```sh
λ dotnet new winforms -o TestWFCore
The template "Windows Forms (WinForms) Application" was created successfully.

Processing post-creation actions...
Running 'dotnet restore' on TestWFCore\TestWFCore.csproj...
  D:\百度云同步盘\Dev\lessons\.NET\examples\TestWFCore\TestWFCore.csproj 的还原在 55.36 ms 内完成。


Restore succeeded.
```

### 运行项目

```sh
 cd TestWFCore\
 dotnet run
```

![WinFrom桌面应用程序](WinFrom桌面应用程序.assets/image-20191117174232748.png)

### 使用Visual Studio Code开发

![使用Visual Studio Code开发](WinFrom桌面应用程序.assets/image-20191117174347341.png)

## 使用 .NET Framework开发

### 前提

安装SharpDevelop，Visual Studio 是大部分开发者使用的 Windows 系统中的开发工具，但是它体积过于庞大，安装时间甚至超过安装一个操作系统的时间，因此对于开发一个小程序来说极为不适用，所以，SharpDevelop，成为开发.net程序的首选替代品。 

* https://sourceforge.net/projects/sharpdevelop/ 

* https://github.com/icsharpcode

![SharpDevelop](WinFrom桌面应用程序.assets/image-20191117175519294.png)

### 创建项目

打开New Project，选中C#-》Windows Application 

![SharpDevelop创建WinFrom项目](WinFrom桌面应用程序.assets/image-20191117175958350.png)

### 窗体设计

如图打开窗体设计器，可以进行窗体设计

![窗体设计](WinFrom桌面应用程序.assets/image-20191117180646793.png)

为案例追加点击事件

```csharp
/*
 * Created by SharpDevelop.
 * User: lgc653
 * Date: 2019/11/17
 * Time: 18:03
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace TestWF
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		void Button1Click(object sender, EventArgs e)
		{
			MessageBox.Show("桌面程序界面绘制", "测试");
		}
	}
}
```

### 运行项目

按F5运行该项目

![运行项目](WinFrom桌面应用程序.assets/image-20191117181053855.png)

### 代码解析

#### MainForm.resx

resx文件是存放资源的，如图，我将一个百度的logo图片文件导入

![resx文件](WinFrom桌面应用程序.assets/image-20191117181939472.png)

查看MainForm.resx文件

```xml
<?xml version="1.0" encoding="utf-8"?>
<root>
  <!-- 
    Microsoft ResX Schema 
    
    Version 2.0
    
    The primary goals of this format is to allow a simple XML format 
    that is mostly human readable. The generation and parsing of the 
    various data types are done through the TypeConverter classes 
    associated with the data types.
    
    Example:
    
    ... ado.net/XML headers & schema ...
    <resheader name="resmimetype">text/microsoft-resx</resheader>
    <resheader name="version">2.0</resheader>
    <resheader name="reader">System.Resources.ResXResourceReader, System.Windows.Forms, ...</resheader>
    <resheader name="writer">System.Resources.ResXResourceWriter, System.Windows.Forms, ...</resheader>
    <data name="Name1"><value>this is my long string</value><comment>this is a comment</comment></data>
    <data name="Color1" type="System.Drawing.Color, System.Drawing">Blue</data>
    <data name="Bitmap1" mimetype="application/x-microsoft.net.object.binary.base64">
        <value>[base64 mime encoded serialized .NET Framework object]</value>
    </data>
    <data name="Icon1" type="System.Drawing.Icon, System.Drawing" mimetype="application/x-microsoft.net.object.bytearray.base64">
        <value>[base64 mime encoded string representing a byte array form of the .NET Framework object]</value>
        <comment>This is a comment</comment>
    </data>
                
    There are any number of "resheader" rows that contain simple 
    name/value pairs.
    
    Each data row contains a name, and value. The row also contains a 
    type or mimetype. Type corresponds to a .NET class that support 
    text/value conversion through the TypeConverter architecture. 
    Classes that don't support this are serialized and stored with the 
    mimetype set.
    
    The mimetype is used for serialized objects, and tells the 
    ResXResourceReader how to depersist the object. This is currently not 
    extensible. For a given mimetype the value must be set accordingly:
    
    Note - application/x-microsoft.net.object.binary.base64 is the format 
    that the ResXResourceWriter will generate, however the reader can 
    read any of the formats listed below.
    
    mimetype: application/x-microsoft.net.object.binary.base64
    value   : The object must be serialized with 
            : System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
            : and then encoded with base64 encoding.
    
    mimetype: application/x-microsoft.net.object.soap.base64
    value   : The object must be serialized with 
            : System.Runtime.Serialization.Formatters.Soap.SoapFormatter
            : and then encoded with base64 encoding.

    mimetype: application/x-microsoft.net.object.bytearray.base64
    value   : The object must be serialized into a byte array 
            : using a System.ComponentModel.TypeConverter
            : and then encoded with base64 encoding.
    -->
  <xsd:schema id="root" xmlns="" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
    <xsd:import namespace="http://www.w3.org/XML/1998/namespace" />
    <xsd:element name="root" msdata:IsDataSet="true">
      <xsd:complexType>
        <xsd:choice maxOccurs="unbounded">
          <xsd:element name="metadata">
            <xsd:complexType>
              <xsd:sequence>
                <xsd:element name="value" type="xsd:string" minOccurs="0" />
              </xsd:sequence>
              <xsd:attribute name="name" use="required" type="xsd:string" />
              <xsd:attribute name="type" type="xsd:string" />
              <xsd:attribute name="mimetype" type="xsd:string" />
              <xsd:attribute ref="xml:space" />
            </xsd:complexType>
          </xsd:element>
          <xsd:element name="assembly">
            <xsd:complexType>
              <xsd:attribute name="alias" type="xsd:string" />
              <xsd:attribute name="name" type="xsd:string" />
            </xsd:complexType>
          </xsd:element>
          <xsd:element name="data">
            <xsd:complexType>
              <xsd:sequence>
                <xsd:element name="value" type="xsd:string" minOccurs="0" msdata:Ordinal="1" />
                <xsd:element name="comment" type="xsd:string" minOccurs="0" msdata:Ordinal="2" />
              </xsd:sequence>
              <xsd:attribute name="name" type="xsd:string" use="required" msdata:Ordinal="1" />
              <xsd:attribute name="type" type="xsd:string" msdata:Ordinal="3" />
              <xsd:attribute name="mimetype" type="xsd:string" msdata:Ordinal="4" />
              <xsd:attribute ref="xml:space" />
            </xsd:complexType>
          </xsd:element>
          <xsd:element name="resheader">
            <xsd:complexType>
              <xsd:sequence>
                <xsd:element name="value" type="xsd:string" minOccurs="0" msdata:Ordinal="1" />
              </xsd:sequence>
              <xsd:attribute name="name" type="xsd:string" use="required" />
            </xsd:complexType>
          </xsd:element>
        </xsd:choice>
      </xsd:complexType>
    </xsd:element>
  </xsd:schema>
  <resheader name="resmimetype">
    <value>text/microsoft-resx</value>
  </resheader>
  <resheader name="version">
    <value>2.0</value>
  </resheader>
  <resheader name="reader">
    <value>System.Resources.ResXResourceReader, System.Windows.Forms, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089</value>
  </resheader>
  <resheader name="writer">
    <value>System.Resources.ResXResourceWriter, System.Windows.Forms, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089</value>
  </resheader>
  <assembly alias="System.Drawing" name="System.Drawing, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" />
  <data name="pictureBox1.Image" type="System.Drawing.Bitmap, System.Drawing" mimetype="application/x-microsoft.net.object.bytearray.base64">
    <value>
        /9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYa
        HSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgo
        KCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCAAwAGQDASIAAhEBAxEB/8QA
        GgABAAIDAQAAAAAAAAAAAAAAAAYHAwUIBP/EADIQAAEDAwMDAgUDAwUAAAAAAAECAwQABREGEiEHEzEU
        QSJRYXGBFTKRCBYjM0JSofD/xAAZAQEAAwEBAAAAAAAAAAAAAAAAAwQGBQL/xAAtEQABBAECBAUDBQEA
        AAAAAAABAAIDEQQhMQUSQVETFGGBkSIy8BVScaHB0f/aAAwDAQACEQMRAD8A6ppSlESlKURKUpREpSlE
        SlKURKUpREpSlESlKh+suodi0hcWYV4VJD7rQeT2mtw2kkec/NJry97WC3GgpoMeXJf4cLS49gphSobp
        LqTpvVNw9BbJTiZhBUhp5soKwOTt9jge3mplRj2vFtNpPjS4z+SZpaexFJVQf1A6/vOjWLK3p9bbTkxT
        i1vqbDnwo2/CAcjndz78cVt+qd6cutk1LpTT6ZS9QIgh/YhBwtrcjelJ91FKiMfWqs0CzE0z0vucrqRa
        HHremahVtgy2ylxbu07tgVggHjPtwfNd/g+NC8eafT+V3LydTY/wn+ioZmPjprhVi/ZSDXXW65WROmhA
        t0YuzIDE+YHgrGHB+xGDx4Jyc+R9avWG+JUNiQEKQHW0ubVDBTkZwfrVE2fWWkeovq5U3TTDOorRCcfg
        NOudxLqW0lQSMbQrB52kHyce9eLoh1QvMyZfFatnOy7bGjCSXezuU0suJQEgIGcHf49tv3qxn8OHli9s
        fhmLV1ne9q3BA9uy8R8z3hrdSdl0TSoEerWkUuJbVNlBxXISYT2T+Ntet7qVppmHEkrlSQ3LW4hkCI6V
        KLYSV/DtzwFD/wAKyvjx/uHyuieGZgoGJ2voVMqVX0fq1p1xx7ut3NhhCgG31wnCh7P/ABwCf5A81O4U
        luZDYlMbu0+2lxG5JSdqhkZB5B58GvTJGP8AtNqLIw58avGYW33CzUpSvarJVKdTBdz1r08NOCKq6fpy
        u0JX+n5e3Z/GfzV11ENY9PbJq25sz7r6sSGWgyksu7BtyT8vmo1BkRukbTe4XU4RlRYs5fNsWuG17itr
        Fj3VZvybvbOqunpnUP0bO2O6Iy7fjak4IJX/ALscn/r61pbxOW3pp69ack9QQ22QpibLlBUY4WEkqwc4
        8j71bdh6V6Xs0/1jUV6S+EqQkynS4Eggg4HjwT5rxr6PaaUwuOl26txVEnsJmK7Y5zjB+tVDjS0fW+uv
        TrS77OM4DXtJvQNH200gFxP02a37kb6LQ6M1PcpnUaQ/cJklyIdOMz1RUuK7QcLbKlFKM4B+JX81GLBP
        /vK6ae1VqGHcLo/Ju/pYseKvEa2IQpBBcG05Jzk5xkCrlsmibRZrym6Qw/6kQkW/43Nye0hKAOMecITz
        96jbHRyxw7hIk2m66htTb69640Gd2mvtgJzj81ouDTRY0cgnNOOx301sdx0+K2Wa4rPFkStdAKaGgdtQ
        qymaFjsa3YuUH9RYSuJOuSpLKsJakNuObQFYwB8I+GpJpvU4vXQ3UF2hwo9qurOWZD0BsMd1xOwhzKcc
        kL/nNSiX0hty46mIGotTwWVlRcaauKihzd+4kKByTk5+ea39q0DZLZop3S8VD4tzwPdUV/5HFEglRVjz
        wPbwKvcUz4srEMYcXP2GlUNd/kfCq4bhDkMkePpBBPyqogWlmUiDcpFk6hP3AMJxLbkAnBTzsUTkJOTx
        9a1kJ9262fQy7qZ9wWtF4Cgla3HlYb4A5ycccZ9qt9rpzBaaQ21e9SobQAlKU3RwBIHgAfKsjXTiyR4l
        qYiO3CN+md/07rMkocSXuFncOc1j/Kv/AD+R/wAWu/XMbqSTrWh0HK8dT3cNlz5FXBkW61W8xZCwJCU7
        2A8t53arKkhrvbQfskY48V1bbG22rbEbZadZaQyhKG3SStACRhKiSSSPB5P3qINdMNPNPepaVc0XAqUp
        c5M51L7m7zuWDzU0jMiPGaZSpa0toCApxRUogDGSTyT9amxYHRXzUubxzikOcGCG9CSb9ff89FkpSlXF
        n1//2Q==
</value>
  </data>
</root>
```

> :fire:注意`pictureBox1.Image`，这就是刚才导入的图片

#### MainForm.Designer.cs

```csharp
/*
 * Created by SharpDevelop.
 * User: lgc653
 * Date: 2019/11/17
 * Time: 18:03
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace TestWF
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.PictureBox pictureBox1;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.button1 = new System.Windows.Forms.Button();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(13, 13);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 0;
			this.button1.Text = "测试按钮";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(13, 64);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(129, 70);
			this.pictureBox1.TabIndex = 1;
			this.pictureBox1.TabStop = false;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(282, 253);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.button1);
			this.Name = "MainForm";
			this.Text = "TestWF";
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);

		}
	}
}
```

> :fire: 注意pictureBox1、button1的定义

#### MainForm.cs

```csharp
/*
 * Created by SharpDevelop.
 * User: lgc653
 * Date: 2019/11/17
 * Time: 18:03
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace TestWF
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		void Button1Click(object sender, EventArgs e)
		{
			MessageBox.Show("桌面程序界面绘制", "测试");
		}
	}
}
```

### 再次运行项目

![带图片的项目运行](WinFrom桌面应用程序.assets/image-20191117182844635.png)

### SharpDevelop常见问题

#### SharpDevelop打开新版本Visual Studio

使用SharpDevelop打开新版本Visual Studio建立的项目或者解决方案，会提示类似如下信息：

> The tools version "15.0" is unrecognized. Available tools versions are "4.0".  C:\Dev\CSharp--SerialPort\MySerialPort\MySerialPort\MySerialPort.csproj

可以将MySerialPort.csproj文件打开，然后将配置文件中的`ToolsVersion="15.0"`修改成`ToolsVersion="4.0"`即可

```xml
<?xml version="1.0" encoding="utf-8"?>
<Project ToolsVersion="15.0" xmlns="http://schemas.microsoft.com/developer/msbuild/2003">
    <!-- 中间配置略过…… -->
</Project>
```

#### OutputPath

> The OutputPath property is not set for project 'OperateCamera.csproj'.  Please check to make sure that you have specified a valid combination of Configuration and Platform for this project.  Configuration='Debug'  Platform='AnyCPU'.  This error may also appear if some other project is trying to follow a project-to-project reference to this project, this project has been unloaded or is not included in the solution, and the referencing project does not build using the same or an equivalent Configuration or Platform.

![](WinFrom桌面应用程序.assets/image-20210321163805902.png)

#### 不支持64位debug

![](WinFrom桌面应用程序.assets/image-20210321165455778.png)

根据提示进行设置

![](WinFrom桌面应用程序.assets/image-20210321165609473.png)

## 将代码移植到.NET Core

打开Form1.Designer.cs、TestWFCore\Form1.cs文件，将SharpDevelop中代码移植过去

### Form1.resx

创建Form1.resx文件，直接把相关代码拷贝过来

### Form1.Designer.cs

```csharp
namespace TestWFCore
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox1;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Text = "Form1";            
            
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.button1 = new System.Windows.Forms.Button();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
            // 
            // button1
            //             
            this.button1.Location = new System.Drawing.Point(13, 13);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "测试按钮";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1Click);
            this.Controls.Add(this.button1);
            // 
			// pictureBox1
			// 
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(13, 64);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(129, 70);
			this.pictureBox1.TabIndex = 1;
			this.pictureBox1.TabStop = false;
            this.Controls.Add(this.pictureBox1);
        }

        #endregion
    }
}
```

### Form1.cs

```csharp
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestWFCore
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        void Button1Click(object sender, EventArgs e)
		{
			MessageBox.Show("桌面程序界面绘制", "测试");
		}

    }
}
```

移植代码后既可以运行

![将代码移植到.NET Core](WinFrom桌面应用程序.assets/image-20191117183546686.png)

---

![](WinFrom桌面应用程序.assets/幻灯片2.png)
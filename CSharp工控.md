# CSharp工控

## 使用结构体

### 先引用命名空间

```c#
using System.Runtime.InteropServices;
```

### 在public partial class Form1 : Form中声明结构体类型

```c#
[StructLayout(LayoutKind.Sequential)]  //不写这个也行，默认的内存排列就是Sequential,也就是按成员的先后顺序排列.
public struct PVCI_INIT_CONFIG
{
	public uint AccCode;
	public uint AccMask;
	public uint Reserved;
	public byte Filter;
	public byte kCanBaud;
	public byte Timing0;
	public byte Timing1;
	public byte Mode;
	public byte CanRx_IER;
}
```

### 在需要使用的地方定义

形式1

```c#
s x;
x.a = 0x55;
x.b = 0x66;
```

 形式2：

```c#
PVCI_INIT_CONFIG[] config = new PVCI_INIT_CONFIG[1];  //定义一个结构体数组
config[0].AccCode = 0x80000008;
config[0].AccMask = 0xFFFFFFFF;
config[0].Reserved = 204;
config[0].Filter = 0;
config[0].kCanBaud = 12;  //500k
config[0].Timing0 = 0x00;
config[0].Timing1 = 0x1C;
config[0].CanRx_IER = 1; //Enable CAN reveive
config[0].Mode = 0;
```

## C#获得时间和路径    

```c#
Console.WriteLine(DateTime.Now .ToString ()+":"+DateTime.Now.Millisecond .ToString()); //时间，精确到毫秒
Console.WriteLine(Environment.CurrentDirectory.ToString());  //当前路径
Console.WriteLine(Environment.TickCount.ToString());  //获得系统启动经过的毫秒数
System.Threading.Thread.Sleep(1000);  //暂停1秒
```

## 转换

### 字节转化为单精度浮点数

```c#
byte[] retdata = new byte[] { 0x00, 0x00, 0x7A, 0x43 }; //43 7A 00 00 是250，在这里要倒过来写
byte[] r;
r = new byte[4];
r[3] = 0x43;
r[2] = 0xac;
r[1] = 0x00;
r[0] = 0x00;
Console.WriteLine(BitConverter.ToSingle(r, 0).ToString());
```

### 单精度转成字节

```c#
float floatvalue;
floatvalue = 2500;
byte[] b = BitConverter.GetBytes(floatvalue);
Console.WriteLine(Convert.ToString(b[3], 16) + " " + Convert.ToString(b[2], 16) + " " + Convert.ToString(b[1], 16) + " " + Convert.ToString(b[0], 16));
```

### ASCII码字符转成16进制数

在串口通信有实际应用

* 如9的ASCII码是57，将9的ASCII传进函数，然后57-0x30就是数字9
* 再如F的ASCII码是70，70-'A'=70-65=5，然后5+10就是15也就是F表示的数字

```C
public int HexChar(char c)
{
    if ((c >= '0') && (c <= '9'))
        return c - 0x30;
    else if ((c >= 'A') && (c <= 'F'))
        return c - 'A' + 10;
    else if ((c >= 'a') && (c <= 'f'))
        return c - 'a' + 10;
    else
        return 0x10;
}
//这个将FE之类的字符串转成数字
public int Str2Hex(string str)
{
    int len = str.Length;
    if (len == 2)
    {
        int a = HexChar(str[0]);
        int b = HexChar(str[1]);
        if (a == 16 || b == 16)
        {
            Console.WriteLine("format error！");
            return 256;
        }
        else
        {
            return a * 16 + b;
        }
    }
    else
    {
        Console.WriteLine("len must be 2");
        return 256;
    }
}
Program program = new Program();
//可以这样使用，强制转换为字节型，并以字符串显示为254
Console.WriteLine(Convert.ToString((byte)program.Str2Hex("FE")));
//这样就显示FE了
Console.WriteLine(Convert.ToString((byte)program.Str2Hex("FE"), 16));
```

### 字符转ASCII码，ASCII码转字符 

#### 字符转ASCII码

```c#
public static int Asc(string character)
{
    if (character.Length == 1)
    {
        System.Text.ASCIIEncoding asciiEncoding = new System.Text.ASCIIEncoding();
        int intAsciiCode = (int)asciiEncoding.GetBytes(character)[0];
        return (intAsciiCode);
    }
    else
    {
        throw new Exception("Character is not valid.");
    }

}
```

#### ASCII码转字符： 

单个字符

```c#
public static string Chr(int asciiCode)
{
    if (asciiCode >= 0 && asciiCode <= 255)
    {
        System.Text.ASCIIEncoding asciiEncoding = new System.Text.ASCIIEncoding();
        byte[] byteArray = new byte[] { (byte)asciiCode };
        string strCharacter = asciiEncoding.GetString(byteArray);
        return (strCharacter);
    }
    else
    {
        throw new Exception("ASCII Code is not valid.");
    }

}
```

Excel专用

```c#
public static string ChrExcel(int asciiCode)
{
    if (asciiCode > 0 && asciiCode <= 255)
    {
        System.Text.ASCIIEncoding asciiEncoding = new System.Text.ASCIIEncoding();
        string strCharacter = string.Empty;
        byte[] byteArray = null;
        int division = (asciiCode - 64) / 26;
        int mod = (asciiCode - 64) % 26;
        if (mod == 0)
        {
            division = division - 1;
            mod = 26;
        }

        if ((division == 0) && (mod <= 26))
        {
            byteArray = new byte[] { (byte)(mod + 64) };
            strCharacter = strCharacter + asciiEncoding.GetString(byteArray);
        }
        else
        {
            byteArray = new byte[] { (byte)(division + 64) };
            strCharacter = strCharacter + asciiEncoding.GetString(byteArray);

            byteArray = new byte[] { (byte)(mod + 64) };
            strCharacter = strCharacter + asciiEncoding.GetString(byteArray);
        }

        return strCharacter;
    }
    else
    {
        return "ASCII Code is not valid.";
    }
}
```

###  字符串转成字符数组

```c#
string ss;
string txt = "";
ss = "hello world";
char[] sx = ss.ToCharArray();
int i;
for (i = 0; i < sx.Length; i++)
{
    txt = txt + " " + sx[i];
}
Console.WriteLine(txt);
```

### 整形数据与字节数组相互转换

例如：吉阳CAN的ID号移位问题

```c#
int id;
id = 0x088090C1 << 3;
byte[] sID = BitConverter.GetBytes(id);
Console.WriteLine(Convert.ToString(sID[3], 16) + " " + Convert.ToString(sID[2], 16) + " " + Convert.ToString(sID[1], 16) + " " + Convert.ToString(sID[0], 16) + " ");
//将字节数组组合成32位整形
int rID = 0x00000000;
rID = (sID[3] << 24) | (sID[2] << 16) | (sID[1] << 8) | sID[0];
rID = rID >> 3;
```

### ASCII码的使用，适用于串口通信

```c#
// ASCII码的使用，适用于串口通信
char x = (char)59;     //ASCII码是59
Console.WriteLine("hello" + x.ToString() + (char)32 + "world");  //ASCII码32是空格
//写成数组形式
char[] str = { (char)45, (char)48, (char)100 };  //(char)77 就是ASCII码77的 强制转为字符
Console.WriteLine(str[0].ToString() + str[1].ToString() + str[2].ToString()); //用在串口发送上比较合适
```

### 案例

```sh
 dotnet new console --output sampleConverter
 dotnet run --project sampleConverter
```

##  C#与C++交互

有些DLL有依赖文件，例如：吉阳CANUSB的，和DLL相关的有两个VCI_CAN文件和一个SiUSBXp.dll文件。将所有相关文件拷贝到bin\debug中和exe文件在一个文件中。

```c#
using System.Runtime.InteropServices;
[DllImport("VCI_CAN.dll",EntryPoint = "VCI_OpenDevice")]
public static extern int VCI_OpenDevice(uint Devtype, uint Devindex, uint Reserv3);
```

**C#与C++交互，总体来说可以有两种方法：**

1. 利用C++/CLI作为代理中间层

2. 利用PInvoke实现直接调用

* **第一种方法**：实现起来比较简单直观，并且可以实现C#调用C++所写的类，但是问题是MONO架构不支持C++/CIL功能，因此无法实现脱离Microsoft.NET Framework跨平台运行。
*  **第二种方法**：简单的实现并不麻烦，只要添加DllImportAttribute特性即可导入C++的函数，但是问题是PInvoke不能简单的实现C++类的调用。

> :warning:**注意事项**:
>
>  PInvoke从功能上来说，只支持函数调用，在被导出的函数前面一定要添加`rxtern “C`来指明导出函数的时候使用C语言方式编译和链接的，这样保证函数定义的名字相同，否则如果默认按C++方式导出，那个函数名字就会变得乱七八糟，我们的程序就无法找到入口点了。

### 互调的基本原理

首先，我们-来看一个在常规不过的概念“数据类型”。在大多数的静态语言中定义变量的时候都要先指定其数据的类型，所谓数据类型，都是人们加强的一个便于记忆的名称，究其本质就是指明了这个数据在内存中到底占用了几个字节，程序在运行时，首先找到这个数据的地址，然后在按着该类型的长度，读取相应的内存，然后再做处理。了解了前面，所有编程语言之间进行互调就有眉目了，对于不同的语言之间的互调，只要将该数据的指针（内存地址）传递给另一个语言，在另一个语言中根据通信协议将指针所指向的数据存入长度对应的数据类型即可，当然要满足以下几点。

1. 对于像Java，.NET这样有运行时虚拟机编程语言来讲，由于虚拟机会让堆内存来回转移，因此，在进行互调的时候要保证正在被互调的数据所在的内存一定要固定，不能被转移。

2. 有一些编程语言支持指针，有一些编程语言不支持指针（如Java），这个问题并不重要，所谓指针，其实就是一个内存地址，对于32位os的指针是一个32位整数，而对于64位机os的指针是一个64位整数。因为大多数语言中都有整形数，所以可以利用整形来接收指针。

### 基本数据类型的传递

 互调过程中，最基本要传递的无非是数值和字符，即：int、long、float、char等等，但是此类型非彼类型，C/C++与C#中有一些数据类型长度是不一样的，下表中列出常见数据类型的异同：

| **C/C++**                                      | **C#** | **长度** |
| ---------------------------------------------- | ------ | -------- |
| short                                          | short  | 2Bytes   |
| int                                            | int    | 4Bytes   |
| long(该类型在传递的时候常常会弄混)             | int    | 4Bytes   |
| bool                                           | bool   | 1Byte    |
| char(Ascii码字符)                              | byte   | 1Byte    |
| wchar_t（Unicode字符，该类型与C#中的Char兼容） | char   | 2Bytes   |
| float                                          | float  | 4Bytes   |
| double                                         | double | 8Bytes   |

> 最容易弄混的是就是long、char两个类型，在C/C++中long和int都是4个字节，都对应着C#中的int类型，而C/C++中的char类型占一个字节，用来表示一个ASCII码字符，在C#中能够表示一个字节的是byte类型。与C#中char类型对应的应该是C/C++中的wchar_t类型，对应的是一个2字节的Unicode字符。

### 案例解决方案TestCPPDLL

#### 新建 C++ Win32项目，类型为DLL。

![创建项目TestCPPDLL](CSharp工控.assets/image-20210309162643769.png)

#### IDE自动生成如下文件

![](CSharp工控.assets/image-20210309162857195.png)

#### 定义TestCPPDLL.h，新增该头文件

```c++
#define TESTCPPDLL_API __declspec(dllexport)

EXTERN_C TESTCPPDLL_API int __stdcall Add(int a,int b);

EXTERN_C TESTCPPDLL_API void __stdcall WriteString(wchar_t*content);

//传入一个整型指针，将其所指向的内容加1

EXTERN_C TESTCPPDLL_API void __stdcall AddInt(int *i);

//传入一个整型数组的指针以及数组长度，遍历每一个元素并且输出

EXTERN_C TESTCPPDLL_API void __stdcall AddIntArray(int *firstElement,int arraylength);

//在C++中生成一个整型数组，并且数组指针返回给C#

EXTERN_C TESTCPPDLL_API int* __stdcall GetArrayFromCPP();

 

//定义一个函数指针

typedef void (__stdcall *CPPCallback)(int tick);

//定义一个用于设置函数指针的方法,

//并在该函数中调用C#中传递过来的委托

EXTERN_C TESTCPPDLL_API void __stdcall SetCallback(CPPCallback callback);

 

struct Vector3

{

    float X,Y,Z;

};

EXTERN_C TESTCPPDLL_API void __stdcall SendStructFromCSToCPP(Vector3 vector);
```

第一行代码中定义了一个名为`"TESTCPPDLL_API"`的宏，该宏对应的内容是`"__declspec(dllexport)"`意思是将后面修饰的内容定义为DLL中要导出的内容。当然你也可以不使用这个宏，可以直接将`"__declspec(dllexport)"`写在要导出的函数前面。

 第二行中的`"EXTERN_C"`，是在`"winnt.h"`中定义的宏，在函数前面添加`"EXTERN_C"`等同于在函数前面添加`extern "C"`，意思是该函数在编译和连接时使用C语言的方式，以保证函数名字不变。第二行的代码是一个函数的声明，说明该函数可以被模块外部调用，其定义实现在dllmain.cpp中。

#### 我们可以在dllmain.cpp中进行代码编辑

```c++
// dllmain.cpp : 定义 DLL 应用程序的入口点。
#include "pch.h"

#include <iostream>

#include "TestCPPDLL.h"

using namespace std;

TESTCPPDLL_API int __stdcall Add(int a, int b)

{

	return a + b;

}

TESTCPPDLL_API void __stdcall WriteString(wchar_t* content)

{

	wprintf(content);

	printf("\n");

}



TESTCPPDLL_API void __stdcall AddInt(int* i)

{

	(*i)++;

}



TESTCPPDLL_API void __stdcall AddIntArray(int* firstElement, int arrayLength)

{

	int* currentPointer = firstElement;

	for (int i = 0; i < arrayLength; i++)

	{

		cout << *currentPointer;

		currentPointer++;

	}

	cout << endl;

}

int* arrPtr;

TESTCPPDLL_API int* __stdcall GetArrayFromCPP()

{

	arrPtr = new int[10];



	for (int i = 0; i < 10; i++)

	{

		arrPtr[i] = i;

	}



	return arrPtr;

}



TESTCPPDLL_API void __stdcall SetCallback(CPPCallback callback)

{

	int tick = 100;

	//下面的代码是对C#中委托进行调用

	callback(tick);

}



TESTCPPDLL_API void __stdcall SendStructFromCSToCPP(Vector3 vector)

{

	cout << "got vector3 in cpp,x:";

	cout << vector.X;

	cout << ",Y:";

	cout << vector.Y;

	cout << ",Z:";

	cout << vector.Z;

}
```

> :zap: **预编译头pch.h**
>
> 在许多 Visual Studio c + + 项目类型中，默认情况下启用此选项。 此选项指定的默认包含文件为 *pch* 或 Visual Studio 2017 及更早版本中的 *stdafx.h* 

#### 添加一个C#的应用程序

首先，添加一个C#的应用程序，如果要在C#中调用C++的DLL文件，先要在C#的类中添加一个静态方法，并且使用DllImportAttribute对该方法进行修饰，代码如下所示

![](CSharp工控.assets/image-20210309193819299.png)

添加完成后的解决方案情况

![](CSharp工控.assets/image-20210309193932998.png)

引入System.Runtime.InteropServices

```c#
using System;
using System.Runtime.InteropServices;

namespace ConsoleTestApp
{
    class Program
    {
        [DllImport(@"Libraries\TestCPPDLL.dll", EntryPoint = "Add")]
        extern static int Add(int a, int b);
        static void Main(string[] args)
        {
            int c = Add(1, 2);
            Console.WriteLine(c);
            Console.Read();
        }
    }
}

```

> **.NET字符串前面加@**
>
> 让转移字符""保持原意，不要转义，
> `string path="c:\ex";`
> 默认的`"\"`是作为转义来使用的，而不是一个真的斜杠字符串
> 正确的写法应该是：
> `string path=@"c:\ex";`
> @让斜杠保持原意，不要转义

#### 修改C#的项目属性

将platform target设置为x86，并且允许非安全代码（后面有用）。

![](CSharp工控.assets/image-20210309200340049.png)

#### 设置生成事件

在本项目中新建Libraries目录（bin\Debug\netcoreapp3.1\Libraries\，根据具体路径修改），在生成前置事件中，将C++项目生成的DLL拷贝到本项目的Libraries目录中

![](CSharp工控.assets/image-20210309194911484.png)

```sh
copy /Y "$(SolutionDir)Debug\TestCPPDLL.dll" "$(ProjectDir)$(OutDir)Libraries\TestCPPDLL.dll"
```

#### 运行程序

将ConsoleTestApp设置为启动项目，运行等于3。

![](CSharp工控.assets/image-20210309200447190.png)

### TestCPPDLL传递字符串

前面的Add方法中传递的是数值类型（int），其他的数据类型，如float,double,和bool类型的传递方式是一样的，下面演示如何传递字符串。

在TestCPPDLL.h中添加一个新的函数声明，代码如下：

```c++
EXTERN_C TESTCPPDLL_API void __stdcall WriteString(wchar_t*content); 
```

这里的参数是wchar_t类型的指针，对应着C#中的char类型。TestCPPDLL.cpp中添加如下代码：

```c++
TESTCPPDLL_API void __stdcall WriteString(wchar_t*content) 
{ 
    cout<<content; 
} 
```

该代码的功能就是将输入的字符串通过C++在控制台上输出。下面是在C#中的声明:

```c#
[DllImport(@"Libraries\TestCPPDLL.dll", EntryPoint = "WriteString")]
extern unsafe static void WriteString(char*c); 
```

调用过程如下所示：

```c#
unsafe
{
    //在传递字符串时，将字符所在的内存固化，
    //并取出字符数组的指针
    fixed (char* p = &("hello".ToCharArray()[0]))
    {
        //调用方法
        WriteString(p);
    }
}
```

得到结果

```sh
3
hello
```

### 指针的传递

根据前面介绍的数据类型对照表，我们可以直接在方法中传递指针，但是要注意的是我们常常需要将数组的指针（数据入口地址，第一个元素的地址），数据从C/C++到C#时问题不大，但是如果从C#到C/C++时一定要将数组先固化，然后再传递处理。

下面演示如何传递指针，首先在TestCPPDLL.h中添加下列声明：

```c++
//传入一个整型指针，将其所指向的内容加1
EXTERN_C TESTCPPDLL_API void __stdcall AddInt(int *i); 
//传入一个整型数组的指针以及数组长度，遍历每一个元素并且输出
EXTERN_C TESTCPPDLL_API void __stdcall AddIntArray(int *firstElement,int arraylength); 
//在C++中生成一个整型数组，并且数组指针返回给C#
EXTERN_C TESTCPPDLL_API int* __stdcall GetArrayFromCPP(); 
其实现写在TestCPPDLL.cpp中，代码如下所示：
TESTCPPDLL_API void __stdcall AddInt(int *i) 
{ 
    (*i)++; 
} 
TESTCPPDLL_API void __stdcall AddIntArray(int *firstElement,int arrayLength) 
{ 
    int*currentPointer=firstElement; 
    for (int i = 0; i < arrayLength; i++) 
    { 
        cout<<*currentPointer; 
        currentPointer++; 
    } 
    cout<<endl; 
} 
 
int *arrPtr; 
TESTCPPDLL_API int* __stdcall GetArrayFromCPP() 
{ 
    arrPtr=new int[10]; 
     
    for (int i = 0; i < 10; i++) 
    { 
        arrPtr[i]=i; 
    } 
     
    return arrPtr; 
}
```

对应调用的C#代码如下所示：

```c#
[DllImport(@"Libraries\TestCPPDLL.dll", EntryPoint = "AddInt")] 
extern unsafe static void AddInt(int* i); 
[DllImport(@"Libraries\TestCPPDLL.dll", EntryPoint = "AddIntArray")] 
extern unsafe static void AddIntArray(int* firstElement, int arraylength); 
[DllImport(@"Libraries\TestCPPDLL.dll", EntryPoint = "GetArrayFromCPP")] 
extern unsafe static int* GetArrayFromCPP();
```

调用过程如下所示：

```c#
unsafe
{ 
    // 调用C++中的AddInt方法
    int i = 10; 

    AddInt(&i); 
    Console.WriteLine(i); 

    //调用C++中的AddIntArray方法将C#中的数据传递到C++中，并在C++中输出
    int[] CSArray = new int[10]; 
    for (int iArr = 0; iArr < 10; iArr++) 
    { 
        CSArray[iArr] = iArr; 
    } 
    fixed (int* pCSArray = &CSArray[0]) 
    { 
        AddIntArray(pCSArray, 10); 
    } 
    //调用C++中的GetArrayFromCPP方法获取一个C++中建立的数组
    int* pArrayPointer = null; 
    pArrayPointer = GetArrayFromCPP(); 
    for (int iArr = 0; iArr < 10; iArr++) 
    { 
        Console.WriteLine(*pArrayPointer); 
        pArrayPointer++; 
    } 
}
```

得到结果

```sh
3
hello
11
0123456789
0
1
2
3
4
5
6
7
8
9
```

### 函数指针的传递

前面说明的都是简单数据类型的及其指针的传递，利用PInvoke我们也可以实现函数指针的传递，C#中并没有函数指针的概念，但是可以使用委托（delegate）来代替函数指针。

大家可能会问，为什么要传递函数指针呢？利用PInvoke可以实现C#对C/C++函数的调用，反过来，我们能不能在C/C++程序运行的某一时刻，来调用一个C#对应的函数呢？（例如在C++中存在一个独立线程，该线程可能在任意时刻触发一个事件，并且需要通知C#）。这个时候，我们就有必要将一个C#中已经指向某一个函数的函数指针（委托）传递给C++。

想要传递函数指针，首先要在C#中定义一个委托，并且在C++中定义一个函数指针，同时要保证委托和函数指针具备相同的函数原型，我们首先编写C#的代码，如下所示：

```c#
//定义一个委托，返回值为空，存在一个整型参数
public delegate void CSCallback(int tick); 
//定义一个用于回调的方法，与前面定义的委托的原型一样
//该方法会被C++所调用
static void CSCallbackFunction(int tick) 
{ 
    Console.WriteLine(tick.ToString ()); 
} 
//定义一个委托类型的实例，
//在主程序中该委托实例将指向前面定义的CSCallbackFunction方法
static CSCallback callback; 
```

在CS的主程序中让callback指向CSCallbackFunction方法，代码如下所示：

```c#
//调用委托所指向的方法
callback = CSCallbackFunction;
```

然后在C/C++中定义一个函数指针，并且添加一个用于设置函数指针的函数，TestCPPDLL.h中的代码如下所示：

```c++
//定义一个函数指针
typedef void (__stdcall *CPPCallback)(int tick); 
//定义一个用于设置函数指针的方法,
//并在该函数中调用C#中传递过来的委托
EXTERN_C TESTCPPDLL_API void SetCallback(CPPCallback callback); 
```

SetCallback函数的实现在TestCPPDLL.cpp中，代码如下所示：

```c++
TESTCPPDLL_API void SetCallback(CPPCallback callback) 
{ 
    int tick=rand(); 
    //下面的代码是对C#中委托进行调用
    callback(tick); 
} 
```

在C#中添加SetCallback函数的声明，代码如下所示：

```c#
//这里使用CSCallback委托类型来兼容C++里的CPPCallback函数指针
[DllImport(@"Libraries\TestCPPDLL.dll", EntryPoint = "SetCallback")] 
extern static void SetCallback(CSCallback callback);
```

在C#中的调用过程如下所示：

```c#
//让委托指向将被回调的方法
callback = CSCallbackFunction; 
//将委托传递给C++
SetCallback(callback); 
```

SetCallback方法被执行后，在C#中定义的CSCallbackFunction就会被C++所调用。

得到结果

```sh
3
hello
11
0123456789
0
1
2
3
4
5
6
7
8
9
100
```

### 结构体的传递

传递结构体的想法和传递一个int类型数据类似，struct中的数据是在内存中顺序排列的，只要保证保证以下几点，就可以直接传递结构体，甚至是结构体的指针：

- 要传递的成员为公有的值类型字段
- C#中结构体字段类型与C++结构体中的字段类型相兼容
- C#结构中的字段顺序与C++结构体中的字段顺序相同，要保证该功能，需要将C#结构体标记为[StructLayout( LayoutKind.Sequential)] 

下面通过代码进行说明，首先在C#中添加一个结构体，代码如下所示：

```c++
[StructLayout( LayoutKind.Sequential)] 
struct Vector3
{ 
    public float X, Y, Z; 
} 
```

该结构体表示一个3D向量，包括X，Y，Z三个float类型的分量。

然后在TestCPPDLL.h中也定义一个相同结构的结构体，代码如下所示：

```c++
struct Vector3
{ 
    float X,Y,Z; 
}; 
```

在TestCPPDLL.h中声明一个用于传递Vector3结构体的一个函数，代码如下所示：

```c++
EXTERN_C TESTCPPDLL_API void __stdcall SendStructFromCSToCPP(Vector3 vector); 
```

在TestCPPDLL.cpp中将其实现，代码如下所示：

```c++
TESTCPPDLL_API void __stdcall SendStructFromCSToCPP(Vector3 vector) 
{ 
    cout<<"got vector3 in cpp,x:"; 
    cout<<vector.X; 
    cout<<",Y:"; 
    cout<<vector.Y; 
    cout<<",Z:"; 
    cout<<vector.Z; 
}
```

在C#中添加对SendStructFromCSToCPP函数的声明，代码如下所示：

```c#
[DllImport(@"Libraries\TestCPPDLL.dll", EntryPoint = "SendStructFromCSToCPP")] 
extern static void SendStructFromCSToCPP(Vector3 vector); 
```

C#中的调用过程如下所示：

```c#
//建立一个Vector3的实例
Vector3 vector = new Vector3() { X =10,Y=20,Z=30 }; 
//将vector传递给C++并在C++中输出
SendStructFromCSToCPP(vector); 
```

得到结果

```sh
3
hello
11
0123456789
0
1
2
3
4
5
6
7
8
9
100
got vector3 in cpp,x:10,Y:20,Z:30
```

### 完整的C#代码

```c#
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;


namespace ConsoleApplication1
{
    class Program
    {

        [DllImport(@"Libraries\TestCPPDLL.dll", EntryPoint = "Add")]
        extern static int Add(int a, int b);
        [DllImport(@"Libraries\TestCPPDLL.dll", EntryPoint = "WriteString")]
        extern unsafe static void WriteString(char* c);
        [DllImport(@"Libraries\TestCPPDLL.dll", EntryPoint = "AddInt")]
        extern unsafe static void AddInt(int* i);
        [DllImport(@"Libraries\TestCPPDLL.dll", EntryPoint = "AddIntArray")]
        extern unsafe static void AddIntArray(int* firstElement, int arraylength);
        [DllImport(@"Libraries\TestCPPDLL.dll", EntryPoint = "GetArrayFromCPP")]
        extern unsafe static int* GetArrayFromCPP();



        //定义一个委托，返回值为空，存在一个整型参数
        public delegate void CSCallback(int tick);
        //定义一个用于回调的方法，与前面定义的委托的原型一样
        //该方法会被C++所调用
        static void CSCallbackFunction(int tick)
        {
            Console.WriteLine(tick.ToString());
        }
        //定义一个委托类型的实例，
        //在主程序中该委托实例将指向前面定义的CSCallbackFunction方法
        static CSCallback callback;
        //这里使用CSCallback委托类型来兼容C++里的CPPCallback函数指针
        [DllImport(@"Libraries\TestCPPDLL.dll", EntryPoint = "SetCallback")]
        extern static void SetCallback(CSCallback callback);
        [StructLayout(LayoutKind.Sequential)]
        struct Vector3
        {
            public float X, Y, Z;
        }
        [DllImport(@"Libraries\TestCPPDLL.dll", EntryPoint = "SendStructFromCSToCPP")]
        extern static void SendStructFromCSToCPP(Vector3 vector);
        static void Main(string[] args)
        {
            int c = Add(1, 2);
            Console.WriteLine(c);
            //因为使用指针，因为要声明非安全域
            unsafe
            {
                //在传递字符串时，将字符所在的内存固化，
                //并取出字符数组的指针
                fixed (char* p = &("hello".ToCharArray()[0]))
                {
                    //调用方法
                    WriteString(p);
                }
            }
            unsafe
            {
                // 调用C++中的AddInt方法
                int i = 10;
                AddInt(&i);
                Console.WriteLine(i);
                //调用C++中的AddIntArray方法将C#中的数据传递到C++中，并在C++中输出
                int[] CSArray = new int[10];
                for (int iArr = 0; iArr < 10; iArr++)
                {
                    CSArray[iArr] = iArr;
                }
                fixed (int* pCSArray = &CSArray[0])
                {
                    AddIntArray(pCSArray, 10);
                }
                //调用C++中的GetArrayFromCPP方法获取一个C++中建立的数组
                int* pArrayPointer = null;
                pArrayPointer = GetArrayFromCPP();
                for (int iArr = 0; iArr < 10; iArr++)
                {
                    Console.WriteLine(*pArrayPointer);
                    pArrayPointer++;
                }
            }
            //让委托指向将被回调的方法callback = CSCallbackFunction;
            //将委托传递给C++
            callback = CSCallbackFunction;
            SetCallback(callback);
            //建立一个Vector3的实例
            Vector3 vector = new Vector3() { X = 10, Y = 20, Z = 30 };
            //将vector传递给C++并在C++中输出
            SendStructFromCSToCPP(vector);
            Console.Read();
        }
    }
}
```


![](Visual Studio Code 开发.NET Core应用程序.assets/幻灯片0.png)

# Visual Studio Code 开发.NET Core应用程序

开源和跨平台开发是Microsoft 的当前和将来至关重要的策略。.NET Core已开源，同时开发了其他项来使用和支持新的跨平台策略。.NET Core 3.0 目前已经正式发布，是适用于针对 Web 和云构建跨平台应用程序的最新开源技术，可在 Linux、Mac OS X 和 Windows 上运行。

.NET Core使用各种命令行工具来生成基架、构建和运行应用程序，同时可以使用 Visual Studio Code 进行编辑。

Visual Studio Code 是微软为广大开发人员提供的免费开源的跨平台代码编辑器，和其它流行的代码编辑器，如：Sublime, Atom一样，它非常小，运行速度快，同时通过各种插件支持不同开发语言的编写。不同的地方在于，VSC的插件不仅仅提供静态的语言高亮，自动语法检测和完成功能外；还提供更加高级的编译器服务支持，这使得VSC可以在一定程度上替代IDE的功能，进行代码的编译，调试和发布操作。

## 先决条件

### 任务1：安装Visual Studio Code和.NET Core

下载Visual Studio Code ，从https://code.visualstudio.com/ 下载最新版本并安装

![安装Visual Studio Code](Visual Studio Code 开发.NET Core应用程序.assets/home-screenshot-win-lg.png) 

从  https://dotnet.microsoft.com/download  下载.NET Core进行安装

 ![img](Visual Studio Code 开发.NET Core应用程序.assets/downloads-dot-net-core.svg) 

Node.JS和NPM，以及 bower, gulp 和 grunt 等前端工具, Node.js是一个javascript的运行引擎，提供服务端的javascript运行能力，同时也包含了npm这个包管理器，可以用来安装 bower, glup，grunt等前端工具。下载地址：[http://nodejs.org](http://nodejs.org/)

安装完成后，让通过以下命令安装前端工具

```bash
npm install bower gulp grunt-cli -g
```

### 任务2：安装插件

安装好Visual Studio Code 之后，需要安装下面插件： C# for Visual Studio Code (powered by OmniSharp)

> 新版的Visual Studio Code已经很智能了，打开相关文件，就会提示你安装对应的插件。

## 使用命令行界面构建. NET Core应用程序

.NET Core CLI 是开发 .NET Core 应用程序的一个新的跨平台工具链的基础。它是“基础”的原因时它是在其它的、高级别工具的主要层，如集成开发环境（IDEs），由编辑器和构建者组成。

默认它是跨平台的，并且对支持的每个平台有相同的表现范围。这意味着，当你学会如何使用工具，你可以从任何支持的平台上以同样的方式使用它。

本练习中现在我们假设你已经安装好了VS Code开发工具、.Net Core 3.0 SDK，并且已经为VS Code安装好了C#扩展。

1. 我们先在我们的电脑硬盘的新建一个文件夹。创建的文件夹名称为HelloWorld。注意，这一步不是在VS Code中完成的，VS Code中不能创建文件夹。
2. 在VS Code开发环境中，选择 文件->打开文件夹，然后选择我们刚刚创建文件夹HelloWorld打开
3. 选择 查看->集成终端 命令或直接摁下快捷键<kbd>Ctrl</kbd>  +  <kbd>&#96;</kbd>，VS Code开发环境中会出现一个集成的终端。比如我接下来在集成终端中输入命令`dotnet new sln -n HelloWorld`，在我们的HelloWorld文件夹下会出现一个解决方案HelloWorld.sln

接下来，我们再在集成终端中输入`dotnet new consol`，经过VS Code一阵的挣扎和折腾，我们会发现左边的文件列表中多了一个叫HelloWorld的console项目。如下图： 

![dotnet new consol](Visual Studio Code 开发.NET Core应用程序.assets/image-20191117153655735.png)

按下F5，选择.NET Core, 出来一个launch.json，如下图： 

![launch.json](Visual Studio Code 开发.NET Core应用程序.assets/image-20191117154057591.png)

```json
{
  // 使用 IntelliSense 了解相关属性。 
  // 悬停以查看现有属性的描述。
  // 欲了解更多信息，请访问: https://go.microsoft.com/fwlink/?linkid=830387
  "version": "0.2.0",
  "configurations": [
    {
      "name": ".NET Core Launch (console)",
      "type": "coreclr",
      "request": "launch",
      "preLaunchTask": "build",
      "program": "${workspaceFolder}/bin/Debug/netcoreapp3.0/HelloWorld.dll",
      "args": [],
      "cwd": "${workspaceFolder}",
      "console": "internalConsole",
      "stopAtEntry": false
    },
    {
      "name": ".NET Core Attach",
      "type": "coreclr",
      "request": "attach",
      "processId": "${command:pickProcess}"
    }
  ]
}
```

再次按下F5，这是由于默认启动的是 .NET Core Launch (console)

![.NET Core Launch (console)](Visual Studio Code 开发.NET Core应用程序.assets/image-20191117154209025.png)

## 回顾命令

```bash
# 创建解决方案工程
dotnet new sln
# 创建库
dotnet new classlib
# 创建控制台应用
dotnet new console
# 将某个工程添加到解决方案中
dotnet sln add ./XXXX/XXXXX.csproj
# 创建测试单元
dotnet new xunit
# 在某个工程中添加对另一个工程的引用
dotnet add reference ../XXXX/XXXX.csproj
```

## 添加一个Nuget包

### 引入Nuget包Newtonsoft.Json

下面我们演示给项目添加一个Nuget包，打开 https://www.nuget.org/ 

在输入框中输入最常用的包json，页面会调整到 https://www.nuget.org/packages?q=json 

选中排名第一的 [Newtonsoft.Json](https://www.nuget.org/packages/Newtonsoft.Json/)  

![Newtonsoft.Json](Visual Studio Code 开发.NET Core应用程序.assets/image-20191117155617513.png)

打开`HelloWorld.csproj`文件进行修改

源文件

```xml
<Project Sdk="Microsoft.NET.Sdk">

  <PropertyGroup>
    <OutputType>Exe</OutputType>
    <TargetFramework>netcoreapp3.0</TargetFramework>
  </PropertyGroup>

</Project>
```

修改为

```xml
<Project Sdk="Microsoft.NET.Sdk">

  <PropertyGroup>
    <OutputType>Exe</OutputType>
    <TargetFramework>netcoreapp3.0</TargetFramework>
  </PropertyGroup>
  <ItemGroup>
    <PackageReference Include="Newtonsoft.Json" Version="12.0.3" />
  </ItemGroup>
    
</Project>
```

![添加一个Nuget包](Visual Studio Code 开发.NET Core应用程序.assets/image-20191117160155120.png)

提示你要重新执行restore的命令，执行后会发现dll已经被下载`HelloWorld\bin\Debug\netcoreapp3.0\Newtonsoft.Json.dll`

访问包的主页 https://www.newtonsoft.com/json ，获取使用方法示例

### 创建一个Product.cs

```csharp
using System;

namespace HelloWorld
{
  class Product
  {
    public String Name {get; set;}
    public DateTime Expiry {get; set;}
    public String[] Sizes {get; set;}
  }
}
```

### 修改Program.cs

* `using Newtonsoft.Json;`
* `JsonConvert.SerializeObject(product)`
* `Console.WriteLine(json)`

```csharp
using System;
using Newtonsoft.Json;

namespace HelloWorld
{
  class Program
  {
    static void Main(string[] args)
    {
      Console.WriteLine("Hello World!");
      Product product = new Product();
      product.Name = "Apple";
      product.Expiry = new DateTime(2008, 12, 28);
      product.Sizes = new string[] { "Small" };

      string json = JsonConvert.SerializeObject(product);
      Console.WriteLine(json);
    }
  }
}

```

### F5执行JsonConvert

![F5执行JsonConvert](Visual Studio Code 开发.NET Core应用程序.assets/image-20191117161723224.png)

## 使用 Visual Studio Code和 Omnisharp 调试 c# 代码

### 设置断点

![设置断点](Visual Studio Code 开发.NET Core应用程序.assets/image-20191117162019832.png)

### 开始跟踪代码

![开始跟踪代码](Visual Studio Code 开发.NET Core应用程序.assets/image-20191117162056760.png)

### 查看被跟踪的变量

![查看被跟踪的变量](Visual Studio Code 开发.NET Core应用程序.assets/image-20191117162150943.png)

## 创建解决方案

本练习要使用Visual studio code完成一个包含多个项目的解决方案，包括类库和Web项目。结合Visual Studio Code和.NET Core CLI，创建项目结构如下：

```
piedpiper
└── src
├── piedpiper.domain
├── piedpiper.sln
├── piedpiper.tests
└── piedpiper.website
```

### 创建解决方案

首先，我们将创建我们的解决方案（.sln）文件。打开Visual Studio Code的集成终端：

```bash
mkdir src
cd src
dotnet new sln -n piedpiper
```

显示如下结果

```sh
Windows PowerShell
版权所有 (C) Microsoft Corporation。保留所有权利。

尝试新的跨平台 PowerShell https://aka.ms/pscore6


    目录: D:\百度云同步盘\Dev\lessons\.NET\examples\piedpiper


Mode                LastWriteTime         Length Name                                                                                          
----                -------------         ------ ----                                                                                          
d-----       2019/11/17     16:26                src


PS D:\百度云同步盘\Dev\lessons\.NET\examples\piedpiper> cd src
PS D:\百度云同步盘\Dev\lessons\.NET\examples\piedpiper\src> dotnet new sln -n piedpiper
The template "Solution File" was created successfully.
```

这将创建一个sln名为的新文件piedpiper.sln。

### 创建一个项目

```sh
dotnet new mvc -o piedpiper.website
```

显示如下结果

```sh
PS D:\百度云同步盘\Dev\lessons\.NET\examples\piedpiper\src> dotnet new mvc -o piedpiper.website
The template "ASP.NET Core Web App (Model-View-Controller)" was created successfully.
This template contains technologies from parties other than Microsoft, see https://aka.ms/aspnetcore/3.0-third-party-notices for details.      

Processing post-creation actions...
Running 'dotnet restore' on piedpiper.website\piedpiper.website.csproj...
  D:\百度云同步盘\Dev\lessons\.NET\examples\piedpiper\src\piedpiper.website\piedpiper.website.csproj 的还原在 54.24 ms 内完成。

Restore succeeded.
```

这将在同一目录中的piedpiper.website文件夹中创建一个ASP.NET Core MVC应用程序。如果我们目前看到我们的文件夹结构，它看起来像这样：

### 创建其它项目

接下来我们可以为我们的域名和测试项目做同样的事情：

```bash
dotnet new classlib -o piedpiper.domain
dotnet new xunit -o piedpiper.tests
```

### 将项目添加到我们的解决方案中

在这一点上，我们有一个没有引用项目的解决方案文件，我们可以通过调用list命令来验证这一点：

```bash
dotnet sln list
```

> :zap:未在解决方案中找到项目。接下来我们将我们的项目添加到我们的解决方案文件，我们很容易在Visual Studio 2017中打开解决方案，然后手动添加对每个项目的引用。Visual Studio Code也可以通过.NET Core CLI完成。

![将项目添加到我们的解决方案中](Visual Studio Code 开发.NET Core应用程序.assets/image-20191117163338275.png)

现在开始使用以下命令添加每个项目，我们通过引用.csproj文件来执行此操作：

```sh
dotnet sln add piedpiper.website/piedpiper.website.csproj
dotnet sln add piedpiper.domain/piedpiper.domain.csproj
dotnet sln add piedpiper.tests/piedpiper.tests.csproj
```

执行完观察`piedpiper.sln`文件的变化

```yaml
Microsoft Visual Studio Solution File, Format Version 12.00
# Visual Studio 15
VisualStudioVersion = 15.0.26124.0
MinimumVisualStudioVersion = 15.0.26124.0
Project("{FAE04EC0-301F-11D3-BF4B-00C04F79EFBC}") = "piedpiper.website", "piedpiper.website\piedpiper.website.csproj", "{BE35D2C0-6D19-42B5-9765-D586FE02B656}"
EndProject
Project("{FAE04EC0-301F-11D3-BF4B-00C04F79EFBC}") = "piedpiper.domain", "piedpiper.domain\piedpiper.domain.csproj", "{03AB8642-53EC-4407-90B9-0CCA67E13634}"
EndProject
Project("{FAE04EC0-301F-11D3-BF4B-00C04F79EFBC}") = "piedpiper.tests", "piedpiper.tests\piedpiper.tests.csproj", "{6231274B-235A-4F22-962E-DF171F045168}"
EndProject
Global
	GlobalSection(SolutionConfigurationPlatforms) = preSolution
		Debug|Any CPU = Debug|Any CPU
		Debug|x64 = Debug|x64
		Debug|x86 = Debug|x86
		Release|Any CPU = Release|Any CPU
		Release|x64 = Release|x64
		Release|x86 = Release|x86
	EndGlobalSection
	GlobalSection(SolutionProperties) = preSolution
		HideSolutionNode = FALSE
	EndGlobalSection
	GlobalSection(ProjectConfigurationPlatforms) = postSolution
		{BE35D2C0-6D19-42B5-9765-D586FE02B656}.Debug|Any CPU.ActiveCfg = Debug|Any CPU
		{BE35D2C0-6D19-42B5-9765-D586FE02B656}.Debug|Any CPU.Build.0 = Debug|Any CPU
		{BE35D2C0-6D19-42B5-9765-D586FE02B656}.Debug|x64.ActiveCfg = Debug|Any CPU
		{BE35D2C0-6D19-42B5-9765-D586FE02B656}.Debug|x64.Build.0 = Debug|Any CPU
		{BE35D2C0-6D19-42B5-9765-D586FE02B656}.Debug|x86.ActiveCfg = Debug|Any CPU
		{BE35D2C0-6D19-42B5-9765-D586FE02B656}.Debug|x86.Build.0 = Debug|Any CPU
		{BE35D2C0-6D19-42B5-9765-D586FE02B656}.Release|Any CPU.ActiveCfg = Release|Any CPU
		{BE35D2C0-6D19-42B5-9765-D586FE02B656}.Release|Any CPU.Build.0 = Release|Any CPU
		{BE35D2C0-6D19-42B5-9765-D586FE02B656}.Release|x64.ActiveCfg = Release|Any CPU
		{BE35D2C0-6D19-42B5-9765-D586FE02B656}.Release|x64.Build.0 = Release|Any CPU
		{BE35D2C0-6D19-42B5-9765-D586FE02B656}.Release|x86.ActiveCfg = Release|Any CPU
		{BE35D2C0-6D19-42B5-9765-D586FE02B656}.Release|x86.Build.0 = Release|Any CPU
		{03AB8642-53EC-4407-90B9-0CCA67E13634}.Debug|Any CPU.ActiveCfg = Debug|Any CPU
		{03AB8642-53EC-4407-90B9-0CCA67E13634}.Debug|Any CPU.Build.0 = Debug|Any CPU
		{03AB8642-53EC-4407-90B9-0CCA67E13634}.Debug|x64.ActiveCfg = Debug|Any CPU
		{03AB8642-53EC-4407-90B9-0CCA67E13634}.Debug|x64.Build.0 = Debug|Any CPU
		{03AB8642-53EC-4407-90B9-0CCA67E13634}.Debug|x86.ActiveCfg = Debug|Any CPU
		{03AB8642-53EC-4407-90B9-0CCA67E13634}.Debug|x86.Build.0 = Debug|Any CPU
		{03AB8642-53EC-4407-90B9-0CCA67E13634}.Release|Any CPU.ActiveCfg = Release|Any CPU
		{03AB8642-53EC-4407-90B9-0CCA67E13634}.Release|Any CPU.Build.0 = Release|Any CPU
		{03AB8642-53EC-4407-90B9-0CCA67E13634}.Release|x64.ActiveCfg = Release|Any CPU
		{03AB8642-53EC-4407-90B9-0CCA67E13634}.Release|x64.Build.0 = Release|Any CPU
		{03AB8642-53EC-4407-90B9-0CCA67E13634}.Release|x86.ActiveCfg = Release|Any CPU
		{03AB8642-53EC-4407-90B9-0CCA67E13634}.Release|x86.Build.0 = Release|Any CPU
		{6231274B-235A-4F22-962E-DF171F045168}.Debug|Any CPU.ActiveCfg = Debug|Any CPU
		{6231274B-235A-4F22-962E-DF171F045168}.Debug|Any CPU.Build.0 = Debug|Any CPU
		{6231274B-235A-4F22-962E-DF171F045168}.Debug|x64.ActiveCfg = Debug|Any CPU
		{6231274B-235A-4F22-962E-DF171F045168}.Debug|x64.Build.0 = Debug|Any CPU
		{6231274B-235A-4F22-962E-DF171F045168}.Debug|x86.ActiveCfg = Debug|Any CPU
		{6231274B-235A-4F22-962E-DF171F045168}.Debug|x86.Build.0 = Debug|Any CPU
		{6231274B-235A-4F22-962E-DF171F045168}.Release|Any CPU.ActiveCfg = Release|Any CPU
		{6231274B-235A-4F22-962E-DF171F045168}.Release|Any CPU.Build.0 = Release|Any CPU
		{6231274B-235A-4F22-962E-DF171F045168}.Release|x64.ActiveCfg = Release|Any CPU
		{6231274B-235A-4F22-962E-DF171F045168}.Release|x64.Build.0 = Release|Any CPU
		{6231274B-235A-4F22-962E-DF171F045168}.Release|x86.ActiveCfg = Release|Any CPU
		{6231274B-235A-4F22-962E-DF171F045168}.Release|x86.Build.0 = Release|Any CPU
	EndGlobalSection
EndGlobal
```

再次运行dotnet sln list命令查看

```bash
dotnet sln list
# 显示结果
PS D:\百度云同步盘\Dev\lessons\.NET\examples\piedpiper\src> dotnet sln list
项目
--
piedpiper.website\piedpiper.website.csproj
piedpiper.domain\piedpiper.domain.csproj
piedpiper.tests\piedpiper.tests.csproj
PS D:\百度云同步盘\Dev\lessons\.NET\examples\piedpiper\src>
```

### 向项目添加项目引用

接下来，我们要开始向我们的项目添加项目引用，通过`dotnet add reference`命令将我们的域库链接到我们的网站和单元测试库：

```bash
dotnet add piedpiper.tests reference piedpiper.domain/piedpiper.domain.csproj
# 已将引用“..\piedpiper.domain\piedpiper.domain.csproj”添加到项目。
```

现在，如果要查看测试项目的内容（piedpiper.tests\piedpiper.tests.csproj），我们将看到我们的domain已被引用：

```xml
<Project Sdk="Microsoft.NET.Sdk">

  <PropertyGroup>
    <TargetFramework>netcoreapp3.0</TargetFramework>

    <IsPackable>false</IsPackable>
  </PropertyGroup>

  <ItemGroup>
    <PackageReference Include="Microsoft.NET.Test.Sdk" Version="16.2.0" />
    <PackageReference Include="xunit" Version="2.4.0" />
    <PackageReference Include="xunit.runner.visualstudio" Version="2.4.0" />
    <PackageReference Include="coverlet.collector" Version="1.0.1" />
  </ItemGroup>

  <ItemGroup>
    <ProjectReference Include="..\piedpiper.domain\piedpiper.domain.csproj" />
  </ItemGroup>

</Project>

```

接下来，我们将为我们的网站项目做同样的事情，所以让我们去我们的网站文件夹并运行相同的命令：

```bash
dotnet add piedpiper.website reference piedpiper.domain/piedpiper.domain.csproj
# 已将引用“..\piedpiper.domain\piedpiper.domain.csproj”添加到项目。
```

### 构建整个解决方案

如果我们返回到我们的根源文件夹并运行build命令，我们应该看到所有的构建成功：

```bash
dotnet build
```

查看结果

```sh
用于 .NET Core 的 Microsoft (R) 生成引擎版本 16.3.0+0f4c62fea
版权所有(C) Microsoft Corporation。保留所有权利。

  D:\百度云同步盘\Dev\lessons\.NET\examples\piedpiper\src\piedpiper.domain\piedpiper.domain.csproj 的还原在 18.29 ms 内完成。
  D:\百度云同步盘\Dev\lessons\.NET\examples\piedpiper\src\piedpiper.website\piedpiper.website.csproj 的还原在 18.28 ms 内完成。
  D:\百度云同步盘\Dev\lessons\.NET\examples\piedpiper\src\piedpiper.tests\piedpiper.tests.csproj 的还原在 20.35 ms 内完成。
  piedpiper.domain -> D:\百度云同步盘\Dev\lessons\.NET\examples\piedpiper\src\piedpiper.domain\bin\Debug\netstandard2.0\piedpiper.domain.dll   
  piedpiper.tests -> D:\百度云同步盘\Dev\lessons\.NET\examples\piedpiper\src\piedpiper.tests\bin\Debug\netcoreapp3.0\piedpiper.tests.dll       
  piedpiper.website -> D:\百度云同步盘\Dev\lessons\.NET\examples\piedpiper\src\piedpiper.website\bin\Debug\netcoreapp3.0\piedpiper.website.dll 
  piedpiper.website -> D:\百度云同步盘\Dev\lessons\.NET\examples\piedpiper\src\piedpiper.website\bin\Debug\netcoreapp3.0\piedpiper.website.Views.dll

已成功生成。
    0 个警告
    0 个错误

已用时间 00:00:00.87
```

![构建整个解决方案](Visual Studio Code 开发.NET Core应用程序.assets/image-20191117164325576.png)

### 将NuGet包添加到项目或更新它

假设我们要将NuGet包添加到我们的一个项目中，我们可以使用该add package命令来执行此操作。

首先导航到要添加NuGet软件包的项目：

```bash
cd .\piedpiper.tests\
dotnet add package shouldly
# 或者，我们可以使用版本参数指定要安装的版本：
dotnet add package shouldly -v 2.8.3
```

将NuGet软件包更新到最新版本也是一样简单，只需使用相同的命令而不使用版本参数：

```bash
dotnet add package shouldly
```

![将NuGet包添加到项目](Visual Studio Code 开发.NET Core应用程序.assets/image-20191117164648244.png)

### 运行解决方案

点击调试图标，自动弹出提示

![点击调试图标，自动弹出提示](Visual Studio Code 开发.NET Core应用程序.assets/image-20191117164953911.png)

点击YES，要你选择从那个项目运行（测试还是WEB）

![选择从那个项目运行](Visual Studio Code 开发.NET Core应用程序.assets/image-20191117165037799.png)

选择从website运行，自动生成以下文件（launch.json、tasks.json），可以具体看看配置

![自动生成以下文件](Visual Studio Code 开发.NET Core应用程序.assets/image-20191117165208526.png)

再次按<kbd>F5</kbd>运行成功后访问 https://localhost:5001/ 

![运行成功](Visual Studio Code 开发.NET Core应用程序.assets/image-20191117165637365.png)

## 完善解决方案

### 添加Newtonsoft.Json

在piedpiper.domain项目中追加逻辑，首先添加Newtonsoft.Json

```sh
 cd .\piedpiper.domain\
 dotnet add package Newtonsoft.Json --version 12.0.3
```

### 完善Product.cs

将Product.cs移植过来

```csharp
using System;
using Newtonsoft.Json;

namespace piedpiper.domain
{
  public class Product
  {
    public String Name { get; set; }
    public DateTime Expiry { get; set; }
    public String[] Sizes { get; set; }
    public String JsonConvertSizes()
    {
        string json = JsonConvert.SerializeObject(this.Sizes);
        return json;
    }
  }
}
```

> 注意修改namespace为piedpiper.domain
>
> 注意修改class为public才能被访问

### 引用piedpiper.domain

在piedpiper.website中使用Product，修改`src\piedpiper.website\Controllers\HomeController.cs`

```csharp
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using piedpiper.website.Models;
using piedpiper.domain;

namespace piedpiper.website.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            Product product = new Product();
            product.Name = "Apple";
            product.Expiry = new DateTime(2008, 12, 28);
            product.Sizes = new string[] { "Small" };
            ViewData["Sizes"]= product.JsonConvertSizes();
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
```

> 我们引入了`using piedpiper.domain`以调用`product.JsonConvertSizes()`
>
> 将`ViewData["Sizes"]= product.JsonConvertSizes();`

### 修改Index.cshtml

修改Index.cshtml输出`@ViewData["Sizes"]`

```html
@{
    ViewData["Title"] = "Home Page";
}

<div class="text-center">
    <h1 class="display-4">@ViewData["Sizes"]</h1>
    <p>Learn about <a href="https://docs.microsoft.com/aspnet/core">building Web apps with ASP.NET Core</a>.</p>
</div>
```

### 运行成功

运行发现成功

![运行发现成功](Visual Studio Code 开发.NET Core应用程序.assets/image-20191117172026674.png)

## 使用GIT

### 添加.gitignore

```sh
# Build and Object Folders  
bin/  
obj/  
  
# Nuget packages directory  
packages/  
  
## Ignore Visual Studio temporary files, build results, and  
## files generated by popular Visual Studio add-ons.  
  
# User-specific files  
*.suo  
*.user  
*.sln.docstates  
  
# Build results  
[Dd]ebug/  
[Rr]elease/  
x64/  
*_i.c  
*_p.c  
*.ilk  
*.meta  
*.obj  
*.pch  
*.pdb  
*.pgc  
*.pgd  
*.rsp  
*.sbr  
*.tlb  
*.tli  
*.tlh  
*.tmp  
*.log  
*.vspscc  
*.vssscc  
.builds  
  
# Visual C++ cache files  
ipch/  
*.aps  
*.ncb  
*.opensdf  
*.sdf  
  
# Visual Studio profiler  
*.psess  
*.vsp  
*.vspx  
  
# Guidance Automation Toolkit  
*.gpState  
  
# ReSharper is a .NET coding add-in  
_ReSharper*  
  
# NCrunch  
*.ncrunch*  
.*crunch*.local.xml  
  
# Installshield output folder  
[Ee]xpress  
  
# DocProject is a documentation generator add-in  
DocProject/buildhelp/  
DocProject/Help  
UpgradeLog*.XML  
  
# Lightswitch  
_Pvt_Extensions  
GeneratedArtifacts  
*.xap  
ModelManifest.xml  
  
#Backup file  
*.bak

#zzzili
v15/
.gitignore
```

### 修改HomeController.cs

```csharp
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using piedpiper.website.Models;
using piedpiper.domain;

namespace piedpiper.website.Controllers
{
  public class HomeController : Controller
  {
    private readonly ILogger<HomeController> _logger;

    public HomeController(ILogger<HomeController> logger)
    {
      _logger = logger;
    }

    public IActionResult Index()
    {
      Product product = new Product();
      product.Name = "Apple";
      product.Expiry = new DateTime(2008, 12, 28);
      product.Sizes = new string[] { "Small", ".NET开发人员的完美.gitignore文件" };
      ViewData["Sizes"] = product.JsonConvertSizes();
      return View();
    }

    public IActionResult Privacy()
    {
      return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
      return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
  }
}
```

可以通过GIT查看差异，完成常见的GIT操作

![比较差异](Visual Studio Code 开发.NET Core应用程序.assets/image-20191117173232786.png)

---

![](Visual Studio Code 开发.NET Core应用程序.assets/幻灯片2.png)


![](WPF.assets/幻灯片0.png)

---

#  WPF

WPF（Windows Presentation Foundation）是微软推出的基于Windows 的用户界面框架，属于.NET Framework 3.0的一部分。它提供了统一的编程模型、语言和框架，真正做到了分离界面设计人员与开发人员的工作；同时它提供了全新的多媒体交互用户图形界面。

* WPF是微软提供的一种用来开发“桌面应用”的技术（框架），**这项技术本身和C#没有关系**，必须会的是xaml语法。
* 对XML、HTML、XHTML、ASP.NET之类的“标准通用标记语言”，对于学习是有所帮助的。
* **有**WinForm或ASP.NET经验，主要是对控件事件的处理要有所了解。
* **具备**面向对象的思想：在WPF中，经常要灵活运用各种继承关系、多态、重载等。
* DataBinding要有所了解：Binding是WPF的一大亮点。

## .Net core创建WPF项目 

这里做一个简单的用.NetCore 创建的WPF例子 供大家参考。

```sh
dotnet new wpf -n "MyCoreWPF"
```

在App.xaml中的`StartupUri`属性可以指定项目运行时的启动窗体。

```xml
<Application x:Class="MyCoreWPF.App"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:local="clr-namespace:MyCoreWPF"
             StartupUri="MainWindow.xaml">
    <Application.Resources>
         
    </Application.Resources>
</Application>
```

`MainWindow.xaml`的设计窗体中我们可以修改Title。还可以设置MainWindow的属性和添加事件。完成了这些设置以后，我们就可以对窗体添加内容了。

##  一个例子 

这回的需求是这样的——UI上有一个TextBox和一个Slider，要求Slider的滑块滑动时TextBox的文本显示Slider的值；当TextBox里的文本改变时，就让Slider的滑块与之同步。

 对于一个有经验的Windows Form开发老手来说，他的思路是这样的：

1.  在UI上拖放控件
2.  为Slider的ValueChanged事件添加响应函数（事件处理函数），函数中将Slider的Value属性（double类型）转换成一个string类型的值并赋给TextBox的Text属性。
3.  为TextBox的TextChanged事件添加响应函数，对TextBox的Text属性进行检验，看看它是否能解析为一个double值（新手常常忘记这一点而导致bug）并且落在Slider的取值范围内，如果一切顺利，就把它赋给Slider的Value属性。

我们再看看如何使用WPF来实现， 你也可以一句C#代码都不写就完成漂亮的程序！ 

`MainWindow.xaml`

```xml
<Window x:Class="MyCoreWPF.MainWindow" 
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation" 
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml" 
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008" 
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
    xmlns:local="clr-namespace:MyCoreWPF" mc:Ignorable="d" Title="MainWindow" Height="450" Width="450">
    <Grid>
        <Grid.Background>
            <LinearGradientBrush>
                <GradientStop Offset="0" Color="Blue"></GradientStop>
                <GradientStop Offset="0.5" Color="LightBlue"></GradientStop>
            </LinearGradientBrush>
        </Grid.Background>
        <TextBox Height="23" Margin="10,10,10,0" Name="textBox1" VerticalAlignment="Top" Text="{Binding ElementName=slider1, Path=Value, UpdateSourceTrigger=PropertyChanged}" />
        <Slider Height="21" Margin="10,40,10,0" Name="slider1" VerticalAlignment="Top" Maximum="100" />
    </Grid>
</Window>
```

![一个WPF例子](WPF.assets/image-20191117202919367.png)

这些代码80%都是VS自动生成的，而且它们的结构的非常简单——箱子里装着一个口袋，口袋里装着两个核桃。上面这段程序最重要的一句就是：

`Text="{Binding ElementName=slider1, Path=Value, UpdateSourceTrigger=PropertyChanged}"`

即可以说它是WPF最精华的部分，也可以说它是与Windows Form开发相比变化最大的地方，它就是——Data Binding（数据关联）。 

##  解剖最简单的GUI程序 

说实话，XAML这种语言一点也不难。如果把XML看成是“父类”，那么XAML就是XML的一个派生类了，所以XML的概念在XAML中是通用的。 

比如我有一个Window，这个Window里有1个Grid，这个Grid里又包含着3个TextBox、2个Button，那么这1个Grid就是这个Window的子元素，3个TextBox和2个Button又是Grid的子元素；而Window的Name、Icon、尺寸乃至Resources都是这个Window的属性。

`MyCoreWPF\MainWindow.xaml`

```xml
<Window x:Class="MyCoreWPF.MainWindow" 
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation" 
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml" 
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008" 
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
    xmlns:local="clr-namespace:MyCoreWPF" mc:Ignorable="d" Title="MainWindow" Height="450" Width="450">
    <Grid>
        <Grid.Background>
            <LinearGradientBrush>
                <GradientStop Offset="0" Color="Blue"></GradientStop>
                <GradientStop Offset="0.5" Color="LightBlue"></GradientStop>
            </LinearGradientBrush>
        </Grid.Background>
        <TextBox Height="23" Margin="10,10,10,0" Name="textBox1" VerticalAlignment="Top" Text="{Binding ElementName=slider1, Path=Value, UpdateSourceTrigger=PropertyChanged}" />
        <Slider Height="21" Margin="10,40,10,0" Name="slider1" VerticalAlignment="Top" Maximum="100" />
    </Grid>
</Window>
```

> `x:Class="MyCoreWPF.MainWindow"` 指明了界面和后台逻辑类的关系

`MyCoreWPF\MainWindow.xaml.cs`

```csharp
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyCoreWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
    }
}
```

CS文件如何和xaml文件交互呢，这里我们举一个简单的例子，把`MyCoreWPF\MainWindow.xaml.cs`文件我们做一点简单的修改

```csharp
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyCoreWPF
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    public MainWindow()
    {
      InitializeComponent();
      var Title = this.Title;
      var Height = this.Height;
      var Width = this.Width;
      Console.WriteLine(Title.ToString() + "==========" + Height.ToString() + "=============" + Width.ToString());
      this.Title = "深入浅出WPF之XAML";
    }
  }
}
```

## XAML有哪些优点

* XAML可以设计出专业的UI和动画——好用
* XAML不需要专业的编程知识，它简单易懂、结构清晰——易学
* XAML使设计师能直接参与软件开发，随时沟通、无需二次转化——高效

然而，XAML这位翩翩君子的才华可远不止这些。自从应用程序从命令行界面（Console User Interface，CUI，这本书的读者还有用过DOS的吗？就是那东西）升级为图形用户界面（Graphic User Interface，GUI）后，程序员们就一直追求将视图（View，也就是UI）与逻辑代码的分离。以往的开发模式中，程序员很难保证用来实现UI的 代码完全不与用来实现程序逻辑的代码纠缠在一起。UI代码与逻辑代码纠缠在一起称为UI与逻辑的紧耦合，它往往带来以下的后果：

* 无论是软件的功能还是UI设计有所变化或者是出了bug，都将导致大量代码的修改。
* 会让逻辑代码更加难以理解——修改往往比重写更困难，因为在修改之前必须先读懂。
* 重用逻辑代码变成了Mission Impossible

XAML 另一个巨大的优点就是：它帮助开发团队真正实现了UI与逻辑的剥离。XAML是一种单纯的声明型语言，也就是说，它只能用来声明一些UI元素、绘制UI和动画（在XAML里实现动画是不需要编程的），根本无法在其中加入程序逻辑，这就强制地把逻辑代码从UI代码中赶走了。

这样，与UI相关的元素统统集中在程序的UI层、与逻辑相关的代码统统集中在程序逻辑层，形成了一种“高内聚-低耦合”的结构。形成这种结构后，无论是对UI进行较大改动还是打算重用底层逻辑，都不会花费太大力气。

这就好比有一天你给A客户做了一个橘子，A客户很喜欢；A客户把你的产品介绍给了B客户，B客户很喜欢橘子味道，但希望它看上 去像个香蕉——这时候，你只需要把橘子皮撕下来 、换一套香蕉皮即可——只需很少的成本就可以获得与先前一样大的收益。

## WPF最常用的几种布局方式

* **StackPanel**：堆栈面板，通过Orientation属性设置子元素的布局排列方向为“Vertical”（垂直）和“Horizontal”（水平），不写其默认值为“Vertical”，当设置为“Vertical”时子元素会沿水平方向拉伸，反之设置为“Horizontal”时子元素会沿垂直方向拉伸。

* **DockPanel**：支持子元素停靠在面板的任意一条边上，通过附加属性Dock控制他们的停靠位置（Left、Top、Right、Bottom），填充空间按照“先到先得”的原则，最后一个加入面板的子元素将填满剩下的空间，如不想将最后加入面板的元素填满剩下的空间将属性LastChildFill值设为“False”，默认为“True”。

* **WrapPanel**：可换行面板与StackPanel相似，通过Orientation属性设置子元素的排列顺序，从左至右按顺序位置定位子元素，当前行无法放下元素时断开至下一行，或者排序按照从上至下或从右至左的顺序进行，通过ItemHeight可以设置当前面板中所有子元素的高度，当然也有ItemWidth设置所有子元素的宽度。

* **Canvas**：面板是最轻量级的布局容器，它不会自动调整内部元素的排列和大小，不指定元素位置，元素将默认显示在画布的左上方。Canvas主要用来画图。Canvas默认不会自动裁剪超过自身范围的内容，即溢出的内容会显示在Canvas外面，这是因为Canvas的ClipToBounds属性默认值是“False”，我们可以显式地设置为“True”来裁剪多出的内容。下面XAML代码简单演示了Canvas面板的使用。

* **Grid**：比起其他Panel，功能是最多最为复杂的布局控件。它由`<Grid.ColumnDefinitions>`列元素集合和`<Grid.RowDefinitions>`行元素集合两种元素组成。而放在Grid面板中的元素必须显式采用附加属性定义其所在行和列，否则元素均默认放置在第0行第0列。

## WPF中的六大控件类型

1. 布局控件：可以容纳多个控件或者其它布局控件，例如Grid,StackPanel,DockPanel，父类：Panel。

2. 内容控件：只能容纳一个其它控件或者布局控件作为它的内容。Window,Button等控件属于此类。父类：ContentControl.

3. 带标题内容控件：相当于一个内容控件，但可以加一个标题，标题部分也可以容纳一个控件或者一个布局控件。GroupBox,TabItem.父类：HeaderedContentControl.

4. 条目控件：可以显示一列数据，它们的类型一般情况下相同。ListBox,ComboBox。父类：ItemsControl

5. 带标题条目控件：相当于一个条目控件加上一个标题显示区。TreeViewItem,MenuItem都属于此类控件。父类：HeaderedItemsControl.

6. 特殊内容控件：比如TextBox容纳的是字符串，TextBlock可以容纳可自由控制格式的文本Image容纳图片类型数据

## WPF之Binding

Data Binding机制可以说是WPF实现**`MVVM`**的关键，与之配套的Dependency Property和DataTemplate共同完成了数据到UI的通路，让逻辑层与UI分离。 

### 最简单的Data Binding

举个最简单的Data Binding的例子：

**UI代码**

```xml
<Window x:Class="MyCoreWPF.MainWindow" 
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation" 
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml" 
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008" 
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
    xmlns:local="clr-namespace:MyCoreWPF" mc:Ignorable="d" Title="MainWindow" Height="450" Width="450">
    <Grid>
        <StackPanel>
            <TextBox x:Name="textBoxStu" Margin="3" BorderBrush="Black"/>
        </StackPanel>
    </Grid>
</Window>
```

**定义用户类**

```csharp
namespace MyCoreWPF
{
  public class Student
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public int Age { get; set; }
  }
}
```

**后台代码**

```csharp
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyCoreWPF
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    private Student stu;
    public MainWindow()
    {
      InitializeComponent();
      var Title = this.Title;
      var Height = this.Height;
      var Width = this.Width;
      Console.WriteLine(Title.ToString() + "==========" + Height.ToString() + "=============" + Width.ToString());
      this.Title = "深入浅出WPF之XAML";
      textBoxStu.SetBinding(TextBox.TextProperty, new Binding("Name")
      {
        Source = new MyCoreWPF.Student { Name = "Jack" }
      });
    }
  }
}

```

**运行程序，TextBox中显示了Jack字符串。**

> :zap:这样就实现了一个Student的对象和一个TextBox的binding，更加准确的说法是Student对象的Name属性和TextBox的Text属性进行了Binding。

观察SetBinding方法的两个参数：第一个参数是一个依赖属性，第二个参数是一个Binding类型的对象，它指定了数据源和需要进行关联的属性。

### 更改响应

**但是所谓绑定，应该对更改也有响应才对。那么下面对代码稍加更改加以验证：**

(UI代码片段)

```xml
<StackPanel>
    <TextBox x:Name="textBoxStu" Margin="5" BorderBrush="Black"/>
    <Button Content="Show" Margin="5" Click="Button_Click"/>
    <Button Content="Change" Margin="5" Click="Button_Click_1"/>
</StackPanel>
```

(后台代码)

```csharp
private Student stu;

public MainWindow()
{
　　InitializeComponent();
　　textBoxStu.SetBinding(TextBox.TextProperty,
　　　　new Binding("Name")
　　　　{
　　　　　　Source = stu = new Student { Name = "Jack" }
　　　　}
　　);}

private void Button_Click(object sender, RoutedEventArgs e)
{
　　MessageBox.Show(stu.Name);
}

private void Button_Click_1(object sender, RoutedEventArgs e)
{
　　stu.Name += " Changed!";
}
```

运行代码：

![WPF之Binding](WPF.assets/image-20191117215152555.png)

### UI元素响应

**但在对象属性更改时，textBoxStu中的文本并没有变化**

如果希望属性变化时UI元素也能响应，那就需要在属性的set索引器中激发一个PropertyChanged事件，它被定义在INotifyPropertyChanged接口中。

修改Student类：

```csharp
using System.ComponentModel;
namespace MyCoreWPF
{
  public class Student : INotifyPropertyChanged
  {
    public int Id { get; set; }

    private string name;

    public event PropertyChangedEventHandler PropertyChanged;

    public string Name
    {
      get { return name; }
      set
      {
        name = value;
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Name"));
      }
    }
    public int Age { get; set; }
  }
}
```

测试：

![UI元素响应](WPF.assets/1229247-20171214001429551-2139349288.png)

(在stu.Name发生改变后，UI也响应了这一变化)

至此，基本实现了最简单的数据与UI绑定。 

---

![](WPF.assets/幻灯片2.png)
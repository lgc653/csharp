# MySQL

## MySQL 安装

所有平台的 MySQL 下载地址为： [MySQL 下载](https://dev.mysql.com/downloads/mysql/) 。 挑选你需要的 *MySQL Community Server* 版本及对应的平台。

> **注意：**安装过程我们需要通过开启管理员权限来安装，否则会由于权限不足导致无法安装。

### Windows 上安装 MySQL

Windows 上安装 MySQL 相对来说会较为简单，最新版本可以在 [MySQL 下载](https://dev.mysql.com/downloads/mysql/) 中下载中查看(**更详细安装：[Windows 上安装 MySQL](https://www.runoob.com/w3cnote/windows10-mysql-installer.html)**)。

![img](MySQL入门.assets/330405-20160709174318905-664331194.png)

![img](MySQL入门.assets/20DBD7BA-A653-4AE3-887E-2A16E6EBB2E3.png)

点击 **Download** 按钮进入下载页面，点击下图中的 **No thanks, just start my download.** 就可立即下载：

![img](MySQL入门.assets/330405-20160709174941374-1821908969.png)

下载完后，我们将 zip 包解压到相应的目录，这里我将解压后的文件夹放在 **C:\web\mysql-8.0.11** 下。

**接下来我们需要配置下 MySQL 的配置文件**

打开刚刚解压的文件夹 **C:\web\mysql-8.0.11** ，在该文件夹下创建 **my.ini** 配置文件，编辑 **my.ini** 配置以下基本信息：

```ini
[client]
# 设置mysql客户端默认字符集
default-character-set=utf8
 
[mysqld]
# 设置3306端口
port = 3306
# 设置mysql的安装目录
basedir=C:\\web\\mysql-8.0.11
# 设置 mysql数据库的数据的存放目录，MySQL 8+ 不需要以下配置，系统自己生成即可，否则有可能报错
# datadir=C:\\web\\sqldata
# 允许最大连接数
max_connections=20
# 服务端使用的字符集默认为8比特编码的latin1字符集
character-set-server=utf8
# 创建新表时将使用的默认存储引擎
default-storage-engine=INNODB
```

**接下来我们来启动下 MySQL 数据库：**

以管理员身份打开 cmd 命令行工具，切换目录：

```sh
cd C:\web\mysql-8.0.11\bin
```

初始化数据库：

```sh
mysqld --initialize --console
```

执行完成后，会输出 root 用户的初始默认密码，如：

```sh
...
2018-04-20T02:35:05.464644Z 5 [Note] [MY-010454] [Server] A temporary password is generated for root@localhost: APWCY5ws&hjQ
...
```

**APWCY5ws&hjQ** 就是初始密码，后续登录需要用到，你也可以在登陆后修改密码。

输入以下安装命令：

```sh
mysqld install
```

启动输入以下命令即可：

```sh
net start mysql
```

> 注意: 在 5.7 需要初始化 data 目录：
>
> ```
> cd C:\web\mysql-8.0.11\bin 
> mysqld --initialize-insecure 
> ```
>
> 初始化后再运行 net start mysql 即可启动 mysql。

### 登录 MySQL

当 MySQL 服务已经运行时, 我们可以通过 MySQL 自带的客户端工具登录到 MySQL 数据库中, 首先打开命令提示符, 输入以下格式的命名:

```sh
mysql -h 主机名 -u 用户名 -p
```

参数说明：

- **-h** : 指定客户端所要登录的 MySQL 主机名, 登录本机(localhost 或 127.0.0.1)该参数可以省略;
- **-u** : 登录的用户名;
- **-p** : 告诉服务器将会使用一个密码来登录, 如果所要登录的用户名密码为空, 可以忽略此选项。

如果我们要登录本机的 MySQL 数据库，只需要输入以下命令即可：

```sh
mysql -u root -p
```

按回车确认, 如果安装正确且 MySQL 正在运行, 会得到以下响应:

```sh
Enter password:
```

若密码存在, 输入密码登录, 不存在则直接按回车登录。登录成功后你将会看到 Welcome to the MySQL monitor... 的提示语。

然后命令提示符会一直以 **mysq>** 加一个闪烁的光标等待命令的输入, 输入 **exit** 或 **quit** 退出登录。

## MySQL 管理

### 启动及关闭 MySQL 服务器

### Windows 系统下

在 Windows 系统下，打开命令窗口(cmd)，进入 MySQL 安装目录的 bin 目录。

启动：

```sh
cd c:/mysql/bin
mysqld --console
```

关闭：

```sh
cd c:/mysql/bin
mysqladmin -uroot shutdown
```

### Linux 系统下

首先，我们需要通过以下命令来检查MySQL服务器是否启动：

```sh
ps -ef | grep mysqld
```

如果MySql已经启动，以上命令将输出mysql进程列表， 如果mysql未启动，你可以使用以下命令来启动mysql服务器:

```sh
root@host# cd /usr/bin
./mysqld_safe &
```

如果你想关闭目前运行的 MySQL 服务器, 你可以执行以下命令:

```sh
root@host# cd /usr/bin
./mysqladmin -u root -p shutdown
Enter password: ******
```

### MySQL 用户设置

如果你需要添加 MySQL 用户，你只需要在 mysql 数据库中的 user 表添加新用户即可。

以下为添加用户的的实例，用户名为guest，密码为guest123，并授权用户可进行 SELECT, INSERT 和 UPDATE操作权限：

```sh
root@host# mysql -u root -p
Enter password:*******
mysql> use mysql;
Database changed

mysql> INSERT INTO user 
          (host, user, password, 
           select_priv, insert_priv, update_priv) 
           VALUES ('localhost', 'guest', 
           PASSWORD('guest123'), 'Y', 'Y', 'Y');
Query OK, 1 row affected (0.20 sec)

mysql> FLUSH PRIVILEGES;
Query OK, 1 row affected (0.01 sec)

mysql> SELECT host, user, password FROM user WHERE user = 'guest';
+-----------+---------+------------------+
| host      | user    | password         |
+-----------+---------+------------------+
| localhost | guest | 6f8c114b58f2ce9e |
+-----------+---------+------------------+
1 row in set (0.00 sec)
```

在添加用户时，请注意使用MySQL提供的 PASSWORD() 函数来对密码进行加密。 你可以在以上实例看到用户密码加密后为： 6f8c114b58f2ce9e.

**注意：**在 MySQL5.7 中 user 表的 password 已换成了**authentication_string**。

**注意：**password() 加密函数已经在 8.0.11 中移除了，可以使用 MD5() 函数代替。

**注意：**在注意需要执行 **FLUSH PRIVILEGES** 语句。 这个命令执行后会重新载入授权表。

如果你不使用该命令，你就无法使用新创建的用户来连接mysql服务器，除非你重启mysql服务器。

你可以在创建用户时，为用户指定权限，在对应的权限列中，在插入语句中设置为 'Y' 即可，用户权限列表如下：

- Select_priv
- Insert_priv
- Update_priv
- Delete_priv
- Create_priv
- Drop_priv
- Reload_priv
- Shutdown_priv
- Process_priv
- File_priv
- Grant_priv
- References_priv
- Index_priv
- Alter_priv

另外一种添加用户的方法为通过SQL的 GRANT 命令，以下命令会给指定数据库TUTORIALS添加用户 zara ，密码为 zara123 。

```sh
root@host# mysql -u root -p
Enter password:*******
mysql> use mysql;
Database changed

mysql> GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,DROP
    -> ON TUTORIALS.*
    -> TO 'zara'@'localhost'
    -> IDENTIFIED BY 'zara123';
```

以上命令会在mysql数据库中的user表创建一条用户信息记录。

**注意:** MySQL 的SQL语句以分号 (;) 作为结束标识。

### /etc/my.cnf 文件配置

一般情况下，你不需要修改该配置文件，该文件默认配置如下：

```ini
[mysqld]
datadir=/var/lib/mysql
socket=/var/lib/mysql/mysql.sock

[mysql.server]
user=mysql
basedir=/var/lib

[safe_mysqld]
err-log=/var/log/mysqld.log
pid-file=/var/run/mysqld/mysqld.pid
```

在配置文件中，你可以指定不同的错误日志文件存放的目录，一般你不需要改动这些配置。

## MySQL常用图形化管理工具

MySQL 图形化管理工具极大地方便了数据库的操作与管理，除了系统自带的命令行管理工具之外，常用的图形化管理工具还有 MySQL Workbench、phpMyAdmin、Navicat、MySQLDumper、SQLyog、MySQL ODBC Connector。

这里向大家推荐两款，他们都开源免费，功能强大，全中文支持：

### HeidiSQL 

**HeidiSQL**是一款用于简单化迷你的服务器和数据库管理的图形化界面，采用Delphi开发，支持MySQL、SQL Server、PostgreSQL服务器。HeidiSQL提供了一个用于在数据库浏览之间切换SQL查询和标签带有语法突出显示的简单易用的界面。可以方便浏览您的数据库，管理表、视图、存储过程、触发器，浏览和编辑记录，管理用户权限。此外，你可以从文本文件导入数据，运行SQL查询，在两个数据库之间同步表以及导出选择的表到其它数据库或者SQL脚本中。

![Table Editor](MySQL入门.assets/table_editor.png)

###  DBeaver

DBeaver 是一个通用的数据库管理工具和 SQL 客户端，支持 MySQL, PostgreSQL, Oracle, DB2, MSSQL, Sybase, Mimer, HSQLDB, Derby, 以及其他兼容 JDBC 的数据库。DBeaver 提供一个图形界面用来查看数据库结构、执行SQL查询和脚本，浏览和导出数据，处理BLOB/CLOB 数据，修改数据库结构等等。

![](MySQL入门.assets/erd.png)

## MySQL数据库的基本操作

MySQL 安装好之后就可以进行数据库的相关操作了。数据库就像车库一样，每一个小区里都有很多车库用来存放车，它们都有唯一的车库号。同样，在 MySQL 中也可以创建多个不同名称的数据库存储数据。

SQL 是一种专门用来与数据库通信的语言，我们使用 SQL 来操作数据库。

对数据库进行查询和修改操作的语言叫做 SQL（Structured Query Language，结构化查询语言）。SQL 语言是目前广泛使用的关系数据库标准语言，是各种数据库交互方式的基础。

> 著名的大型商用数据库 Oracle、DB2、Sybase、SQL Server，开源的数据库 PostgreSQL、MySQL，甚至一些小型的数据库 Access 等都支持 SQL。近些年蓬勃发展的 NoSQL 系统最初是宣称不再需要 SQL 的，后来也不得不修正为 Not Only SQL，来拥抱 SQL。

SQL 是一种数据库查询和程序设计语言，用于存取数据以及查询、更新和管理关系数据库系统。与其他程序设计语言（如 C语言、Java 等）不同的是，SQL 由很少的关键字组成，每个 SQL 语句通过一个或多个关键字构成。

SQL 具有如下优点。

1. 一体化：SQL 集数据定义、数据操作和数据控制于一体，可以完成数据库中的全部工作。
2. 使用方式灵活：SQL 具有两种使用方式，可以直接以命令方式交互使用；也可以嵌入使用，嵌入C、C++、Fortran、COBOL、Java 等语言中使用。
3. 非过程化：只提操作要求，不必描述操作步骤，也不需要导航。使用时只需要告诉计算机“做什么”，而不需要告诉它“怎么做”，存储路径的选择和操作的执行由数据库管理系统自动完成。
4. 语言简洁、语法简单：该语言的语句都是由描述性很强的英语单词组成，而且这些单词的数目不多。


SQL 包含以下 4 部分：

### 数据定义语言（Data Definition Language，DDL）

用来创建或删除数据库以及表等对象，主要包含以下几种命令：

- DROP：删除数据库和表等对象
- CREATE：创建数据库和表等对象
- ALTER：修改数据库和表等对象的结构

### 数据操作语言（Data Manipulation Language，DML）

用来变更表中的记录，主要包含以下几种命令：

- SELECT：查询表中的数据
- INSERT：向表中插入新数据
- UPDATE：更新表中的数据
- DELETE：删除表中的数据

### 数据查询语言（Data Query Language，DQL）

用来查询表中的记录，主要包含 SELECT 命令，来查询表中的数据。

### 数据控制语言（Data Control Language，DCL）

用来确认或者取消对数据库中的数据进行的变更。除此之外，还可以对数据库中的用户设定权限。主要包含以下几种命令：

- GRANT：赋予用户操作权限
- REVOKE：取消用户的操作权限
- COMMIT：确认对数据库中的数据进行的变更
- ROLLBACK：取消对数据库中的数据进行的变更


下面是一条 SQL 语句的例子，该语句声明创建一个名叫 students 的表：

```sql
CREATE TABLE students (
    student_id INT UNSIGNED,
    name VARCHAR(30) ,
    sex CHAR(1),
    birth DATE,
    PRIMARY KEY(student_id)
);
```

该表包含 4 个字段，分别为 student_id、name、sex、birth，其中 student_id 定义为表的主键。

现在只是定义了一张表格，但并没有任何数据，接下来这条 SQL 声明语句，将在 students 表中插入一条数据记录：

```sql
INSERT INTO students (student_id, name, sex, birth)
VALUES (41048101, 'MySQL教程', '1', '2013-02-14');
```

执行完该 SQL 语句之后，students 表中就会增加一行新记录，该记录中字段 student_id 的值为“41048101”，name 字段的值为“MySQL教程”。sex 字段值为“1”，birth 字段值为“2013-02-14”。

再使用 SELECT 查询语句获取刚才插入的数据，如下：

```sql
SELECT name FROM students WHERE student_id=41048101;
```

上面简单列举了常用的数据库操作语句，在这里留下一个印象即可，后面我们会详细介绍这些知识。

> 注意：SQL 语句不区分大小写，许多 SQL 开发人员习惯对 SQL 本身的关键字进行大写，而对表或者列的名称使用小写，这样可以提高代码的可阅读性和可维护性。本教程也按照这种方式组织 SQL 语句。大多数数据库都支持通用的 SQL 语句，同时不同的数据库具有各自特有的 SQL 语言特性。

### 拓展

标准 SQL 是指符合国际标准的 SQL，而非某个数据库厂商的 SQL 语法（如：Microsoft SQL Server 的 T-SQL，Oracle 的 PL/SQL，MySQL）。

标准 SQL 可以在任何数据库中使用，而数据库厂商的 SQL 只适合它们对应的数据库，如 T-SQL 只适合 Microsoft SQL Server。

本教程讲解的 SQL 是专门针对 MySQL 的，虽然多数语法也适用于其它 DBMS，但不是所有 SQL 语法都是完全可移植的。 

## SQL的基本书写规则

对于 SQL 初学者，在写 SQL 语句时，只要遵守下面几个书写规则，就可以避免很多错误。这些规则都非常简单，下面我们来逐一介绍。

### SQL 语句要以分号`;`结尾

在 RDBMS （关系型数据库）当中，SQL 语句是逐条执行的，一条 SQL 语句代表着数据库的一个操作。

我们通常在句子的句尾加注标点表示这句话结束，中文句子以句号`。`结尾，英文以点号`.`结尾，而 SQL 语句则使用英文分号`;`结尾。

### SQL 语句不区分大小写

SQL 不区分关键字的大小写。例如，不管写成 SELECT 还是 select，解释都是一样的。表名和列名也是如此。

提示：关键字是数据库事先定义的，有特别意义的单词。

虽然可以根据个人喜好选择大写还是小写（或大小写混杂），但为了理解起来更加容易，本教程使用以下规则来书写 SQL 语句。

- 关键字大写
- 数据库名、表名和列名等小写


需要注意的是，插入到表中的数据是区分大小写的。例如，向数据库中插入单词 Computer、COMPUTER 或 computer，这三个是不一样的数据

### 常数的书写方式是固定的

SQL 语句常常需要直接书写字符串、日期或者数字。例如，书写向表中插入字符串、日期或者数字等数据的 SQL 语句。

在 SQL 语句中直接书写的字符串、日期或者数字等称为常数。常数的书写方式如下所示：

- SQL 语句中含有字符串的时候，需要像 'abc' 这样，使用英文单引号`'`将字符串括起来，用来标识这是一个字符串。
- SQL 语句中含有日期的时候，同样需要使用英文单引号将其括起来。日期的格式有很多种（'26 Jan 2010' 或者'10/01/26' 等），本教程统一使用 '2020-01-26' 这种`'年-月-日'`的格式。
- 在 SQL 语句中书写数字的时候，不需要使用任何符号标识，直接写成 1000 这样的数字即可。

> 注意：列名不是字符串，不能使用单引号。在MySQL 中可以用倒引号把表名和列名括起来。
>
> ```sql
> SELECT `name` FROM `students`
> ```

### 单词需要用半角空格或者换行来分隔

SQL 语句的单词之间必须使用半角空格（英文空格）或换行符来进行分隔。没有分隔的语句会发生错误，无法正常执行。

下面是分隔和未分隔的 SQL 语句：

- CREATE TABLE Product（正确）
- CREATETABLE Product（错误）
- CREATE TABLEProduct（错误）

> 不能使用全角空格（中文空格）作为单词的分隔符，否则会发生错误，出现无法预期的结果。
>
> SQL 语句中的标点符号必须都是英文状态下的，即半角字。

## MySQL操作数据库

在 MySQL 中，可使用 **SHOW DATABASES** 语句来查看或显示当前用户权限范围以内的数据库。查看数据库的语法格式为：

```sql
SHOW DATABASES [LIKE '数据库名'];
/* 示例 */
SHOW DATABASES LIKE 'test_db';
```

语法说明如下：

- LIKE 从句是可选项，用于匹配指定的数据库名称。LIKE 从句可以部分匹配，也可以完全匹配。
- 数据库名由单引号`' '`包围。

在 MySQL 中，可以使用 **CREATE DATABASE** 语句创建数据库，语法格式如下：

```sql
CREATE DATABASE [IF NOT EXISTS] <数据库名>
[[DEFAULT] CHARACTER SET <字符集名>] 
[[DEFAULT] COLLATE <校对规则名>];
/* 示例 */
CREATE DATABASE IF NOT EXISTS test_db_char
DEFAULT CHARACTER SET utf8
DEFAULT COLLATE utf8_chinese_ci;
```

`[ ]`中的内容是可选的。语法说明如下：

- <数据库名>：创建数据库的名称。MySQL 的数据存储区将以目录方式表示 MySQL 数据库，因此数据库名称必须符合操作系统的文件夹命名规则，不能以数字开头，尽量要有实际意义。注意在 MySQL 中不区分大小写。
- IF NOT EXISTS：在创建数据库之前进行判断，只有该数据库目前尚不存在时才能执行操作。此选项可以用来避免数据库已经存在而重复创建的错误。
- [DEFAULT] CHARACTER SET：指定数据库的字符集。指定字符集的目的是为了避免在数据库中存储的数据出现乱码的情况。如果在创建数据库时不指定字符集，那么就使用系统的默认字符集。
- [DEFAULT] COLLATE：指定字符集的默认校对规则。

> MySQL 的字符集（CHARACTER）和校对规则（COLLATION）是两个不同的概念。字符集是用来定义 MySQL 存储字符串的方式，校对规则定义了比较字符串的方式。后面我们会单独讲解 MySQL 的字符集和校对规则。

在 MySQL 中，可以使用 **ALTER DATABASE** 来修改已经被创建或者存在的数据库的相关参数。修改数据库的语法格式为：

```sql
ALTER DATABASE [数据库名] { [ DEFAULT ] CHARACTER SET <字符集名> |[ DEFAULT ] COLLATE <校对规则名>}
/* 示例 */
ALTER DATABASE test_db
DEFAULT CHARACTER SET gb2312
DEFAULT COLLATE gb2312_chinese_ci;
```

语法说明如下：

- ALTER DATABASE 用于更改数据库的全局特性。
- 使用 ALTER DATABASE 需要获得数据库 ALTER 权限。
- 数据库名称可以忽略，此时语句对应于默认数据库。
- CHARACTER SET 子句用于更改默认的数据库字符集。

在 MySQL 中，当需要删除已创建的数据库时，可以使用 **DROP DATABASE** 语句。其语法格式为：

```sql
DROP DATABASE [ IF EXISTS ] <数据库名>
/* 示例 */
DROP DATABASE IF EXISTS test_db_del;
```

语法说明如下：

- <数据库名>：指定要删除的数据库名。
- IF EXISTS：用于防止当数据库不存在时发生错误。
- DROP DATABASE：删除数据库中的所有表格并同时删除数据库。使用此语句时要非常小心，以免错误删除。如果要使用 DROP DATABASE，需要获得数据库 DROP 权限。

> 注意：MySQL 安装后，系统会自动创建名为 information_schema 和 mysql 的两个系统数据库，系统数据库存放一些和数据库相关的信息，如果删除了这两个数据库，MySQL 将不能正常工作。

在 MySQL 中，**USE** 语句用来完成一个数据库到另一个数据库的跳转。

当用 CREATE DATABASE 语句创建数据库之后，该数据库不会自动成为当前数据库，需要用 USE 来指定当前数据库。其语法格式为：

```sql
USE <数据库名>
/* 示例 */
USE test_db;
```

## MySQL数据表的基本操作

数据表是数据库的重要组成部分，每一个数据库都是由若干个数据表组成的。换句话说，没有数据表就无法在数据库中存放数据。

在 MySQL 中，可以使用 **CREATE TABLE** 语句创建表。其语法格式为：

CREATE TABLE <表名> ([表定义选项])表选项;

其中，`[表定义选项]`的格式为：

<列名1> <类型1> [,…] <列名n> <类型n>

CREATE TABLE 命令语法比较多，其主要是由表创建定义（create-definition）、表选项（table-options）和分区选项（partition-options）所组成的。

这里首先描述一个简单的新建表的例子，然后重点介绍 CREATE TABLE 命令中的一些主要的语法知识点。

CREATE TABLE 语句的主要语法及使用说明如下：

- CREATE TABLE：用于创建给定名称的表，必须拥有表CREATE的权限。
- <表名>：指定要创建表的名称，在 CREATE TABLE 之后给出，必须符合标识符命名规则。表名称被指定为 db_name.tbl_name，以便在特定的数据库中创建表。无论是否有当前数据库，都可以通过这种方式创建。在当前数据库中创建表时，可以省略 db-name。如果使用加引号的识别名，则应对数据库和表名称分别加引号。例如，'mydb'.'mytbl' 是合法的，但 'mydb.mytbl' 不合法。
- <表定义选项>：表创建定义，由列名（col_name）、列的定义（column_definition）以及可能的空值说明、完整性约束或表索引组成。
- 默认的情况是，表被创建到当前的数据库中。若表已存在、没有当前数据库或者数据库不存在，则会出现错误。

> 提示：使用 CREATE TABLE 创建表时，必须指定以下信息：
>
> - 要创建的表的名称不区分大小写，不能使用SQL语言中的关键字，如DROP、ALTER、INSERT等。
> - 数据表中每个列（字段）的名称和数据类型，如果创建多个列，要用逗号隔开。

## 在指定的数据库中创建表

数据表属于数据库，在创建数据表之前，应使用语句“USE<数据库>”指定操作在哪个数据库中进行，如果没有选择数据库，就会抛出 No database selected 的错误。

### MySQL创建数据表

在 MySQL 中，可以使用 **CREATE TABLE** 语句创建表。其语法格式为：

```sql
CREATE TABLE <表名> ([表定义选项])表选项;
```

其中，`[表定义选项]`的格式为：

```sql
<列名1> <类型1> [,…] <列名n> <类型n>
```

CREATE TABLE 命令语法比较多，其主要是由表创建定义（create-definition）、表选项（table-options）和分区选项（partition-options）所组成的。

这里首先描述一个简单的新建表的例子，然后重点介绍 CREATE TABLE 命令中的一些主要的语法知识点。

CREATE TABLE 语句的主要语法及使用说明如下：

- CREATE TABLE：用于创建给定名称的表，必须拥有表CREATE的权限。
- <表名>：指定要创建表的名称，在 CREATE TABLE 之后给出，必须符合标识符命名规则。表名称被指定为 db_name.tbl_name，以便在特定的数据库中创建表。无论是否有当前数据库，都可以通过这种方式创建。在当前数据库中创建表时，可以省略 db-name。如果使用加引号的识别名，则应对数据库和表名称分别加引号。例如，'mydb'.'mytbl' 是合法的，但 'mydb.mytbl' 不合法。
- <表定义选项>：表创建定义，由列名（col_name）、列的定义（column_definition）以及可能的空值说明、完整性约束或表索引组成。
- 默认的情况是，表被创建到当前的数据库中。若表已存在、没有当前数据库或者数据库不存在，则会出现错误。

> 提示：使用 CREATE TABLE 创建表时，必须指定以下信息：
>
> - 要创建的表的名称不区分大小写，不能使用SQL语言中的关键字，如DROP、ALTER、INSERT等。
> - 数据表中每个列（字段）的名称和数据类型，如果创建多个列，要用逗号隔开。

创建员工表 tb_emp1，结构如下表所示。

| 字段名称 | 数据类型    | 备注         |
| -------- | ----------- | ------------ |
| id       | INT(ll)     | 员工编号     |
| name     | VARCHAR(25) | 员工名称     |
| deptld   | INT(ll)     | 所在部门编号 |
| salary   | FLOAT       | 工资         |

选择创建表的数据库 test_db，创建 tb_emp1 数据表，输入的 SQL 语句如下所示。

```sql
USE test_db;

CREATE TABLE tb_emp1
(
    id INT(11),
    name VARCHAR(25),
    deptId INT(11),
    salary FLOAT
);
```

### MySQL查看表结构命令

创建完数据表之后，经常需要查看表结构（表信息）。在 [MySQL](http://c.biancheng.net/mysql/) 中，可以使用 DESCRIBE 和 SHOW CREATE TABLE 命令来查看数据表的结构。

DESCRIBE/DESC 语句会以表格的形式来展示表的字段信息，包括字段名、字段数据类型、是否为主键、是否有默认值等，语法格式如下：

```sql
DESCRIBE <表名>;
```

或简写成：

```sql
DESC <表名>;
```

### 更多相关命令

操作表的命令繁多，较难记忆，可以用上面提供的图形化界面工具创建、修改表后，获取相关的表修改SQL

![](MySQL入门.assets/image-20210329145537484.png)

## MySQL操作表中数据

MySQL 提供了功能丰富的数据库管理语句，包括向数据库中插入数据的 INSERT 语句，更新数据的 UPDATE 语句，以及当数据不再使用时，删除数据的 DELETE 语句。

### MySQL SELECT：数据表查询语句

在 MySQL 中，可以使用 SELECT 语句来查询数据。查询数据是指从数据库中根据需求，使用不同的查询方式来获取不同的数据，是使用频率最高、最重要的操作。

SELECT 的语法格式如下：

```sql
SELECT{* | <字段列名>}[FROM <表 1>, <表 2>…[WHERE <表达式>[GROUP BY <group by definition>[HAVING <expression> [{<operator> <expression>}…]][ORDER BY <order by definition>][LIMIT[<offset>,] <row count>]]
```

其中，各条子句的含义如下：

- `{*|<字段列名>}`包含星号通配符的字段列表，表示所要查询字段的名称。
- `<表 1>，<表 2>…`，表 1 和表 2 表示查询数据的来源，可以是单个或多个。
- `WHERE <表达式>`是可选项，如果选择该项，将限定查询数据必须满足该查询条件。
- `GROUP BY< 字段 >`，该子句告诉 MySQL 如何显示查询出来的数据，并按照指定的字段分组。
- `[ORDER BY< 字段 >]`，该子句告诉 MySQL 按什么样的顺序显示查询出来的数据，可以进行的排序有升序（ASC）和降序（DESC），默认情况下是升序。
- `[LIMIT[<offset>，]<row count>]`，该子句告诉 MySQL 每次显示查询出来的数据条数。

```sql
use test_db;
SELECT * FROM tb_students_info;
```

### MySQL INSERT：插入数据

数据库与表创建成功以后，需要向数据库的表中插入数据。在 MySQL 中可以使用 INSERT 语句向数据库已有的表中插入一行或者多行元组数据。

INSERT 语句有两种语法形式，分别是 INSERT…VALUES 语句和 INSERT…SET 语句。

#### INSERT…VALUES语句

INSERT VALUES 的语法格式为：

```sql
INSERT INTO <表名> [ <列名1> [ , … <列名n>] ]
VALUES (值1) [… , (值n) ];
```

语法说明如下。

- `<表名>`：指定被操作的表名。
- `<列名>`：指定需要插入数据的列名。若向表中的所有列插入数据，则全部的列名均可以省略，直接采用 INSERT<表名>VALUES(…) 即可。
- `VALUES` 或 `VALUE` 子句：该子句包含要插入的数据清单。数据清单中数据的顺序要和列的顺序相对应。

#### INSERT…SET语句

语法格式为：

```sql
INSERT INTO <表名>
SET <列名1> = <值1>,
    <列名2> = <值2>,
    …
```

此语句用于直接给表中的某些列指定对应的列值，即要插入的数据的列名在 SET 子句中指定，col_name 为指定的列名，等号后面为指定的数据，而对于未指定的列，列值会指定为该列的默认值。

#### 详细说明

由 INSERT 语句的两种形式可以看出：

- 使用 INSERT…VALUES 语句可以向表中插入一行数据，也可以插入多行数据；
- 使用 INSERT…SET 语句可以指定插入行中每列的值，也可以指定部分列的值；
- INSERT…SELECT 语句向表中插入其他表的数据。
- 采用 INSERT…SET 语句可以向表中插入部分列的值，这种方式更为灵活；
- INSERT…VALUES 语句可以一次插入多条数据。

在 MySQL 中，用单条 INSERT 语句处理多个插入要比使用多条 INSERT 语句更快。

当使用单条 INSERT 语句插入多行数据的时候，只需要将每行数据用圆括号括起来即可。

```sql
INSERT INTO tb_courses
(course_id,course_name,course_grade,course_info)
VALUES(1,'Network',3,'Computer Network');
```

### MySQL UPDATE：修改数据

在 MySQL 中，可以使用 UPDATE 语句来修改、更新一个或多个表的数据。

使用 UPDATE 语句修改单个表，语法格式为：

```sql
UPDATE <表名> SET 字段 1=值 1 [,字段 2=值 2… ] [WHERE 子句 ]
[ORDER BY 子句] [LIMIT 子句]
```

语法说明如下：

- `<表名>`：用于指定要更新的表名称。
- `SET` 子句：用于指定表中要修改的列名及其列值。其中，每个指定的列值可以是表达式，也可以是该列对应的默认值。如果指定的是默认值，可用关键字 DEFAULT 表示列值。
- `WHERE` 子句：可选项。用于限定表中要修改的行。若不指定，则修改表中所有的行。
- `ORDER BY` 子句：可选项。用于限定表中的行被修改的次序。
- `LIMIT` 子句：可选项。用于限定被修改的行数。

> 注意：修改一行数据的多个列值时，SET 子句的每个值用逗号分开即可。

```sql
UPDATE tb_courses_new
SET course_grade=4;
```

### MySQL DELETE：删除数据

在 MySQL 中，可以使用 DELETE 语句来删除表的一行或者多行数据。

使用 DELETE 语句从单个表中删除数据，语法格式为：

```sql
DELETE FROM <表名> [WHERE 子句] [ORDER BY 子句] [LIMIT 子句]
```

语法说明如下：

- `<表名>`：指定要删除数据的表名。
- `ORDER BY` 子句：可选项。表示删除时，表中各行将按照子句中指定的顺序进行删除。
- `WHERE` 子句：可选项。表示为删除操作限定删除条件，若省略该子句，则代表删除该表中的所有行。
- `LIMIT` 子句：可选项。用于告知服务器在控制命令被返回到客户端前被删除行的最大值。

> 注意：在不使用 WHERE 条件的时候，将删除所有数据。

```sql
DELETE FROM tb_courses
WHERE course_id=4;
```


![](ADO.NET.assets/幻灯片0.png)

# ADO.NET

ADO.NET是.NET框架中的重要组件，主要用于完成C#应用程序访问数据库。

![ADO.NET概要](ADO.NET.assets/63651-20171023090555785-1366516311.png)

## ADO.NET的组成

![ADO.NET的组成](ADO.NET.assets/63651-20171023091053051-396149880.png)

* `System.Data`  →  DataTable，DataSet，DataRow，DataColumn，DataRelation，Constraint，DataColumnMapping，DataTableMapping
* `System.Data.Coummon`  →  各种数据访问类的基类和接口

*  `System.Data.SqlClient`  →  对Sql Server进行操作的数据访问类
  1. SqlConnection → 数据库连接器
  2. SqlCommand  → 数据库命名对象
  3. SqlCommandBuilder → 生存SQL命令
  4. SqlDataReader → 数据读取器
  5. SqlDataAdapter → 数据适配器，填充DataSet
  6. SqlParameter → 为存储过程定义参数
  7. SqlTransaction  → 数据库事物


## Connection连接对象

Connection对象也称为数据库连接对象，Connection对象的功能是负责对数据源的连接。所有Connection对象的基类都是DbConnection类。 

### 连接字符串

基本语法：数据源(Data Source)+数据库名称(Initial Catalog)+用户名(User ID)+密码(Password)

#### SQL Server连接字符串

标准安全连接： 

```bash
Data Source=.;Initial Catalog=myDataBase;User Id=myUsername;Password=myPassword;
# 或者
Server=myServerAddress;Database=myDataBase;User Id=myUsername;Password=myPassword;Trusted_Connection=False;
```

可信连接：

```bash
Data Source=myServerAddress;Initial Catalog=myDataBase;Integrated Security=SSPI;
# 或者
Server=myServerAddress;Database=myDatabase;Trusted_Connection=True; 
```

#### Access连接字符串

```bash
Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\myDatabase.mdb;User Id=admin;Password=;
```

#### MySQL连接字符串

```bash
Server=myServerAddress;Database=myDatabase;Uid=myUsername;Pwd=myPassword;
```

#### DB2连接字符串

```bash
Server=myAddress:myPortNumber;Database=myDatabase;UID=myUsername;PWD=myPassword;
```

#### Oracle连接字符串

```bash
Data Source=TORCL;User Id=myUsername;Password=myPassword; 
```

在VS中获得连接字符串并连接到数据库：

工具->连接到数据库

![在VS中获得连接字符串并连接到数据库](ADO.NET.assets/63651-20171023102955613-1462009058.png)

选择SQLServer

![选择SQLServer](ADO.NET.assets/63651-20171023103034644-781864166.png)

继续

![选择SQLServer](ADO.NET.assets/63651-20171023103108644-248861927.png)

如上图，填写好相关信息

在高级中可以查看连接字符串的所有信息

![选择SQLServer](ADO.NET.assets/63651-20171023103402301-1309721307.png)

在VS中可以实现数据库管理：

![在VS中可以实现数据库管理](ADO.NET.assets/63651-20171023103552863-1051673337.png)

> 注意，这里使用的localdb（可以在SQL Server对象管理器中右键查看连接属性获得以下信息）
>
> * 连接字符串为`Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=MyCar;Integrated Security=True;`
> * 默认数据库位置在：`C:\Users\lgc653\AppData\Local\Microsoft\Microsoft SQL Server Local DB\Instances\MSSQLLocalDB`
>   * 连接字符串除了代码中有，Form2用到的MyCarDataSet的连接字符串保存在app.config里面

### 连接到数据库

Connection对象有两个重要属性： 

1. **ConnectionString**：表示用于打开 SQL Server 数据库的字符串；
2. **State**：表示 Connection 的状态，有Closed和Open两种状态。 

Connection对象有两个重要方法：

1. **Open()方法**：指示打开数据库；
2. **Close()方法**：指示关闭数据库。

```csharp
//创建连接对象1
using (SqlConnection conn1 = new SqlConnection("连接字符串"))    
{        
    conn1.Open();       
}
```

### 示例

我们这里使用MSSQLLocalDB，`窗口——》SQL Server对象资源管理器`

![image-20210426202415543](ADO.NET.assets/image-20210426202415543.png)

> 项目的启动可以在Program.cs中设置
>
> * FormTest1 —— CarType
> * Form1 —— 增删查
> * Form2 —— DataSet
> * FormCar —— 编辑案例
>
> ` Application.Run(new Form1());

```csharp
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace MyCar
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
```

#### 创建数据库与表

```sql
/**创建数据库*/
create database MyCar;
go
use MyCar;
/**创建表*/
create table Car
(
Id int primary key identity(1,1),  --编号
Title nvarchar(128) not null,  --车名
Speed int default(0),  --车速
Info ntext --详细
)

/**添加数据*/
insert into Car(Title,Speed,Info)
select 'BYD',130,'比亚迪' union
select 'BMW',160,'宝马' union
select 'Benz',160,'奔驰' 

/**查询*/
SELECT [Id]
      ,[Title]
      ,[Speed]
      ,[Info]
  FROM [MyCar].[dbo].[Car]
GO

/**创建表*/
create table CarType
(
Id int primary key identity(1,1),  --编号
TypeName nvarchar(128) not null,  --车类别名
Memo ntext --详细
)
```

####  创建窗体项目MyCar

![创建窗体项目MyCar](ADO.NET.assets/63651-20171023111111160-163134555.png)

#### 连接到数据

创建连接对象，打开数据

```csharp
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;

namespace MyCar
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnConnection_Click(object sender, EventArgs e)
        {
            //创建连接对象，指定连接字符串参数
            SqlConnection conn = new SqlConnection("Data Source=.;Initial Catalog=MyCar;User Id=sa;Password=sa;");
            //打开数据
            conn.Open();
            MessageBox.Show("打开成功，状态"+conn.State);
            conn.Close();
            MessageBox.Show("关闭数据库成功");
        }
    }
}
```

执行结果：

![执行结果](ADO.NET.assets/63651-20171023112007348-499460254.png)

## Command对象

Command对象也称为数据库命令对象，Command对象主要执行包括添加、删除、修改及查询数据的操作的命令。也可以用来执行存储过程。用于执行存储过程时需要将Command对象的CommandType 属性设置为CommandType.StoredProcedure，默认情况下CommandType 属性为CommandType.Text，表示执行的是普通SQL语句。

Command主要有三个方法： 

### ExecuteNonQuery

**ExecuteNonQuery()：**执行一个SQL语句，返回受影响的行数，这个方法主要用于执行对数据库执行增加、更新、删除操作，注意查询的时候不是调用这个方法。用于完成insert，delete，update操作。

```csharp
//新增
private void btnAdd_Click(object sender, EventArgs e)
{
    //创建连接对象，并使用using释放（关闭），连接用完后会被自动关闭
    using (SqlConnection conn=new SqlConnection("server=.;uid=sa;pwd=sa;database=MyCar"))
    {
        //打开连接
        conn.Open();
        //将执行的sql
        String sql = "INSERT INTO Car([Title] ,[Speed] ,[Info]) VALUES('奇瑞' ,190,'国产轿车')";
        //创建命令对象，指定要执行sql语句与连接对象conn
        SqlCommand cmd = new SqlCommand(sql,conn);
        //执行,返回影响行数
        int rows=cmd.ExecuteNonQuery();
        if (rows > 0) MessageBox.Show("新增成功！");
    }
}
```

执行结果：

![执行结果](ADO.NET.assets/63651-20171023142501644-897744257.png)**

![执行结果](ADO.NET.assets/63651-20171023142521019-1990609235.png)

#### 拼接字符串

```csharp
private void btnAdd_Click(object sender, EventArgs e)
{
    //创建连接对象，并使用using释放（关闭），连接用完后会被自动关闭
    using (SqlConnection conn=new SqlConnection("server=.;uid=sa;pwd=sa;database=MyCar"))
    {
        //打开连接
        conn.Open();
        //将执行的sql
        String sql =String.Format("INSERT INTO Car([Title] ,[Speed] ,[Info]) VALUES('{0}' ,{1},'{2}')"
        ,txtTitle.Text,txtSpeed.Text,txtInfo.Text);
        //创建命令对象，指定要执行sql语句与连接对象conn
        SqlCommand cmd = new SqlCommand(sql,conn);
        //执行,返回影响行数
        int rows=cmd.ExecuteNonQuery();
        if (rows > 0) MessageBox.Show("新增成功！");
    }
}
```

执行：

![拼接字符串](ADO.NET.assets/63651-20171023150914832-1330077027.png)

### 参数

如果直接拼接字符串会存在安全隐患，使用参数可以解决问题。

```csharp
private void btnAdd_Click(object sender, EventArgs e)
{
    //创建连接对象，并使用using释放（关闭），连接用完后会被自动关闭
    using (SqlConnection conn=new SqlConnection("server=.;uid=sa;pwd=sa;database=MyCar"))
    {
        //打开连接
        conn.Open();
        //将执行的sql
        String sql = "INSERT INTO  Car([Title] ,[Speed] ,[Info]) VALUES(@Ttile,@Speed,@Info)";
        //创建命令对象，指定要执行sql语句与连接对象conn
        SqlCommand cmd = new SqlCommand(sql,conn);
        //指定参数
        cmd.Parameters.Add(new SqlParameter("@Ttile", txtTitle.Text));
        cmd.Parameters.Add(new SqlParameter("@Speed", txtSpeed.Text));
        cmd.Parameters.Add(new SqlParameter("@Info", txtInfo.Text));
        //执行,返回影响行数
        int rows=cmd.ExecuteNonQuery();
        if (rows > 0) MessageBox.Show("新增成功！");
    }
}
```

执行结果：

![执行结果](ADO.NET.assets/63651-20171023151631316-661121542.png)

### 删除

这里的示例是insert，如果想执行delete与update代码是一样的，只是变化了SQL。

示例：

```csharp
/// <summary>
/// 删除
/// </summary>
private void btnDelete_Click(object sender, EventArgs e)
{
    //创建连接对象，并使用using释放（关闭），连接用完后会被自动关闭
    using (SqlConnection conn = new SqlConnection("server=.;uid=sa;pwd=sa;database=MyCar"))
    {
        //打开连接
        conn.Open();
        //将执行的sql
        String sql = "delete from Car where Title=@Title";
        //创建命令对象，指定要执行sql语句与连接对象conn
        SqlCommand cmd = new SqlCommand(sql, conn);
        //指定参数
        cmd.Parameters.Add(new SqlParameter("@Title", txtTitle.Text));
        //执行,返回影响行数
        int rows = cmd.ExecuteNonQuery();
        MessageBox.Show("删除成功"+rows+"行！");
    }
}
```

执行结果：

![执行结果](ADO.NET.assets/63651-20171023155917691-1334457773.png)

### ExecuteScalar ()

**ExecuteScalar ()**从数据库检索单个值。这个方法主要用于统计操作。ExecuteScalar ()这个方法是针对SQL语句执行的结果是一行一列的结果集，这个方法只返回查询结果集的第一行第一列。

executeScalar主要用于查询单行单列的值，如聚合函数（count,max,min,agv,sum）。

![ExecuteScalar ()](ADO.NET.assets/63651-20171023160349176-28343421.png)

![*ExecuteScalar ()](ADO.NET.assets/63651-20171023161232144-676857705.png)

示例：

```csharp
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;

namespace MyCar
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            GetCount();
        }

        private void btnConnection_Click(object sender, EventArgs e)
        {
            //创建连接对象，指定连接字符串参数
            SqlConnection conn = new SqlConnection("Data Source=.;Initial Catalog=MyCar;User Id=sa;Password=sa;");
            //打开数据
            conn.Open();
            MessageBox.Show("打开成功，状态" + conn.State);
            conn.Close();
            MessageBox.Show("关闭数据库成功");
        }
    
        //新增
        private void btnAdd_Click(object sender, EventArgs e)
        {
            //创建连接对象，并使用using释放（关闭），连接用完后会被自动关闭
            using (SqlConnection conn = new SqlConnection("server=.;uid=sa;pwd=sa;database=MyCar"))
            {
                //打开连接
                conn.Open();
                //将执行的sql
                String sql = "INSERT INTO Car(Title ,Speed ,Info) VALUES(@Ttile,@Speed,@Info)";
                //创建命令对象，指定要执行sql语句与连接对象conn
                SqlCommand cmd = new SqlCommand(sql, conn);
                //指定参数
                cmd.Parameters.Add(new SqlParameter("@Ttile", txtTitle.Text));
                cmd.Parameters.Add(new SqlParameter("@Speed", txtSpeed.Text));
                cmd.Parameters.Add(new SqlParameter("@Info", txtInfo.Text));
                //执行,返回影响行数
                int rows = cmd.ExecuteNonQuery();
                if (rows > 0) { MessageBox.Show("新增成功！"); GetCount(); }
            }
        }
    
        /// <summary>
        /// 删除
        /// </summary>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            //创建连接对象，并使用using释放（关闭），连接用完后会被自动关闭
            using (SqlConnection conn = new SqlConnection("server=.;uid=sa;pwd=sa;database=MyCar"))
            {
                //打开连接
                conn.Open();
                //将执行的sql
                String sql = "delete from Car where Title=@Title";
                //创建命令对象，指定要执行sql语句与连接对象conn
                SqlCommand cmd = new SqlCommand(sql, conn);
                //指定参数
                cmd.Parameters.Add(new SqlParameter("@Title", txtTitle.Text));
                //执行,返回影响行数
                int rows = cmd.ExecuteNonQuery();
                MessageBox.Show("删除成功" + rows + "行！");
            }
        }
    
        /// <summary>
        /// 查询单行单列的值
        /// </summary>
        private void btnScalar_Click(object sender, EventArgs e)
        {
            GetCount();
        }
    
        private void GetCount()
        {
            //创建连接对象，并使用using释放（关闭），连接用完后会被自动关闭
            using (SqlConnection conn = new SqlConnection("server=.;uid=sa;pwd=sa;database=MyCar"))
            {
                //打开连接
                conn.Open();
                //将执行的sql
                String sql = "select COUNT(*) from Car";
                //创建命令对象，指定要执行sql语句与连接对象conn
                SqlCommand cmd = new SqlCommand(sql, conn);
                //执行查询返回单行单列的值，Object类型
                Object result = cmd.ExecuteScalar();
                //显示结果到标签
                lblCount.Text = result.ToString();
            }
        }
    }
}
```

运行结果：

![运行结果](ADO.NET.assets/63651-20171023161315754-1968733520.png)

可能返回NULL值，需要对结果进行判断，如下：

```csharp
object my = cmd.ExecuteScalar();
if (object.Equals(my,null))  //可以使用Equals进行Null值的判断，易读性强
  Console.WriteLine("Not Data");
else
  Console.WriteLine("Yes"); 
```

## ExecuteReader获得数据

ExecuteReader用于实现只进只读的高效数据查询。

ExecuteReader：返回一个SqlDataReader对象，可以通过这个对象来检查查询结果，它提供了只进只读的执行方式，即从结果中读取一行之后，移动到另一行，则前一行就无法再用。有一点要注意的是执行之后，要等到手动去调用Read()方法之后，DataReader对象才会移动到结果集的第一行，同时此方法也返回一个Bool值，表明下一行是否可用，返回True则可用，返回False则到达结果集末尾。

使用DataReader可以提高执行效率，有两种方式可以提高代码的性能：

一种是基于序号的查找

一个是使用适当的Get方法来查找。因为查询出来的结果一般都不会改变，除非再次改动查询语句，因此可以通过定位列的位置来查找记录。用这种方法有一个问题，就是可能知道一列的名称而不知道其所在的位置，这个问题的解决方案是通过调用DataReader 对象的GetOrdinal()方法，此方法接收一个列名并返回此列名所在的列号。

### 使用ExecuteReader实现数据查询

示例代码：

```csharp
//查询
private void btnQuery_Click(object sender, EventArgs e)
{
    //创建连接对象，并使用using释放（关闭），连接用完后会被自动关闭
    using (SqlConnection conn = new SqlConnection("server=.;uid=sa;pwd=sa;database=MyCar"))
    {
        //打开连接
        conn.Open();
        //将执行的sql
        String sql = "select Id,Title,Speed,Info from Car";
        //创建命令对象，指定要执行sql语句与连接对象conn
        SqlCommand cmd = new SqlCommand(sql, conn);
        //执行查询返回结果集
        SqlDataReader sdr = cmd.ExecuteReader();
        //下移游标，读取一行，如果没有数据了则返回false
        while (sdr.Read())
        {
        	Console.WriteLine("编号：" + sdr["Id"] + "，车名：" + sdr["Title"] + "，速度：" + sdr["Speed"]);
        }
    }
}
```

运行结果：

![运行结果](ADO.NET.assets/63651-20171024101625973-1892314878.png)

### 实体类

实体类用于封装及映射数据。

```csharp
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyCar
{
    /// <summary>
    /// 汽车实体类
    /// </summary>
    public class Car
    {
        /// <summary>
        /// 编号
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 车名
        /// </summary>
        public String Title { get; set; }
        /// <summary>
        /// 速度
        /// </summary>
        public int Speed { get; set; }
        /// <summary>
        /// 详细
        /// </summary>
        public String Info { get; set; }
    }
}
```

### DataGridView展示数据

示例代码：

```csharp
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;

namespace MyCar
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            GetCount();
            BindData();

            List<User> users = new List<User>();
    
            User tom = new User();
            tom.Name = "Tom";
            tom.Age = "18";
            users.Add(tom);
    
            User rose = new User();
            rose.Name = "Rose";
            rose.Age = "88";
            users.Add(rose);
    
            dataGridView1.DataSource = users;
        }
    
        private void btnConnection_Click(object sender, EventArgs e)
        {
            //创建连接对象，指定连接字符串参数
            SqlConnection conn = new SqlConnection("Data Source=.;Initial Catalog=MyCar;User Id=sa;Password=sa;");
            //打开数据
            conn.Open();
            MessageBox.Show("打开成功，状态" + conn.State);
            conn.Close();
            MessageBox.Show("关闭数据库成功");
        }
    
        //新增
        private void btnAdd_Click(object sender, EventArgs e)
        {
            //创建连接对象，并使用using释放（关闭），连接用完后会被自动关闭
            using (SqlConnection conn = new SqlConnection("server=.;uid=sa;pwd=sa;database=MyCar"))
            {
                //打开连接
                conn.Open();
                //将执行的sql
                String sql = "INSERT INTO Car(Title ,Speed ,Info) VALUES(@Ttile,@Speed,@Info)";
                //创建命令对象，指定要执行sql语句与连接对象conn
                SqlCommand cmd = new SqlCommand(sql, conn);
                //指定参数
                cmd.Parameters.Add(new SqlParameter("@Ttile", txtTitle.Text));
                cmd.Parameters.Add(new SqlParameter("@Speed", txtSpeed.Text));
                cmd.Parameters.Add(new SqlParameter("@Info", txtInfo.Text));
                //执行,返回影响行数
                int rows = cmd.ExecuteNonQuery();
                if (rows > 0) { MessageBox.Show("新增成功！"); GetCount(); BindData(); }
            }
        }
    
        /// <summary>
        /// 删除
        /// </summary>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            //创建连接对象，并使用using释放（关闭），连接用完后会被自动关闭
            using (SqlConnection conn = new SqlConnection("server=.;uid=sa;pwd=sa;database=MyCar"))
            {
                //打开连接
                conn.Open();
                //将执行的sql
                String sql = "delete from Car where Title=@Title";
                //创建命令对象，指定要执行sql语句与连接对象conn
                SqlCommand cmd = new SqlCommand(sql, conn);
                //指定参数
                cmd.Parameters.Add(new SqlParameter("@Title", txtTitle.Text));
                //执行,返回影响行数
                int rows = cmd.ExecuteNonQuery();
                MessageBox.Show("删除成功" + rows + "行！");
            }
        }
    
        /// <summary>
        /// 查询单行单列的值
        /// </summary>
        private void btnScalar_Click(object sender, EventArgs e)
        {
            GetCount();
        }
    
        private void GetCount()
        {
            //创建连接对象，并使用using释放（关闭），连接用完后会被自动关闭
            using (SqlConnection conn = new SqlConnection("server=.;uid=sa;pwd=sa;database=MyCar"))
            {
                //打开连接
                conn.Open();
                //将执行的sql
                String sql = "select COUNT(*) from Car";
                //创建命令对象，指定要执行sql语句与连接对象conn
                SqlCommand cmd = new SqlCommand(sql, conn);
                //执行查询返回单行单列的值，Object类型
                Object result = cmd.ExecuteScalar();
                //显示结果到标签
                lblCount.Text = result.ToString();
            }
        }
    
        //查询
        private void btnQuery_Click(object sender, EventArgs e)
        {
            BindData();
        }
    
        private void BindData()
        {
            //定义一个集合，用于存放汽车对象
            List<Car> cars = new List<Car>();
    
            //创建连接对象，并使用using释放（关闭），连接用完后会被自动关闭
            using (SqlConnection conn = new SqlConnection("server=.;uid=sa;pwd=sa;database=MyCar"))
            {
                //打开连接
                conn.Open();
                //将执行的sql
                String sql = "select Id,Title,Speed,Info from Car";
                //创建命令对象，指定要执行sql语句与连接对象conn
                SqlCommand cmd = new SqlCommand(sql, conn);
                //执行查询返回结果集
                SqlDataReader sdr = cmd.ExecuteReader();
                //下移游标，读取一行，如果没有数据了则返回false
                while (sdr.Read())
                {
                    //每一行记录表示一辆车，则实例化一个汽车对象
                    Car car = new Car();
                    car.Id = Convert.ToInt32(sdr["Id"]);  //取得数据库中当前行的Id转换成int类型给对象的Id属性赋值
                    car.Title = sdr["Title"] + "";
                    car.Speed = Convert.ToInt32(sdr["Speed"]);
                    car.Info = sdr["Info"] + "";
                    cars.Add(car);  //将汽车对象添加到集合中
                }
                //绑定数据到控件
                dgvCar.DataSource = cars;
                sdr.Close();  //关闭
            }
        }
    }
}
```
运行结果：

![运行结果](ADO.NET.assets/63651-20171024111639816-143590460.png)

### 删除功能

示例代码：

```csharp
/// <summary>
/// 删除
/// </summary>
private void btnDelete_Click(object sender, EventArgs e)
{
    //SelectedRows选中的行,[0]行，[0]列，Value值
    int id = Convert.ToInt32(dgvCar.SelectedRows[0].Cells[0].Value);
    //创建连接对象
    using (SqlConnection conn = new SqlConnection("server=.;uid=sa;pwd=sa;database=MyCar"))
    {
        conn.Open();  //打开连接
        string sql = "delete from Car where Id=@Id";
        SqlCommand cmd = new SqlCommand(sql, conn);  //sql命令对象
        cmd.Parameters.Add(new SqlParameter("@Id",id));  //指定参数
        int rows = cmd.ExecuteNonQuery();  //执行并返回影响行数
        MessageBox.Show("删除成功"+rows+"行！");
        BindCar();  //重新绑定
    }
}
```

运行结果：

![运行结果](ADO.NET.assets/63651-20171024153749348-623150887.png)

## 5.5、编辑功能

示例代码：

FormCar.cs编辑按钮

```csharp
/// <summary>
/// 编辑
/// </summary>
private void btnEdit_Click(object sender, EventArgs e)
{
    //获得当前选择行的索引
    int index = dgvCar.SelectedRows[0].Index;
    //从集合中获得索引对应的汽车对象
    Car car = cars[index];

    FormEdit edit = new FormEdit();
    edit.car = car;
    edit.ShowDialog();  //打开模式窗口
    BindCar();  //重新绑定
}
```

FormEdit.cs代码：

```csharp
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyCar
{
    public partial class FormEdit : Form
    {

        /// <summary>
        /// 要编辑的汽车对象
        /// </summary>
        public Car car { get; set; }
    
        public FormEdit()
        {
            InitializeComponent();
        }
    
        private void FormEdit_Load(object sender, EventArgs e)
        {
            lblId.Text = car.Id+"";
            txtTitle.Text = car.Title;
            txtSpeed.Text = car.Speed+"";
            txtInfo.Text = car.Info;
        }
    
        //保存
        private void btnSave_Click(object sender, EventArgs e)
        {
            //创建连接对象
            using (SqlConnection conn = new SqlConnection("server=.;uid=sa;pwd=sa;database=MyCar"))
            {
                conn.Open();  //打开连接
                string sql = "update Car set Title=@Title,Speed=@Speed,Info=@Info where Id=@Id";
                SqlCommand cmd = new SqlCommand(sql, conn);  //sql命令对象
                cmd.Parameters.Add(new SqlParameter("@Id", car.Id));  //指定参数
                cmd.Parameters.Add(new SqlParameter("@Title", txtTitle.Text));  //指定参数
                cmd.Parameters.Add(new SqlParameter("@Speed", txtSpeed.Text));  //指定参数
                cmd.Parameters.Add(new SqlParameter("@Info", txtInfo.Text));  //指定参数
                int rows = cmd.ExecuteNonQuery();  //执行并返回影响行数
                MessageBox.Show("修改成功" + rows + "行！");
            }
        }
    }
}
```

运行结果：

![运行结果](ADO.NET.assets/63651-20171024160051160-1603651918.png)

## 综合示例

这里我们将使用Mysql进行开发，首先通过nuget安装Mysql的支持，安装完包会被下载到`c:\Users\lgc653\.nuget\packages\mysql.data\`

![](ADO.NET.assets/image-20210426204604512.png)

完成一个人事管理系统（HR）中的员工（Emp）管理模块，要求实现如下功能：

### 创建数据库与表

#### 创建HR数据库

```sql
--创建数据库
create database HR;
```
结果：

![创建HR数据库](ADO.NET.assets/63651-20171026094433348-137482140.png)

### 创建Emp员工表

Emp员工表（编号Id、姓名Name、电话Phone、身高Height、备注Memo）

```sql
--创建数据库
create database HR;
use HR;
--Emp员工表（编号Id、姓名Name、电话Phone、身高Height、备注Memo）
--创建表
create table Emp
(
Id int primary key identity(100000,1),  --编号
Name nvarchar(32) not null,  --姓名
Phone varchar(32), --电话
Height int, -- 身高
Memo ntext --备注
)
--添加数据
insert into Emp(Name,Phone,Height,Memo) values('李天明','13723887780',158,'单身');
--查询
select Id,Name,Phone,Height,Memo from Emp;
--删除
delete from Emp where Id=100000
--修改
update Emp set Name='李地明',Phone='13723887789',Height=149 where Id=100001
```

结果：

![创建Emp员工表](ADO.NET.assets/63651-20171026095627144-2015889045.png)

### 创建项目与实体类

#### 创建项目

这里同样创建一个WinForms窗体项目

![创建一个WinForms窗体项目](ADO.NET.assets/63651-20171026095718473-684990205.png)

#### 创建实体类

```csharp
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HR.Models
{
    /// <summary>
    /// 员工
    /// </summary>
    public class Emp
    {
        /// <summary>
        /// 编号
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        public String Name { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        public String Phone { get; set; }
        /// <summary>
        /// 身高
        /// </summary>
        public int Height { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public String Memo { get; set; }

    }
}
```

#### 封装数据访问

为了避免重复的数据访问代码，这里我们封装了一个数据库访问工具类：SqlHelper

```csharp
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;

namespace HR.Utils
{
    /// <summary>
    /// 用于访问SQLServer数据库的工具类
    /// </summary>
    public class SqlHelper
    {
        /// <summary>
        /// 连接字符串 write once,only once!  
        /// </summary>
        public static String connString = "server=.;uid=sa;pwd=sa;database=HR";


        /// <summary>
        /// 完成增，删，改
        /// </summary>
        /// <param name="sql">将要执行的sql</param>
        /// <param name="ps">可变参数，指定sql中的参数</param>
        /// <returns>影响行数</returns>
        public static int Execute(String sql, params SqlParameter[] ps)
        {
            using (SqlConnection conn = new SqlConnection(connString))
            {
                //打开连接
                conn.Open();
                //创建命令对象，指定sql与连接对象conn
                SqlCommand cmd = new SqlCommand(sql, conn);
                //指定参数
                if (ps != null) cmd.Parameters.AddRange(ps);
                //执行sql命令，返回影响行数
                return cmd.ExecuteNonQuery();
            }
        }
    
        /// <summary>
        /// 执行查询，返回SqlDataReader，一定要关闭
        /// </summary>
        /// <param name="sql">将要执行的sql</param>
        /// <param name="ps">可变参数，指定sql中的参数</param>
        /// <returns>SqlDataReader结果集</returns>
        public static SqlDataReader Reader(String sql, params SqlParameter[] ps)
        {
            //定义一个连接对象，指定连接字符串using,sa sa MyCar .
            SqlConnection conn = new SqlConnection(connString);
            //打开数据库
            conn.Open();
            //定义命令对象，指定要执行的sql与conn连接参数
            SqlCommand cmd = new SqlCommand(sql, conn);
            //指定参数
            if (ps != null) cmd.Parameters.AddRange(ps);
            //执行SQL查询，返回结果集给sdr，关闭reader时也关闭连接
            return cmd.ExecuteReader(CommandBehavior.CloseConnection);
        }
    
    }
}
```

调用办法：

增删改：

```csharp
int rows = SqlHelper.Execute("delete from Emp where Id=@id",new SqlParameter("id",100002));
MessageBox.Show(rows+"");
```

查询：

```csharp
SqlDataReader sdr = SqlHelper.Reader("select * from Emp where Id=@id",new SqlParameter("id",100003));
if (sdr.Read())
{
	MessageBox.Show(sdr["Name"]+"");
}
sdr.Close();
```

### 实现展示功能

示例代码：

```csharp
#region 绑定员工信息到网格
public void BindData()
{
    emps = new List<Emp>();
    //执行查询获得结果集
    SqlDataReader sdr = SqlHelper.Reader("select Id,Name,Phone,Height,Memo from Emp where Name like @Name",new SqlParameter("@Name",'%'+txtName.Text+"%"));
    while (sdr.Read()) 
    {
        Emp emp = new Emp();
        emp.Id = Convert.ToInt32(sdr["Id"]);
        emp.Name = sdr["Name"] + "";
        emp.Phone = sdr["Phone"] + "";
        emp.Height = Convert.ToInt32(sdr["Height"]);
        emp.Memo = sdr["Memo"] + "";
        emps.Add(emp);
    }
    sdr.Close();
    dgvEmp.DataSource = emps;
}
#endregion
```

运行结果：

 ![运行结果](ADO.NET.assets/63651-20171026113458988-1627015964.png)

### 实现新增功能

示例代码：

按钮事件：

```csharp
FormAdd add = new FormAdd();
add.ShowDialog();
BindData();
```

新增窗口：

```csharp
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using HR.Utils;
using System.Data.SqlClient;

namespace HR
{
    public partial class FormAdd : Form
    {
        public FormAdd()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string sql = "insert into Emp(Name,Phone,Height,Memo) values(@Name,@Phone,@Height,@Memo);";
            int rows = SqlHelper.Execute(sql,
                new SqlParameter("@Name", txtName.Text),
                new SqlParameter("@Phone", txtPhone.Text),
                new SqlParameter("@Height", txtHeight.Text),
                new SqlParameter("@Memo", txtMemo.Text));
            MessageBox.Show("新增成功"+rows+"行!");
        }
    }
}
```

运行结果：

![运行结果](ADO.NET.assets/63651-20171026113633629-866688590.png)

### 实现删除功能

示例代码：

```csharp
#region 删除
private void btnDelete_Click(object sender, EventArgs e)
{
    int id =Convert.ToInt32(dgvEmp.SelectedRows[0].Cells[0].Value);
    int rows = SqlHelper.Execute("delete from Emp where Id=@Id",new SqlParameter("@Id",id));
    MessageBox.Show("删除成功"+rows+"行");
    BindData();
}
#endregion
```

运行结果：

 ![运行结果](ADO.NET.assets/63651-20171026113706176-260767037.png)

### 实现编辑功能

示例代码：

按钮事件：

```csharp
//取得索引
int index=dgvEmp.SelectedRows[0].Index;
FormEdit edit = new FormEdit();
edit.emp = emps[index];
edit.ShowDialog();
BindData(); 
```

窗体代码：

```csharp
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.SqlClient;
using HR.Utils;
using HR.Models;

namespace HR
{
    public partial class FormEdit : Form
    {
        public FormEdit()
        {
            InitializeComponent();
        }

        public Emp emp { get; set; }
    
        private void btnSave_Click(object sender, EventArgs e)
        {
            string sql = "update Emp set Name=@Name,Phone=@Phone,Height=@Height,Memo=@Memo Where Id=@Id";
            int rows = SqlHelper.Execute(sql,
                new SqlParameter("@Name", txtName.Text),
                new SqlParameter("@Phone", txtPhone.Text),
                new SqlParameter("@Height", txtHeight.Text),
                new SqlParameter("@Memo", txtMemo.Text),
                new SqlParameter("@Id", emp.Id));
                            
            MessageBox.Show("修改成功" + rows + "行!");
        }
    
        private void FormEdit_Load(object sender, EventArgs e)
        {
            txtHeight.Text = emp.Height + "";
            txtMemo.Text = emp.Memo;
            txtName.Text = emp.Name;
            txtPhone.Text = emp.Phone;
        }
    }
}
```

运行结果：

![运行结果](ADO.NET.assets/63651-20171026114152519-1546115984.png)

### 实现搜索功能

示例代码：

按钮：

```csharp
#region 查询
private void btnQuery_Click(object sender, EventArgs e)
{
    BindData();
}
#endregion
```

BindData()方法

```csharp
#region 绑定员工信息到网格
public void BindData()
{
    emps = new List<Emp>();
    //执行查询获得结果集
    SqlDataReader sdr = SqlHelper.Reader("select Id,Name,Phone,Height,Memo from Emp where Name like @Name",new SqlParameter("@Name",'%'+txtName.Text+"%"));
    while (sdr.Read()) 
    {
        Emp emp = new Emp();
        emp.Id = Convert.ToInt32(sdr["Id"]);
        emp.Name = sdr["Name"] + "";
        emp.Phone = sdr["Phone"] + "";
        emp.Height = Convert.ToInt32(sdr["Height"]);
        emp.Memo = sdr["Memo"] + "";
        emps.Add(emp);
    }
    sdr.Close();
    dgvEmp.DataSource = emps;
}
#endregion
```

运行结果：

![运行结果](ADO.NET.assets/63651-20171026113728410-2009628031.png)

### 所有代码与扩展

![所有代码与扩展](ADO.NET.assets/63651-20171026121321348-656077150.png)

### 拓展练习——实现上传图片功能

![](ADO.NET.assets/image-20210426212859437.png)

* OpenFileDialog和PictureBox的应用
* Base64String和图片对象的相互转换
* Base64String长字符串的存储
  * 字符长度
  * my.ini的相关配置

上传图片并保存至数据库代码

```csharp
//选择照片
OpenFileDialog dialog = new OpenFileDialog();
dialog.Multiselect = false;//该值确定是否可以选择多个文件
dialog.Title = "请选择文件夹"; //窗体标题
dialog.Filter = "图片文件(*.jpg,*.png)|*.jpg;*.png"; //文件筛选
//默认路径设置为我的电脑文件夹
dialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyComputer);
if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
{
    String file = dialog.FileName;//文件夹路径
    String path = file.Substring(file.LastIndexOf("\\") + 1); //格式化处理，提取文件名
    picEmp.SizeMode = PictureBoxSizeMode.StretchImage; //使图像拉伸或收缩，以适合PictureBox
    this.picEmp.ImageLocation = file;
    //图片转为base64编码的字符串
    Bitmap bmp = new Bitmap(file);
    MemoryStream ms = new MemoryStream();
    bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
    byte[] arr = new byte[ms.Length];
    ms.Position = 0;
    ms.Read(arr, 0, (int)ms.Length);
    ms.Close();
    // 得到base64string存到数据库中即可，PIC字段最好是MEDIUMTEXT
    // 注意修改my.ini的max_allowed_packet、innodb_log_file_size
    string base64string = Convert.ToBase64String(arr);
    string sql = "update Emp set Pic=@Pic Where Id=@Id";
    int rows = SqlHelper.Execute(sql,
                                 new MySqlParameter("@Pic", base64string),  
                                 new MySqlParameter("@Id", emp.Id));

    MessageBox.Show("上传图片成功" + rows + "行!");
}
```

数据库读取并显示到PictureBox代码

```csharp
String imageCode = emp.Pic;
if (!string.IsNullOrEmpty(imageCode))
{
    byte[] bytes = Convert.FromBase64String(imageCode);
    MemoryStream s = new MemoryStream(bytes, true);
    s.Write(bytes, 0, bytes.Length);
    Image pic = new Bitmap(s);
    Bitmap bit = new Bitmap(this.picEmp.Width, this.picEmp.Height);
    Graphics g = Graphics.FromImage(bit);//从指定的 Image 创建新的 Graphics(绘图)。
    g.DrawImage(pic, new Rectangle(0, 0, bit.Width, bit.Height), new Rectangle(0, 0, pic.Width, pic.Height), GraphicsUnit.Pixel);
    this.picEmp.Image = bit;
}
```



---

![](ADO.NET.assets/幻灯片2.png)
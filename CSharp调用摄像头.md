# C#调用摄像头

本文将讲诉在C#的winform程序中如何调用摄像头:

1. 调用USB摄像头
2. 调用普通IP摄像头
3. 通过摄像头SDK进行调用

## 使用的DLL

操作摄像头可以通过一个第三方的组件实现的，名字叫做 [AForge ](http://www.aforgenet.com/)

AForge.NET是一个专门为开发者和研究者基于C#框架设计的，这个框架提供了不同的类库和关于类库的资源，还有很多应用程序例子，包括计算机视觉与人工智能，图像处理，神经网络，遗传算法，机器学习，机器人等领域。

这个框架由一系列的类库组成。主要包括有：

* AForge.Imaging —— 一些日常的图像处理和过滤器
* AForge.Vision —— 计算机视觉应用类库
* AForge.Neuro —— 神经网络计算库AForge.Genetic -进化算法编程库
* AForge.MachineLearning —— 机器学习类库
* AForge.Robotics —— 提供一些机器人的工具类库
* AForge.Video —— 一系列的视频处理类库
* AForge.Fuzzy —— 模糊推理系统类库
* AForge.Controls—— 图像，三维，图表显示控件

![](CSharp调用摄像头.assets/162332073675754.jpg)

## 调用USB摄像头
调用USB摄像头其实比较简单,就是通过读取电脑自身所拥有的设备数，再执行预览。
videoSourcePlayer是AForge中的控件。

```c#
private FilterInfoCollection videoDevices;
this.videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
if (this.videoDevices.Count > 0)
{
    VideoCaptureDevice source = new VideoCaptureDevice(this.videoDevices[0].MonikerString);
    this.videoSourcePlayer.SignalToStop();
    this.videoSourcePlayer.WaitForStop();
    this.videoSourcePlayer.VideoSource = source;
    this.videoSourcePlayer.Start();
}
```

## 调用普通IP摄像头
首先要确认HTTP方式传送的图片的地址，我用的SAMSUN地址是`http://{0}/cgi-bin/video.cgi?msubmenu=jpg`，还有其他的`http://{0}/axis-cgi/jpg/image.cgi?camera=1`
使用JPEGStream或者MJPEGStream，有用户名和密码的就加上。有了source其他的和上面USB一样。

```c#
JPEGStream source = new JPEGStream(URL);
//MJPEGStream source = new MJPEGStream(URL);
source.Login = username;
source.Password = password;
this.OpenVideoSource(source);
```

## 通过摄像头SDK进行调用
这个调用还是很简单的，一般都是使用控件的Handle来进行预览的，我们可能会想取每一帧的图片，那么如何取得每一帧呢？因为是通过Handle来预览的所以控件中无法取得，我们需要调用摄像头SDK的获取每一帧的接口。下面是我对三星摄像头的处理：

```c#
public override Bitmap CapturePicture()
{
    Bitmap bitmap = null;
    string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Templates) + @"\SAMSUNG_CapturePicture.bmp";
    if (File.Exists(path))
    {
        File.Delete(path);
    }
    if (SSNetSDK.XNS_DEV_SaveSnapshot(playHandle, path,0))
    {
        using (MemoryStream stream = new MemoryStream(File.ReadAllBytes(path)))
        {
            bitmap = (Bitmap)Image.FromStream(stream);
        }
        File.Delete(path);
    }
    return bitmap;
}
```

## 案例

程序也很简单，一个WinForm页面，添加一下对Aforge的引用就可以了。

### 下载与添加AForge

解压缩下载的AForge类库——AForge.NET Framework-2.2.5.zip（为了便于使用，应放于容易找到的路径下）

在Visual Studio中新建WinForm程序（Windows窗体应用程序），在新建的项目中添加Aforge.NET的引用

**`Step 1`**：解决方案资源管理器中，右键“引用”--> “添加引用”

![](CSharp调用摄像头.assets/20170202223358911)

**`Step 2`**：在“引用管理器”中点击右下角的“浏览”，找到Aforge.NET的路径，进入\Release文件夹下，选择必要的.dll文件添加到项目中（可参照前面介绍过的Aforge.NET类库的组成）

![](CSharp调用摄像头.assets/20170202223450147) 

**`Step 3`**：点击“确定”，完成类库引用的添加

![](CSharp调用摄像头.assets/20170202223457881)

### 界面设计

下图中间部分是用来实时显示摄像头开启后获取到的内容，是一个自定义控件。这里得说明一下，将AForge.Controls.dll拖拽到左侧的工具箱区域，然后就出来自定义控件了。这里顺便说明一下，我们平时自己开发的自定义控件也可以通过这种方式来给别人用。前台都准备好了之后我们来开始分析后台代码。

整个的思路是先找到电脑上的摄像头设备，然后选择我们需要操作的设备，然后在拍照或者摄像。

![](CSharp调用摄像头.assets/162321082179548.jpg)

### 代码详解

当Form加载的时候，我们监听一下其Load事件，将检测到的摄像头设备添加到后边的ComboBox中供用户选择，关键代码如下：

```c#
private void Form1_Load(object sender, EventArgs e)
{

    try
    {
        // 枚举所有视频输入设备
        videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);

        if (videoDevices.Count == 0)
            throw new ApplicationException();

        foreach (FilterInfo device in videoDevices)
        {
            tscbxCameras.Items.Add(device.Name);
        }

        tscbxCameras.SelectedIndex = 0;

    }
    catch (ApplicationException)
    {
        tscbxCameras.Items.Add("No local capture devices");
        videoDevices = null;
    }
}
```

当用户选择某一摄像头设备再点击连接的时候，我们打开摄像头，并对其进行初始化，关键代码：

```c#
//连接摄像头
private void CameraConn()
{
    VideoCaptureDevice videoSource = new VideoCaptureDevice(videoDevices[tscbxCameras.SelectedIndex].MonikerString);
    videoSource.DesiredFrameSize = new System.Drawing.Size(320, 240);
    videoSource.DesiredFrameRate = 1;

    videoSourcePlayer.VideoSource = videoSource;
    videoSourcePlayer.Start();
}
```

当用户关闭点击关闭摄像头的时候，我们做关闭的处理，代码：

```c#
//关闭摄像头
private void btnClose_Click(object sender, EventArgs e)
{
    videoSourcePlayer.SignalToStop();
    videoSourcePlayer.WaitForStop();
}
```

当用户点击拍照的时候，我们获取摄像头当前的画面，并保存到设定的路径下，然后关闭当前窗口。关键代码：

```c#
//拍照
private void Photograph_Click(object sender, EventArgs e)
{
    try
    {
        if (videoSourcePlayer.IsRunning)
        {
            BitmapSource bitmapSource = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                videoSourcePlayer.GetCurrentVideoFrame().GetHbitmap(),
                IntPtr.Zero,
                Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions());
            PngBitmapEncoder pE = new PngBitmapEncoder();
            pE.Frames.Add(BitmapFrame.Create(bitmapSource));
            string picName = GetImagePath() + "\\" + "xiaosy" + ".jpg";
            if (File.Exists(picName))
            {
                File.Delete(picName);
            }
            using (Stream stream = File.Create(picName))
            {
                pE.Save(stream);
            }
            //拍照完成后关摄像头并刷新同时关窗体
            if (videoSourcePlayer != null && videoSourcePlayer.IsRunning)
            {
                videoSourcePlayer.SignalToStop();
                videoSourcePlayer.WaitForStop();
            }

            this.Close();
        }
    }
    catch (Exception ex)
    {
        MessageBox.Show("摄像头异常：" + ex.Message);
    }
}

private string GetImagePath()
{
    string personImgPath = Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory)
        + Path.DirectorySeparatorChar.ToString() + "PersonImg";
    if (!Directory.Exists(personImgPath))
    {
        Directory.CreateDirectory(personImgPath);
    }

    return personImgPath;
}
```

 关闭之后，在bin目录下的PersonImg中即可找到保存的图片，得到图片我们就可以对它进行进一步的处理。

### 图片后续处理

获取图片后，我们将进行进一步处理，一种是调用第三方dll进行处理，这种模式我们后面将详细讲解，还有一种是调用命令行的模式来处理，这里举一个例子是使用ffmpeg将图片转换成webp格式。

> WebP（发音：weppy）是一种同时提供了有损压缩与无损压缩（可逆压缩）的图片文件格式，派生自影像编码格式VP8，被认为是WebM多媒体格式的姊妹项目，是由Google在购买On2 Technologies后发展出来，以BSD授权条款发布。
>
> WebP 的优势体现在它具有更优的图像数据压缩算法，能带来更小的图片体积，而且拥有肉眼识别无差异的图像质量；同时具备了无损和有损的压缩模式、Alpha 透明以及动画的特性，在 JPEG 和 PNG 上的转化效果都相当优秀、稳定和统一。

之前做过一个测试，对比 PNG 原图、PNG 无损压缩、PNG 转 WebP（无损）、PNG 转 WebP（有损）的压缩效果：

![img](CSharp调用摄像头.assets/9bfba760f53916e6a8a8c2458e0b1c36_1440w.jpg)

```c#
private string RunCmd(string command)
{
    //例Process
    Process p = new Process();
    p.StartInfo.FileName = "cmd.exe";         //确定程序名
    p.StartInfo.Arguments = "/c " + command;   //确定程式命令行
    p.StartInfo.UseShellExecute = false;      //Shell的使用
    p.StartInfo.RedirectStandardInput = true;  //重定向输入
    p.StartInfo.RedirectStandardOutput = true; //重定向输出
    p.StartInfo.RedirectStandardError = true;  //重定向输出错误
    p.StartInfo.CreateNoWindow = true;        //设置置不显示示窗口
    p.Start();   
    return p.StandardOutput.ReadToEnd();      //输出出流取得命令行结果果
}
// 使用ffmpeg将图片压缩成webp格式
string webpName = GetImagePath() + "\\" + "xiaosy" + ".webp";
if (File.Exists(webpName)) {
    File.Delete(webpName);
}
RunCmd("c:\\Tools\\ffmpeg\\ffmpeg -i " + picName + " " + webpName);
```

可以看到图片由400多k变成10k大小，完全看不出压缩的损失。

![](CSharp调用摄像头.assets/image-20210321171350361.png)

![](CSharp调用摄像头.assets/image-20210321171249859.png)
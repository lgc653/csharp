![](Entity Framework.assets/幻灯片0.png)

---

# Entity Framework

Entity Framework的全称是ADO.NET Entity Framework，是微软开发的基于ADO.NET的ORM(Object/Relational Mapping)框架。

1. 支持多种数据库（Microsoft SQL Server, Oracle, and DB2）；
2. 强劲的映射引擎，能很好地支持存储过程；
3. 提供Visual Studio集成工具，进行可视化操作；
4. 能够与ASP.NET, WPF, WCF, WCF Data Services进行很好的集成。

![Entity Framework](Entity Framework.assets/aa937709.EF_AtAGlance_1c(en-us,MSDN.10).png)

## 什么是Entity Framework

学习EF的前提：熟练使用Linq和ADO.NET，因为在使用EF框架进行开发时，我们大多数情况使用Linq进行查询和操作，而EF的底层实现用的是ADO.NET。

### EF的概念

在.NET3.5之前，我们经常编写ADO.NET代码或企业数据访问块来保存或检索底层数据库中的数据。做法是：**打开过一个数据库的连接，创建一个DataSet来获取或提交数据到数据库，通过将DataSet中的数据和.NET对象相互转换来满足业务需求。**这是一个麻烦且容易出错的过程**。**Microsoft提供了“Entity Framework”框架，用于自动地执行所有上述与数据库相关的活动。

EF是一个适用于.NET开发的开源ORM框架。**它使开发人员能够通过领域对象来处理数据，而无需关注存储此数据的基础数据库**。使用实体框架，开发人员在处理数据时可以在更高的抽象级别上工作，并且与传统应用程序相比，可以使用更少的代码创建和维护面向数据的应用程序。

官方定义：“实体框架是一种对象关系映射器（O/RM），它使.NET开发人员能够通过.NET对象来操作数据库。它消除了开发人员通常需要编写的大多数数据访问代码的需求。“

实体框架工作在业务实体（域类）和数据库之间。**它保存实体属性中的数据到数据库，也可以从数据库中检索数据并自动将其转换为实体对象。**

![Entity Framework的概念](Entity Framework.assets/1007918-20180911090954239-1577316072.png)

### EF功能汇总

这里简单地总结EF的特性，以后详细总结

1. `跨平台`：EF Core是一个跨平台的框架，可以在Windows，Linux和Mac上运行。
2. `建模`：EF可以创建具有不同数据类型get / set属性的EDM（Entity Data Model/实体数据模型）。它使用此模型查询或保存底层数据库的数据。
3. `查询`：EF允许我们使用LINQ从底层数据库中检索数据，同时也支持直接对数据库执行原始SQL查询。
4. `更改跟踪`：EF会跟踪需要提交到数据库的实体实例（属性值）发生的更改。
5. `保存`：EF调用SaveChanges()方法时，根据实体发生的更改，对数据库执行INSERT，UPDATE和DELETE命令。EF还提供了异步的SaveChangesAsync()方法。
6. `并发`：默认情况下，从数据是从数据库中提取开始，EF使用乐观并发来避免我们做的修改被其他用户覆盖。
7. `事务`：EF在查询或保存数据时自动执行事务管理。它还提供自定义事务管理的选项。
8. `缓存`：EF包括开箱即用的第一级缓存。因此，重复查询将从缓存中返回数据，而不是访问数据库。
9. `配置`：EF允许我们使用注释属性配置EF模型，也可以使用Fluent API来覆盖默认约定。
10. `迁移`：EF提供了一组迁移命令，我们可以在NuGet Package Manager控制台或命令行界面中执行这些命令来创建或管理底层数据库计划。

### EF组成

下图很好地说明了EF的基本组成：

![EF的基本组成](Entity Framework.assets/1007918-20180518194912201-629098295.png)

EF的组成简单总结如下：

* `EDM(实体数据模型)`：EDM包含三个主要部分——概念模型,映射和存储模型。
  * **概念模型（entity）：** 概念模型包含了模型类和它们之间的关系。 这将是独立于数据库表设计。
  * **存储模型(data)：** 存储模型是数据库设计模型,包括表、视图、存储过程、以及它们之间的关系和钥匙。
  * **映射(mapping)：** 映射由概念模型如何映射到存储模型的信息组成。
* `LINQ To Entity（L2E）`：L2E是一种的查询实体对象的语言， 它返回在概念模型中定义的实体。 
* `Entity SQL`：Entity SQL是一个类似于L2E的查询语言。 然而,它比L2E更加复杂。
* `Object Services(对象服务)`：对象服务是访问数据库中的数据并返回数据的主要入口点。它负责数据实例化，把**Entity Client Data Provider**（下一层）的数据转换成实体对象。
* `Entity Client Data Provider`：主要职责是将L2E或Entity Sql转换成数据库可以识别的Sql查询语句，它通过**ADO.NET Data Provider**向数据库发送或者索取数据。
* `ADO.NET Data Provider`：使用标准的ADO.NET与数据库通信。

## EF工作流程

### EF基本CRUD流程

下边的图就可以很清晰地展示EF的CRUD操作的基本工作流程：

![EF基本CRUD流程](Entity Framework.assets/1007918-20180911094441216-1247635897.png)

这里做一个EF CRUD操作的简单总结：

1. 定义模型：这是EF工作的前提，定义模型包括定义**领域类**（Domain Classes），派生自DbContext的**上下文类**和**Configuration**（如果有的话）。EF将通过模型来执行CRUD操作。
2. 添加数据：将领域对象添加到上下文并调用SaveChanges()方法。EF API将构建适当的INSERT命令并将其执行到数据库。
3. 读取数据：执行LINQ-to-Entities查询。EF API会将此查询转换为底层关系数据库的SQL查询并执行，然后将结果转换为实体对象并显示在UI上。
4. 编辑或删除数据：从上下文更新或删除实体对象并调用该SaveChanges()方法。EF API将构建适当的UPDATE或DELETE命令并执行。

### EF的工作过程简析

这里将展示EF工作的基本过程，EF API（EF6和EF Core）包括几个功能：

1. 将领域类映射到数据库
2. 将LINQ查询转换为SQL并执行
3. 跟踪实体在其生命周期内发生的更改，并将这些更改保存到数据库

![EF的工作过程简析](Entity Framework.assets/1007918-20180911102456070-1406855114.png)

#### 实体数据模型（Entity Data Model）

EF API的首要任务是构建实体数据模型（EDM）。**EDM是整个元数据的内存表示，包含：概念模型，存储模型以及它们之间的映射**。

![Entity Data Model](Entity Framework.assets/1007918-20180911102720310-760109019.png)

 

**概念模型**： EF通过领域类，上下文类，默认约定和配置构建概念模型。

**存储模型**： EF为底层数据库架构构建存储模型。在代码优先方法中，根据概念模型进行推断来构建。在数据库优先模式中，根据目标数据库进行推断来构建。

**映射**： EF包括的映射信息，表示概念模型是如何映射到数据库架构（存储模型）。

#### 查询

EF API使用EDM将LINQ-to-Entities查询转换为SQL查询，并将结果转换回实体对象。

![EF API查询](Entity Framework.assets/1007918-20180911102756154-2099420407.png)

#### 保存

EF API在调用SaveChanges()方法时根据实体的状态推断INSERT，UPDATE和DELETE命令。ChangeTrack会在执行操作时跟踪每个实体的状态。

![EF API保存](Entity Framework.assets/1007918-20180911102847193-2093308575.png)

 

## EF中的上下文简介

### DbContext(上下文类)

在DbFirst模式中，我们添加一个EDM（Entity Data Model）后会自动生成一个.edmx文件,这个文件中包含一个继承DbContext类的上下文实例，DbContext是实体类和数据库之间的桥梁，**DbContext主要负责与数据交互**，主要作用：

1. DbContext包含所有的实体映射到数据库表的实体集`(DbSet < TEntity >)`。
2. DbContext 将LINQ-to-Entities查询转换为SQL查询并将其发送到数据库。
3. 更改跟踪: 它跟踪每个实体从数据库中查询出来后发生的修改变化。
4. 持久化数据: 它也基于实体状态执行插入、更新和删除操作到数据库中。

　　![DbContext(上下文类)](Entity Framework.assets/1007918-20180518210452889-1674081236.png)

从上图可以看出：在EF工作过程中，对实体的操作会导致实体状态的变化，Context根据实体状态的变化生成和执行对应的SQL语句。简言之，对实体的操作给实体添加标签（如Added,Deleted,Modified），DbContext根据实体的标签生成（Insert,Delete,Update）Sql语句，然后EF通过Ado.Net执行Sql实现持久化。

### DbContext中的DbSet

DbSet 表示上下文中给定类型的所有实体的集合或可从数据库中查询的给定类型的所有实体的集合。 可以使用 DbContext.Set 方法从 DbContext 中创建 DbSet 对象,DbSet对应着数据库中的表，DbSet常用的方法如下

| Add(Entity)/AddRange   | 返回添加的Entity   | 添加实体到context中，并给实体标记Added状态 dbcontext.Students.Add(studentEntity) |
| ---------------------- | ------------------ | ------------------------------------------------------------ |
| `AsNoTracking<Entity>` | `DBQuery<Entity>`  | 获取一个不被context缓冲和追踪的序列,多用于获取只读序列 `var studentList = dbcontext.Students.AsNoTracking<Student>().ToList<Student>();` |
| Attach(Entity)         | 返回添加的Entity   | 将实体添加到context上下文中 `dbcontext.Students.Attach(studentEntity);` |
| Find(int)              | 返回对应Id的Entity | 通过主键获取实体对象，如果在数据库和context中不存在那么返回null,注：也会返回在context中存在但还没有写入数据库的实体对象。`Student studEntity = dbcontext.Students.Find(1);` |
| Include                | DBQuery            | include必须是外键连接，且立即执行；join连接不需要外键 ，延时执行 `var studentList = dbcontext.Students.Include("StudentAddress").ToList<Student>(); var studentList = dbcontext.Students.Include(s => s.StudentAddress).ToList<Student>();` |
| Remove/RemoveRange     | 返回删除的entity   | 删除实例，并给实例对象添加deleted标记。 `dbcontext.Students.Remove(studentEntity);` |
| SqlQuery               | DBSqlQuery         | 通过sql获取实例集合. 默认情况下，返回的集合被追踪的，可以使用AsNoTracking()取消追踪。 `var studentEntity = dbcontext.Students.SqlQuery("select * from student where studentid = 1").FirstOrDefault<Student>();` |

### DbContext中的DBEntityEntry

在EF中实体的五种状态：

1. **detached**：实体不在上下文的追踪范围内。如刚new的实例处于detached，可以通过Attach()添加到上下文，此时的状态是unchanged。
2. **unchanged**：未改变，如刚从数据库读出来的实例
3. **added**：添加状态 一般执行`db.Set<T>.Add(t)/ AddRange(ts)`时标记为added。因为新对象在数据库中没有相应的记录，所有不能转成deleted和modified状态。
4. **deleted**:删除状态 一般执行`db.Set<T>.Remove(t)/ RemoveRange(ts)`时标记为deleted。数据库中必须先有了相应的记录，所有deleted不能转为added状态。
5. **modified**:修改状态 改变了实体的属性处于这个状态，可以转为deleted，不能转为added状态。

当EF从数据库中提取一条记录生成一个实体对象之后，应用程序可以针对它的操作太多了，EF是怎么知道哪个对象处于哪个状态的？

EF的解决方案是：为当前所有需要跟踪的实体对象，创建一个相应的DbEntityEntry对象，此对象包容着实体对象每个属性的三个值：Current Value、Original Value和Database Value，只要比较这三个值，很容易地就知道哪个属性值被修改了(设置：`context.Configuration.AutoDetectChangesEnabled = false`则不会去追踪，默认是打开的），从而生成相应的Sql命令。对象的状态会随着操作而改变，我们也可以自己指定状态：

```csharp
//为user生成一个DbEntityEntry对象
DbEntityEntry userEntry = context.Entry(user);
userEntry.State = EntityState.Added;//添加标记
userEntry.State = EntityState.Deleted;//删除标记
userEntry.State = EntityState.Modified;//修改标记
userEntry.State = EntityState.Unchanged;//无变化标记
userEntry.State = EntityState.Detached;//不追踪标记
```

状态间的转化如下图：

![在EF中实体的状态间的转化](Entity Framework.assets/1007918-20180518212429546-2111933560.png)

当DbContext执行SaveChanges()方法时，根据实体的状态生成相应的Sql语句，通过Ado.net完成数据的持久化。

## EF中的实体关系

EF与数据库一样支持三种关系类型：**①一对一 ，②一对多，③多对多**。

下边是一个SchoolDB数据库的实体数据模型，图中包含所有的实体和各个实体间的关系。通过设计器我们很容易看出实体间的对应关系

![SchoolDB数据库的实体数据模型](Entity Framework.assets/1007918-20180911143858146-1605322226.png)

 

### 一对一

如上图，Student和StudentAddress具有一对一的关系（零或一）。一个学生只能有一个或零个地址。实体框架将Student**实体导航属性**添加到StudentAddress实体中，将StudentAddress**实体导航属性**添加到Student实体中。同时，StudentAddress类中将StudentId作为PrimaryKey和ForeignKey属性，这样Student和StudentAddress就成为一对一的关系。

```csharp
public partial class Student
{
    public Student()
    {
        this.Courses = new HashSet<Course>();
    }
    
    public int StudentID { get; set; }
    public string StudentName { get; set; }
    public Nullable<int> StandardId { get; set; }
    public byte[] RowVersion { get; set; }
    //实体导航属性
    public virtual StudentAddress StudentAddress { get; set; }
    }
    
public partial class StudentAddress
{
　　//同时是主键和外键
    public int StudentID { get; set; }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public string City { get; set; }
    public string State { get; set; }
    //实体导航属性
    public virtual Student Student { get; set; }
}
```

在上面的示例中，StudentId属性在StudentAddress类中是同时是PrimaryKey和ForeignKey。我们可以在context的OnModelCreating方法中使用Fluent API进行配置。 

### 一对多

Standard与Teacher实体具一对多的关系。这表示多个教师评同一个级别（成绩级别A,B,C,D），而每个教师只能评一个级别，如给学生1评A级的同时不能再评一个B级。

```csharp
public partial class Standard
{
    public Standard()
    {
        this.Teachers = new HashSet<Teacher>();
    }
    
    public int StandardId { get; set; }
    public string StandardName { get; set; }
    public string Description { get; set; }
    //集合导航属性
    public virtual ICollection<Teacher> Teachers { get; set; }
}

public partial class Teacher
{
    public Teacher()
    {
        this.Courses = new HashSet<Course>();
    }
    public int TeacherId { get; set; }
    public string TeacherName { get; set; }
    public Nullable<int> TeacherType { get; set; }
    //外键
    public Nullable<int> StandardId { get; set; }
    //实体导航属性
    public virtual Standard Standard { get; set; }
}
```

 Standard实体具有**集合导航属性** Teachers （请注意它是复数），这表明一个Standard可以有一个教师集合（老师1和老师2都可以给A级的评分）。而Teacher实体具有Standard**实体导航属性**，表明每个Teacher只能评一个Standard，同时Teacher中包含StandardId外键。这样Standard与Teacher就成为了一对多的关系。 

### 多对多关系

Student和Course具有多到多关系。这表示一个学生可以参加许多课程，而一个课程也可以向许多学生讲授。
数据库包括StudentCourse中间表，表中只包含Student和Course的主键。
如上图所示，Student实体包含**集合导航属性** Courses，Course实体包含**集合导航属性** Students以表示它们之间的多对多关系。这两个表都没有外键关系，它们通过StudentCourse中间表进行关联。

下面的代码片段展示了Student和Course实体类。

```csharp
public partial class Student
{
    public Student()
    {
        this.Courses = new HashSet<Course>();
    }
    
    public int StudentID { get; set; }
    public string StudentName { get; set; }
    public Nullable<int> StandardId { get; set; }
    public byte[] RowVersion { get; set; }
    //集合导航属性
    public virtual ICollection<Course> Courses { get; set; }
}
    
public partial class Course
{
    public Course()
    {
        this.Students = new HashSet<Student>();
    }
    
    public int CourseId { get; set; }
    public string CourseName { get; set; }
     //集合导航属性
    public virtual ICollection<Student> Students { get; set; }
}
```

> :zap: 注：实体框架在中间表仅有两个表的主键（只有StudentId和CourseId）时才自动维护多对多关系。当我们给Student添加一个Course或给Course添加一个Student，执行SaveChange()时，EF会在中间表自动会插入对应的StudentId和CourseId。如果中间表包含其他列，那么EDM也会为中间表创建实体，EF不再自动维护中间表，那么我们就需要手动管理多对多实体的CRUD操作。 

## EF中的持久化场景

使用EF实现实体持久化（保存）到数据库有两种情况：在线场景和离线场景。

### 在线场景

在线场景中，context是同一个上下文实例（从DbContext派生），检索和保存实体都通过同一个context上下文，因此在线场景中的持久化十分简单。

![EF中的持久化场景-在线场景](Entity Framework.assets/1007918-20180911152109586-320901456.png)

 这种方案适用于连接本地数据库或同一网络上的数据库。

> **:fire:优点：**
>
> 执行速度快。上下文跟踪所有实体，并在实体发生更改时自动设置适当的状态。
>
> **:bug:缺点：**
>
> 上下文保持在线状态，因此与数据库的连接保持打开状态。利用更多资源。

### 离线场景

离线场景中，使用不同上下文实例进行检索和保存。一个context检索实体后被释放，创建一个新的实体进行保存工作。

离线场景的保存相对复杂，因为新创建的context没有跟踪实体，因此必须在保存实体之前为每个实体设置适当的状态。在上图中，应用程序使用Context 1检索实体图，执行一些CUD（创建，更新，删除）操作。使用Context2保存时，Context2不知道在这个实体图上执行过哪些操作。

![EF中的持久化场景-离线场景](Entity Framework.assets/1007918-20180911152042748-616860933.png)

 

离线场景适用于Web应用程序或远程数据库。

> **:fire:优点：**
>
> 与在线方案相比，使用更少的资源。没有与数据库的长连接。
>
> **:bug:缺点：**
>
> 需要在保存之前为每个实体设置适当的状态。执行速度比在线方案慢。

## 在线场景中保存数据

在线场景中保存实体数据是一项相当容易的任务，因为使用的是同一个context,这个context会自动跟踪所有实体发生的更改。下图说明了在线场景中的CUD（创建，更新，删除）操作。

![在线场景中的CUD（创建，更新，删除）操作](Entity Framework.assets/1007918-20180911154838266-1933048251.png)


EF在调用context.SaveChange方法时，根据EntityState进行添加、修改或删除实体实例，并执行INSERT，UPDATE和DELETE语句。在线场景中，context跟踪所有实体的实例，EntityState无论何时创建，修改或删除实体，它都会自动为每个实体设置适当的实例。

### 插入数据

使用DbSet.Add方法将新实体添加到上下文（context），调用context.SaveChanges()方法时在数据库中插入新记录。

```csharp
using (var context = new SchoolDBEntities())
{
    var std = new Student()
    {
        FirstName = "Bill",
        LastName = "Gates"
    };
    context.Students.Add(std);

    context.SaveChanges();
}
```

在上面的示例中，context.Students.Add(std)将新创建的Student实体实例,这个新实例的EntityState 为Added。调用context.SaveChanges()方法时数据库构建并执行以下INSERT语句。 

```sql
exec sp_executesql N'INSERT [dbo].[Students]([FirstName], [LastName])
VALUES (@0, @1)
SELECT [StudentId]
FROM [dbo].[Students]
WHERE @@ROWCOUNT > 0 AND [StudentId] = scope_identity()',N
''@0 nvarchar(max) ,@1 nvarchar(max) ',@0=N'Bill',@1=N'Gates'
Go
```

### 更新数据

在线场景中，EF API会跟踪上下文中所有实体。因此，在编辑实体数据时，EF会自动标记EntityState为Modified，在调用SaveChanges()方法时在数据库中生成并执行更新的语句。

```csharp
using (var context = new SchoolDBEntities())
{
    var std = context.Students.First<Student>(); 
    std.FirstName = "Steve";
    context.SaveChanges();
}
```

我们使用从数据库中检索第一个学生:context.Students.First<student>()。一旦我们修改了FirstName，上下文就会将实例的EntityState设置为Modified。当我们调用该SaveChanges()方法时，会在数据库中构建并执行以下Update语句。 

```sql
exec sp_executesql N'UPDATE [dbo].[Students]
SET [FirstName] = @0
WHERE ([StudentId] = @1)',
N'@0 nvarchar(max) ,@1 int',@0=N'Steve',@1=2
Go
```

在更新语句中，EF API通过**主键**找到要修改的实例，修改时仅包含修改的属性，其他属性将被忽略。在上面的示例中，仅FirstName编辑了属性，因此update语句中只包含FirstName列。

### 删除数据

DbSet.Remove()方法用于删除数据库表中的记录。

```csharp
using (var context = new SchoolDBEntities())
{
    var std = context.Students.First<Student>();
    context.Students.Remove(std);
    context.SaveChanges();
}
```

context.Students.Remove(std)将std实体对象标记为Deleted。因此，EF将在数据库中构建并执行以下DELETE语句。

```sql
exec sp_executesql N'DELETE [dbo].[Students]
WHERE ([StudentId] = @0)',N'@0 int',@0=1
Go
```

通过上边的例子可以看出，在线场景中添加，更新或删除中的数据非常容易。

## EF中的查询方法

这里主要介绍两种查询方法 Linq to entity(L2E)和Sql

### L2E查询

L2E查询时可以使用linq query语法，或者lambda表达式，默认返回的类型是IQueryable,（linq查询默认返回的是IEnumerable），下边给出了一个简单的例子

```csharp
//查询名字为ls的用户集合
//query语法
var users = from u in context.UserInfo
    where u.UserName == "ls"
    select u;
//method语法
var users2 = context.UserInfo.Where<UserInfo>(user => user.UserName == "ls");
```

linq的使用方法详见[linq总结系列（一）---基础部分](http://www.cnblogs.com/wyy1234/p/9053580.html)，其中的CRUD操作都是使用linq的语法，这里就不多啰嗦了~ 

### 原生Sql查询和操作

#### DbSet.SqlQuery()

返回的结果是DbSqlQuery类型，该类型实现了IEnumberable接口，所以结果集也可以用linq操作

```csharp
//查询名字为ls,密码是123321的用户集
//使用占位符，执行的时候自动生成sql参数，不用担心sql注入
var user1 = context.UserInfo.SqlQuery("select * from userinfo where username=@p0 and userpass=@p1", "ls", "123");

//自己设sql参数
SqlParameter [] pars= {
    new SqlParameter("@name", SqlDbType.NVarChar, 20) { Value = "ls" },
    new SqlParameter("@pass", SqlDbType.NVarChar, 20) { Value="123"}
};
var user2 = context.Database.SqlQuery<UserInfo>( "select * from userinfo where username=@name and userpass=@pass", pars);

//查询用户人数
int count = context.Database.SqlQuery<int>("select count(*) from userinfo").SingleOrDefault();
```

#### Database.SqlQuery()

context.Database对应着底层数据库，Database.SqlQuery用于查询，可以返回任意类型。

```csharp
using (var ctx = new SchoolDBEntities())
{
    //获取ID为1的student的名字
    string studentName = ctx.Database.SqlQuery<string>("Select studentname from Student where studentid=@id", new SqlParameter("@id", 1))
                            .FirstOrDefault();
}
```

#### Database.ExecuteSqlCommand()

Database.ExecuteSqlCommand()用于通过Sql进行CUD操作，返回int类型（受影响的行数）

```csharp
using (var ctx = new SchoolDBEntities())
{
  //修改
    int noOfRowUpdated = ctx.Database.ExecuteSqlCommand("Update student  set studentname ='changed student by command' where studentid=@id", new SqlParameter("@id", 1));
 //添加
    int noOfRowInserted = ctx.Database.ExecuteSqlCommand("insert into student(studentname) values('New Student')");
 //删除
    int noOfRowDeleted = ctx.Database.ExecuteSqlCommand("delete from student where studentid=@id",new SqlParameter("@id", 1));
}
```

## 预先加载、延迟加载、显示加载

### 预先加载

预先加载：在对一种类型的实体进行查询时，将相关的实体作为查询的一部分一起加载。预先加载可以使用Include（）方法实现。

#### 加载一个相关实体类型

举例：使用Include（）方法从数据库中获取所有学生及成绩级别。
**导航属性实现预先加载：**

```csharp
using (var ctx = new SchoolDBEntities())
{
　　var stud1 = ctx.Students
　　　　　　　　　　.Include("Standard")
　　　　　　　　　　.Where(s => s.StudentName == "Bill")
　　　　　　　　　　.FirstOrDefault<Student>();
}
```

**lambda表达式实现预先加载：**

```csharp
using (var ctx = new SchoolDBEntities())
{
　　var stud1 = ctx.Students.Include(s => s.Standard)
　　　　　　　　　　.Where(s => s.StudentName == "Bill")
　　　　　　　　　　.FirstOrDefault<Student>();
}
```

#### 加载多个相关实体类型

举例：使用Include（）方法从数据库中获取所有学生及其成绩级别和评分老师。
**导航属性实现预先加载：**

```csharp
using (var ctx = new SchoolDBEntities())
{
    var stud1 = ctx.Students.Include("Standard.Teachers")
       　　　　　　　　 .Where(s => s.StudentName == "Bill")
　　　　　　　　　　　　 .FirstOrDefault<Student>();
}                
```

**lambda表达式实现预先加载：**

```csharp
using (var ctx = new SchoolDBEntities())
{
　　var stud1 = ctx.Students.Include(s => s.Standard.Teachers)
　　　　　　　　　　　　　　　　.Where(s => s.StudentName == "Bill")
　　　　　　　　　　　　　　　　.FirstOrDefault<Student>();
}
```

### 延迟加载

#### 概念

延迟加载顾名思义就是不立即加载，而是在我们访问的时候才加载，这和预先加载刚好相反。
一个例子：查询Student的StudentAddress

```csharp
using (var ctx = new SchoolDBEntities())
{
    //这里只加载student实体，导航属性StudentAddress没有加载
　　IList<Student> studList = ctx.Students.ToList<Student>();
　　Student std = studList[0];

　　//只加载特定student的住址(通过一个单独的sql进行实现的)
　　StudentAddress add = std.StudentAddress;
}
```

### 禁用延迟加载

我们可以禁用特定实体或者特定context的延迟加载。去掉实体导航属性的virtual,实体就不能进行延迟加载了。也可以通过context的cofiguration实现禁用该context下所有实体的延迟加载，代码如下：

```csharp
public partial class SchoolDBEntities : DbContext
{
    public SchoolDBEntities(): base("name=SchoolDBEntities")
    {    
        //SchoolDBEntities的所有实体都禁用延迟加载
　　　　this.Configuration.LazyLoadingEnabled = false;
　　}
　　//如果去掉virtual那么Students禁用延迟加载
　　public virtual DbSet<Student> Students { get; set; }
　　protected override void OnModelCreating(DbModelBuilder modelBuilder){}
}
```

### 延迟加载前提

如果要实现延迟加载，必须满足下边三个条件，缺一不可

1. context.Configuration.ProxyCreationEnabled应为true。
2. context.Configuration.LazyLoadingEnabled应为true。
3. 导航属性应定义为public virtual xxx,如果属性未定义为virtual，则Context不会进行延迟加载。

### 显式加载

#### Load方法

即使禁用了延迟加载（在EF 6中），仍然可能延迟加载相关实体，这时可以使用Load()方法显式加载相关实体。
一个例子：

```csharp
using (var context = new SchoolContext())
{
　　var student = context.Students
　　　　　　　　　　　　　　.Where(s => s.FirstName == "Bill")
　　　　　　　　　　　　　　.FirstOrDefault<Student>();

　　context.Entry(student).Reference(s => s.StudentAddress).Load(); // loads StudentAddress
　　context.Entry(student).Collection(s => s.StudentCourses).Load(); // loads Courses collection 
}
```

在上面的例子中， `context.Entry(student).Reference(s => s.StudentAddress).Load()` 会加载StudentAddress实体。**Reference()**方法用于获取指定实体导航属性的对象，Load()方法显式加载。

同样的方式， `context.Entry(student).Collection(s => s.Courses).Load() `**Collection()**加载student的集合导航属性Courses，Load()方法显示加载。

#### Query方法

有时候我们想对查询的结果在加载前进行过滤，Query()方法就可以排上用场了。
一个例子：查询名字为bill的学生的课程集合中数学课。

```csharp
using (var context = new SchoolContext())
{
　　var student = context.Students
　　　　　　　　　　　　　　.Where(s => s.FirstName == "Bill")
　　　　　　　　　　　　　　.FirstOrDefault<Student>();

　　context.Entry(student)
　　　　　　.Collection(s => s.StudentCourses)
　　　　　　.Query()//这里不加载，下面对这一步的结果过滤
　　　　　　　　.Where(sc => sc.CourseName == "Maths")
　　　　　　　　.FirstOrDefault();
}
```

在上边例子中 `Collection(s => s.StudentCourses).Query()` 不加载结果集，我们可以把Query()方法的结果看作是一个中间结果，可以对中间结果做进一步过滤。

## 离线场景附加实体图集到上下文

在离线场景中，保存一个实体要略微困难一些。当我们保存一个离线的实体图集或一个单独的离线实体时，我们需要做两件事。首先，我们要**把实体附加到新的上下文**中，让上下文了知道存在这些实体。其次，我们需要**手动设置每个实体的EntityState**，因为新的上下文不知道这些离线实体都经过了些什么操作，所以新的上下文不能自动地给实体添加EntityState。

下图说明了此过程。

![附加离线实体图集到上下文](Entity Framework.assets/1007918-20180912154404243-854782364.png)

为了将离线实体附加到上下文，并为实体图中的每个实体设置EntityState，EF提供下边几种方法：

### DbContext.Entry(entity).State=EntityState.Added/Modified/Deleted

DbContext.Entry()方法返回一个指向特定实体的DbEntityEntry对象，这个DbEntityEntry对象提供有关实体实例的各种信息，我们也可以使用DbEntityEntry对象来操作实体。最重要的是，我们可以通过DbEntityEntry对象的state属性来指定实体的EntityState如下所示：`context.Entry(entity).state = EntityState.Added/Modified/Deleted`
一个例子：

```csharp
var student = new Student() { //Root entity (无主键值)
    StudentName = "Bill",
    StandardId = 1,
    Standard = new Standard() //Child entity (有主键值)
        {
        StandardId = 1,
        StandardName = "Grade 1"
　　　　  },
 Courses = new List<Course>() {
　　new Course(){ CourseName = "Machine Language" }, //Child entity (无主键值)
　　new Course(){ CourseId = 2 } //Child entity (有主键值)
　　}
 };

using (var context = new SchoolDBEntities())
{
　　context.Entry(student).State = EntityState.Added;
　　
    //context.ChangeTracker.Entities返回context追踪的所有EntityEntry实例
　　foreach (var entity in context.ChangeTracker.Entries()){
　　　　Console.WriteLine("{0}: {1}", entity.Entity.GetType().Name, entity.State);
　　} 
}
```

输出：

```bash
Student: Added 
Standard: Added
Course: Added
Course: Added
```

在上边的例子中，Student实体图集包含了Standard和Course实体， `context.Entry(student).State = EntityState.Added; `将父实体和子实体（无论有没有主键值）的EntityState都设置成Added，所以我们要谨慎使用Entry()方法。
下表说明Entry()方法的规则

| 父实体   | 子实体                          |
| -------- | ------------------------------- |
| Added    | Added                           |
| Modified | Unchanged                       |
| Deleted  | All child entities will be null |

### DbSet.Add()

DbSet.Add()方法将整个实体图集附加到上下文中，同时**把父实体和子实体的状态都设置成Added**
一个例子：

```csharp
//离线实体图集
Student disconnectedStudent = new Student() { StudentName = "New Student" };
disconnectedStudent.StudentAddress = new StudentAddress() { Address1 = "Address", City = "City1" };

using (var context = new SchoolDBEntities())
{
    context.Students.Add(disconnectedStudent);

    // 获取EntityEntry实例用于查看实体的状态
    var studentEntry = context.Entry(disconnectedStudent);
    var addressEntry = context.Entry(disconnectedStudent.StudentAddress);

    Console.WriteLine("Student: {0}", studentEntry.State);
    Console.WriteLine("StudentAddress: {0}", addressEntry.State);
}
```

输出

```bash
Student: Added 
StudentAddress: Added
```

Dbset.Add()方法附加整个实体图集到上下文中，**所有实体的状态都是Added,执行SaveChange()方法时会执行Insert操作，在数据库添加新记录。**

### DbSet.Attach()

DbSet.Attach()方法将实体图集附加到一个新的上下文中，**每个实体的状态都是Unchanged.**
一个例子：

```csharp
//离线实体图集
Student disconnectedStudent = new Student() { StudentName = "New Student" };
disconnectedStudent.StudentAddress = new StudentAddress() { Address1 = "Address", City = "City1" };

using (var context = new SchoolDBEntities())
{
    context.Students.Attach(disconnectedStudent);

    // 获取EntityEntry实例用于查看实体的状态
    var studentEntry = context.Entry(disconnectedStudent);
    var addressEntry = context.Entry(disconnectedStudent.StudentAddress);

    Console.WriteLine("Student: {0}",studentEntry.State);
    Console.WriteLine("StudentAddress: {0}",addressEntry.State);
}
```

输出

```bash
Student: Unchanged 
StudentAddress: Unchanged
```

## 离线场景保存和删除实体/实体图集 

在离线场景中，当我们保存一个离线的实体图集或一个单独的离线实体时，我们需要做两件事。首先，我们要**把实体附加到新的上下文**中，让上下文了知道存在这些实体。其次，我们需要**手动设置每个实体的EntityState**，因为新的上下文不知道这些离线实体都经过了些什么操作，所以新的上下文不能自动地给实体添加EntityState。上一节我们清楚了附加离线图集的方法，附加离线图集时默认添加的EntityState不一定合适，所以我们就要执行第二步：给实体或实体图集添加合适的EntityState。

### 离线场景中保存实体

在离线场景中我们保存一个实体时，最核心的问题：**我们需要知道一个实体是新建的还是本来就存在的**。只有知道了这个问题的答案，我们才能给实体设置EntityState。如果实体主键值是0（主键是Int类型，默认值为0）我们认为这个实体是新建的，给它的状态标记为Added；如果主键值大于0，那么我们就认为这个实体是已经存在的，将它的状态标记为Modified。如下图所示：

 ![离线场景中保存实体](Entity Framework.assets/1007918-20180912162429585-76373735.png) 

 下边是一个例子： 

```csharp
// 新建的离线实体
var student = new Student(){ StudentName = "Bill" };

using (var context = new SchoolDBEntities())
{
    context.Entry(student).State = student.StudentId == 0? EntityState.Added : EntityState.Modified;

    context.SaveChanges();
}
```

因为Student是新建的，Id默认为0，所以Student被标记为Added，在数据库中执行：

```sql
exec sp_executesql N'INSERT [dbo].[Student]([StudentName], [StandardId])
VALUES (@0, NULL)
SELECT [StudentID] FROM [dbo].[Student]
WHERE @@ROWCOUNT > 0 AND [StudentID] = scope_identity(),@0='Bill'
```

同样的，如果一个实体的主键不是0，那么将它的状态被标记为Modified，一个例子:

```csharp
// 虽然是新建的，但是因为Id不是0，所以EF认为是已存在的
var student = new Student(){ StudentId = 1, StudentName = "Steve" };

using (var context = new SchoolDBEntities())
{
    context.Entry(student).State = student.StudentId == 0? EntityState.Added : EntityState.Modified;

    context.SaveChanges();
}
```

在数据库中执行如下代码，注意：如果数据库中没有StudentId 为1的记录时，会报异常

```sql
exec sp_executesql N'UPDATE [dbo].[Student]
SET [StudentName] = @0
WHERE @@ROWCOUNT > 0 AND [StudentID] = @1'N'@0 varchar(50),@1 int',@0='Steve',@1=1
```

### 离线场景中保存实体图集

上边部分我们学习了通过主键值来设置实体的状态，这里我们将学习怎么去保存一个实体图集。

在离线场景中保存一个实体图集是一件比较复杂的事，我们需要进行认真的设计。**离线场景中保存实体图集最需要解决的问题是给实体图集中的每一个实体添加标记适当的状态。**但是怎么去设计呢？在下图中我们可以看到新的Context根本不知道每个实体的状态。

![离线场景中保存实体图集](Entity Framework.assets/1007918-20180912170048440-1631096607.png)

我们必须在执行SaveChange()方法前确定每一个实体的状态，下边介绍通过主键设置实体图集状态的方法

#### 通过主键设置实体图集的状态

我们可以通过主键来设置实体图集中每一个实体的状态。如前边保存实体时介绍的，如果一个实体的主键是CLR数据类型的默认值（如int类型的默认值是0），那么我们认为这个实体是新建的，可以将这个实体标记为Added，在数据库执行Insert命令；如果主键值不是CLR数据类型的默认值，我们认为这个实体是已经存在了，标记为Modified，在数据库执行Update命令。

一个例子：

```csharp
var student = new Student() { //Root entity (没有主键值
        Standard = new Standard()   //Child entity (有主键值)
                    {
                        StandardId = 1,
                        StandardName = "Grade 1"
                    },
        Courses = new List<Course>() {
            new Course(){  CourseName = "Machine Language" }, //Child entity (没有主键值)
            new Course(){  CourseId = 2 } //Child entity (有主键值)
        }
    };

using (var context = new SchoolDBEntities())
{
    //给实体图集中的所有实体的状态进行标记
    context.Entry(student).State = student.StudentId == 0 ? EntityState.Added : EntityState.Modified;

    context.Entry(student.Standard).State = student.Standard.StandardId == 0 ? EntityState.Added : EntityState.Modified;

    foreach (var course in student.Courses)
        context.Entry(course).State = course.CourseId == 0 ? EntityState.Added : EntityState.Modified;
                
    context.SaveChanges();
}
```

在上边的例子中，Student实体图集包含Standard和Course实体，context通过每个实体的主键对该实体的状态进行标记。

### 离线场景删除实体/实体图集

在离线场景删除一个实体很简单，只需**通过Entry()方法把实体的状态标记为Deleted**即可，注：我们在将离线实体附加到上下文提过，当父实体的状态是Deleted时，通过Entry()方法附加实体图集时，实体图集的所有子实体都为null，所以**在执行SaveChange()进行数据库删除时，只删除父实体的记录**！

```csharp
// 待删除的实体
var student = new Student(){ StudentId = 1 };

using (var context = new SchoolDBEntities())
{
    context.Entry(student).State = System.Data.Entity.EntityState.Deleted;    
    context.SaveChanges();
}
```

在上边的例子中，Student实体的实例只有主键值，删除一个实体也只需要主键值就可以了。在数据库中执行如下代码：

```sql
delete [dbo].[Student]
where ([StudentId] = @0)',N'@0 int',@0=1
```

## EF6中的异步查询和异步保存

在.NET4.5中介绍了异步操作，异步操作在EF中也很有用，在EF6中我们可以使用DbContext的实例进行异步查询和异步保存。

### 异步查询

下边是一个通过L2E语法实现异步查询的例子：

```csharp
private static async Task<Student> GetStudent()
{
    Student student = null;

    using (var context = new SchoolDBEntities())
    {
        Console.WriteLine("Start GetStudent...");
         //注意await和FirstOrDefaultAsync
        student = await (context.Students.Where(s => s.StudentID == 1).FirstOrDefaultAsync<Student>());
            
        Console.WriteLine("Finished GetStudent...");
    }
    return student;
}
```

上边的例子中，GetStudent()方法使用async关键字修饰后就表示它是一个异步方法，异步方法的返回类型必须是Task<T>类型，因为GetStudent()方法要返回一个Student实体，所以返回的类型是Task<Student>。

同样的，Linq表达式使用await关键字修饰，await表示让当前线程去执行其他代码，直到linq表达式执行结束并返回结果。我们使用FirstOrDefaultAsync异步扩展方法来获取结果，我们也可以使用其他的异步扩展方法如SingleOrDefautAsync,ToListAsync等。

### 异步保存

EF API提供了SaveChangesAsync()方法来异步地把数据保存到数据库，下边例子中的SaveStudent方法异步的将Student实体保存到数据库。

```csharp
private static async Task SaveStudent(Student editedStudent)
{
    using (var context = new SchoolDBEntities())
    {
        context.Entry(editedStudent).State = EntityState.Modified;
                
        Console.WriteLine("Start SaveStudent...");
                
        int x = await (context.SaveChangesAsync());
                
        Console.WriteLine("Finished SaveStudent...");
    }
}
```

### 一个查询，获取结果，保存的例子

```csharp
public static void AsyncQueryAndSave()
{
    var query = GetStudent();
            
    Console.WriteLine("Do something else here till we get the query result..");

    query.Wait();

    var student = query.Result;
    
    student.FirstName = "Steve";
    //上边的SaveStudent方法        
    var numOfSavedStudent = SaveStudent(student);
            
    Console.WriteLine("Do something else here till we save a student.." );

    studentSave.Wait();

    Console.WriteLine("Saved Entities: {0}", numOfSavedStudent.Result);
}
```

 执行的结果如下所示： 

```bash
Start GetStudent... 
Do something else here till we get the query result..
Finished GetStudent...
Start SaveStudent...
Do something else here till we save a student..
Finished SaveStudent...
Saved Entities: 1
```

在上边的例子中。首先调用**GetStudent()**方法时，把任务存储在query变量中，执行到**await**表达式的时候，当前线程被释放，去执行**调用方法**（AsyncQueryAndSave方法）中的代码，执行到**query.wait()**时，停止线程执行直到GetStudent()彻底执行完成。一旦GetStudent()执行完成，我们可以通过**query.Result**获取查询到的Student实例。SaveStudent()也是一样的执行过程。 

## EF进行批量添加/删除

EF6添加了批量添加/删除实体集合的方法，我们可以使用DbSet.AddRange()方法将实体集合添加到上下文，同时实体集合中的每一个实体的状态都标记为Added，在执行SaveChange()方法时为每个实体执行Insert操作；同样的我们使用DbSet.RemoveRange()方法将集合中的所有实体都标记为deleted状态，在执行SaveChange()方法时为每一条数据执行delete操作。

通过AddRange()和RemoveRange()方法可以有效提升性能，所以建议在进行批量数据的添加/删除时采用这两种方法。

**一个批量添加的例子：**

```csharp
IList<Student> newStudents = new List<Student>() {
                                    new Student() { StudentName = "Steve" };
                                    new Student() { StudentName = "Bill" };
                                    new Student() { StudentName = "James" };
                                };               

using (var context = new SchoolDBEntities())
{
    context.Students.AddRange(newStudents);
    context.SaveChanges();
}
```

 **一个批量删除的例子：** 

```csharp
IList<Student> studentsToRemove = new List<Student>() {
                                    new Student() { StudentId = 1, StudentName = "Steve" };
                                    new Student() { StudentId = 2, StudentName = "Bill" };
                                    new Student() { StudentId = 3, StudentName = "James" };
                                };
    
using (var context = new SchoolDBEntities())
{
    context.Students.RemoveRange(studentsToRemove);
    context.SaveChanges();
}
```

> :zap:特别注意:EF Core的AddRange()和RemoveRange()只访问一次数据库，所以性能比EF6更高。

## EF中的高并发

这里只介绍EF6中database-first开发方案的高并发解决方案。

**EF默认支持乐观并发**：我们从数据库加载了一条数据，这是有人修改了这条数据，而我们手中用的还是旧数据，这就出现了脏读，这个时候我们修改了这条数据然后执行SaveChange()会发生什么呢？EF在保存数据时会首先查看数据库中的数据有没有改变过，数据没有改变就执行保存；数据改变了会抛出异常，我们再次提交前必须解决冲突（提到解决冲突是不是想到了git提交中的冲突？EF中解决高并发的方法和git提交的方法采用的思想是一样的，往下看就知道了）。

### 使用步骤

#### 添加RowVersion列

在EF中database-first开发模式中，为了解决高并发问题我们可以为数据表填加一个**timestamp类型**的的列，列名为rowversion,rowversion是一个二进制的数据，在每次的添加/修改操作后rowversion的值都会变大

以Student实体为例，给Student表添加一个Rowversion列，类型为timestamp，如下所示：

![添加RowVersion列](Entity Framework.assets/1007918-20180913095903091-1991972380.png)

#### 生成/升级EMD

如果没有EDM通过数据库生成新的实体数据模型，如果有EDM则右击设计器->Update Model From Database ->Refresh Student table，这时我们就可以在设计器中看到RowVersion属性了，**RowVersion属性的Concurrency Mode设置为Fixed**，如下图

![生成/升级EMD](Entity Framework.assets/1007918-20180913100736888-301785478.png)

做完这两步，EF API在执行Update时，会把RowVersion添加到where子句中（就像这样：`update tb set cloName=xxx where Id=@id and RowVersion=@rowversion`），如果where子句中的RowVersion值和数据库中的不一样就抛出DbUpdateConcurrencyException。

### 一个例子

```csharp
Student student = null; 

using (var context = new SchoolDBEntities())
{
    student = context.Students.First();
}

//修改学生名字
Console.Write("Enter New Student Name:");
student.StudentName = Console.ReadLine(); //Assigns student name from console

using (var context = new SchoolDBEntities())
{
    try
    {
        context.Entry(student).State = EntityState.Modified;
        context.SaveChanges();

        Console.WriteLine("修改成功！");
    }
    catch (DbUpdateConcurrencyException ex)
    {
        Console.WriteLine("发生高并发异常！");
    }
}
```

假设有两个用户都在执行上边的代码，User1和User2拿到了同一个Student实例，User1打字快1秒就把这个Student的用户修改了，并在数据库保存成功（User1执行Update时RowVersion和数据库一致，所以不报错，保存完成后Student的RowVersion自动改变了），这时User2也完成了修改，在User2执行保存到数据库时（生成的Update语句中的RowVersion和数据库中不一致了，所以抛出异常）。 

## EF6中DbFirst模式下使用存储过程

我们已经知道EF可以将L2E或Entity SQL的查询语句自动转换成SQL命令，也可以根据实体的状态自动生成Insert/update/delete的Sql命令。这节介绍EF中使用预先定义的存储过程对一张或者多种表进行CURD操作。

EF API会新建一个function来映射数据库中的自定义函数或存储过程。下边讲解EF DbFirst模式下存储过程的用法，EF CodeFirst存储过程的用法会在以后的EF CodeFirst系列中介绍。

### DbFirst模式——使用存储过程查询数据

#### 第一步 在数据库新建存储过程

首先在数据库创建一个名为GetCoursesByStudentId的存储过程，这个存储过程返回一个学生的所有课程。

```sql
CREATE PROCEDURE [dbo].[GetCoursesByStudentId]
    -- Add the parameters for the stored procedure here
    @StudentId int = null
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

    -- Insert statements for procedure here
select c.courseid, c.coursename,c.Location, c.TeacherId
from student s 
left outer join studentcourse sc on sc.studentid = s.studentid 
left outer join course c on c.courseid = sc.courseid
where s.studentid = @StudentId
END
```

#### 第二步 生成EMD

这一步和普通生成EMD过程是一样的，值得注意的是在Choose Your Database Objects and Settings这一步勾选我们新建的存储过程（GetCoursesByStudentId）和下边的**Import selected stored procedures and functions into the entity model ,**如下图所示

 ![生成EMD](Entity Framework.assets/1007918-20180913120308548-1629997810.png) 

GetCoursesByStudentId()返回Course实体集合，所以我们不需要返回一个新的复杂类型。右键Function Imports下的**GetCoursesByStudentId**方法，选择**Edit**，出现以下界面，把**Entities**设置为Course即可

![生成EMD](Entity Framework.assets/1007918-20180913133700675-1194972026.png)

通过上边两步，在context中会生成一个GetCoursesByStudentId方法：

![img](Entity Framework.assets/1007918-20180913134050880-1288774760.png)

现在我们就可以愉快地使用context.GetCoursesByStudentId来执行存储过程了！如下：

```csharp
using (var context = new SchoolDBEntities())
{
    var courses = context.GetCoursesByStudentId(1);

    foreach (Course cs in courses)
        Console.WriteLine(cs.CourseName);
}
```

在数据库中执行的命令为： `exec [dbo].[GetCoursesByStudentId] @StudentId=1` 

一点补充：EF中的表值函数和查询的存储过程使用的步骤是一模一样的（表值函数和存储过程本来就很类似，一个最重要的区别是EF中表值函数的返回值可用linq查询过滤）

一点补充：EF中的表值函数和查询的存储过程使用的步骤是一模一样的（表值函数和存储过程本来就很类似，一个最重要的区别是EF中表值函数的返回值可用linq查询过滤）

### DbFirst模式——使用存储过程执行CUD操作

这一部分介绍怎么通过存储过程来执行CUD(creat,update,delete)操作。

我们在数据库添加以下几个存储：

**Sp_InsertStudentInfo（添加）:**

```sql
CREATE PROCEDURE [dbo].[sp_InsertStudentInfo]
    -- Add the parameters for the stored procedure here
    @StandardId int = null,
    @StudentName varchar(50)
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

        INSERT INTO [SchoolDB].[dbo].[Student]([StudentName],[StandardId])
        VALUES(@StudentName, @StandardId)

    SELECT SCOPE_IDENTITY() AS StudentId

END
```

 **sp_UpdateStudent(修改):** 

```sql
CREATE PROCEDURE [dbo].[sp_UpdateStudent]
    -- Add the parameters for the stored procedure here
    @StudentId int,
    @StandardId int = null,
    @StudentName varchar(50)
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

    Update [SchoolDB].[dbo].[Student] 
    set StudentName = @StudentName,StandardId = @StandardId
    where StudentID = @StudentId;

END
```

 **sp_DeleteStudent(删除):** 

```sql
CREATE PROCEDURE [dbo].[sp_DeleteStudent]
    -- Add the parameters for the stored procedure here
    @StudentId int
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

    DELETE FROM [dbo].[Student]
    where StudentID = @StudentId

END
```

首先我们要升级EDM把这些存储过程添加当我们的EDM中。右键设计器，选择**Update Model from Database**,就出现了升级界面，如下图所示，展开**Stored Procedure and Functions**,勾选我们上边创建的三个存储过程，然后点击**Finish。**

![使用存储过程执行CUD操作](Entity Framework.assets/1007918-20180913141003767-896851291.png)**

这时模型浏览器中在Store Model中有了这三个存储过程，但是**Function Imports**中没有，如下所示：

![使用存储过程执行CUD操作](Entity Framework.assets/1007918-20180913141209761-94054108.png)

打开模型设计器在**Student**实体上右键，选择Stored Procedure Mapping，来打开映射详情界面，如下图

![使用存储过程执行CUD操作](Entity Framework.assets/1007918-20180913141549243-496559202.png)

在下面的映射详细界面，给这些下拉菜单分别选择对应的存储过程，同时选择存储过程的输入输出参数，如下图所示：

![使用存储过程执行CUD操作](Entity Framework.assets/1007918-20180913142110226-1030782594.png)

右击Student实体，选择**Validate**,确保没有错误

![使用存储过程执行CUD操作](Entity Framework.assets/1007918-20180913142510944-551063577-1573967342821.png) 

现在我们执行add,update,或者delete时，EF不再执行自动生成的SQL命令，而是通过这些存储过程来执行，下面是一个例子：

```csharp
using (var context = new SchoolDBEntities())
{
    Student student = new Student() { StudentName = "New student using SP"};

    context.Students.Add(student);
    //执行 sp_InsertStudentInfo 
    context.SaveChanges();

    student.StudentName = "Edit student using SP";
    //执行 sp_UpdateStudent
    context.SaveChanges();

    context.Students.Remove(student);
    //执行 sp_DeleteStudentInfo 
    context.SaveChanges();
}
```

 调用SaveChange()方法时，在数据库中执行的命令如下： 

```sql
exec [dbo].[sp_InsertStudentInfo] @StandardId=NULL,@StudentName='New student using SP'
go

exec [dbo].[sp_UpdateStudent] @StudentId=47,@StandardId=NULL,@StudentName='Edit student using SP'
go

exec [dbo].[sp_DeleteStudent] @StudentId=47
go
```

注意：**无论是使用存储过程还是默认生成的Sql命令，当插入一条记录并执行完SaveChange()方法后，这个新的实例会立即分配一个Id**。这样设计是为了有效地追踪实体，让我们可以对实体执行进一步的操作(如没有id执行update会抛出异常)，如下图： 

![使用存储过程执行CUD操作](Entity Framework.assets/1007918-20180913143759196-1989310848.png) 

## DbContext追踪实体状态改变

这一节介绍DbContext追踪实体的变化。EF支持DbContext在其生命周期中自动追踪加载的实体。我们可以通过DbChangeTracker类获取DbContext追踪的所有实体的变化。

注意**每个实体必须有主键值才能被上下文追踪**。概念模型中的实例如果没有主键值，DbContext不会对其执行追踪。

下边的代码显示了DbContext追踪实体状态变化过程

```csharp
static void Main(string[] args)
{
    using (var ctx = new SchoolDBEntities())
    {

        Console.WriteLine("Find Student");
        var std1 = ctx.Students.Find(1);
　　　　　//ctx.ChangeTracker.Enties()方法获取所有追踪实体的DbEntityEntry对象
        Console.WriteLine("Context tracking changes of {0} entity.", ctx.ChangeTracker.Entries().Count());

        DisplayTrackedEntities(ctx.ChangeTracker);

        Console.WriteLine("Find Standard");

        var standard = ctx.Standards.Find(1);

        Console.WriteLine("Context tracking changes of {0} entities.", ctx.ChangeTracker.Entries().Count());
        Console.WriteLine("");
        Console.WriteLine("Editing Standard");
                
        standard.StandardName = "Edited name";
        DisplayTrackedEntities(ctx.ChangeTracker);


        Teacher tchr = new Teacher() { TeacherName = "new teacher" };
        Console.WriteLine("Adding New Teacher");

        ctx.Teachers.Add(tchr);
        Console.WriteLine("");
        Console.WriteLine("Context tracking changes of {0} entities.", ctx.ChangeTracker.Entries().Count());
        DisplayTrackedEntities(ctx.ChangeTracker);

        Console.WriteLine("Remove Student");
        Console.WriteLine("");

        ctx.Students.Remove(std1);
        DisplayTrackedEntities(ctx.ChangeTracker);
    }
}

private static void DisplayTrackedEntities(DbChangeTracker changeTracker)
{
    Console.WriteLine("");

    var entries = changeTracker.Entries();
    foreach (var entry in entries)
    {
        Console.WriteLine("Entity Name: {0}", entry.Entity.GetType().FullName);
        Console.WriteLine("Status: {0}", entry.State);
    }
    Console.WriteLine("");
    Console.WriteLine("");
}
```

输出为： 

```bash
Find Student 
Context tracking changes of 1 entity.

Entity Name: EFTutorials.Student
Status: Unchanged

---------------------------------------
Find Standard
Context tracking changes of 2 entities.

Editing Standard

Entity Name: EFTutorials.Standard
Status: Modified
Entity Name: EFTutorials.Student
Status: Unchanged

---------------------------------------
Adding New Teacher

Context tracking changes of 3 entities.

Entity Name: EFTutorials.Teacher
Status: Added
Entity Name: EFTutorials.Standard
Status: Modified
Entity Name: EFTutorials.Student
Status: Unchanged

---------------------------------------
Remove Student

Entity Name: EFTutorials.Teacher
Status: Added
Entity Name: EFTutorials.Standard
Status: Modified
Entity Name: EFTutorials.Student
Status: Deleted

---------------------------------------
```

通过上边的例子可以：无论我们进行是哪种操作（获取，添加，修改，删除），DbContext（在上下文的生命周期内）都会持续追踪实体的状态的改变。 

## Enum

现在Enum支持的数据类型有：Int16,int32,int63,byte,sbyte。

EF中Enum的使用有两种：

① 通过EDM设计器将实体中的某一属性转换为枚举类型

② 使用已存在的枚举

### 通过EDM设计器将实体中的某一属性转换为枚举类型

一个例子：

我们将把Teacher表的TeacherType列转换为枚举类型。在TeacherType中1表示permanent teachers类型，2表示contractor teachers类型，3表示guest teachers类型。

![通过EDM设计器将实体中的某一属性转换为枚举类型](Entity Framework.assets/1007918-20180913154840283-1452930416.png)

**实现方法**：在Teacher的TeacherType属性上点击右键，选择**Convert to Enum**,如下图

![通过EDM设计器将实体中的某一属性转换为枚举类型](Entity Framework.assets/1007918-20180913155101162-1194654013.png)

这时会弹出**Add Enum Type**会话框，将**Underlying Type**设置为int32(和数据库中类型对应)，添加枚举成员如下图所示

![通过EDM设计器将实体中的某一属性转换为枚举类型](Entity Framework.assets/1007918-20180913155337918-1569141139.png)

点击Ok后打开模型浏览器发现**在Enum Type**文件夹下多了TeacherType，**Teacher模型中的TeacherType**也变成了TeacherType。

 ![通过EDM设计器将实体中的某一属性转换为枚举类型](Entity Framework.assets/1007918-20180913155504989-339191685.png)          ![通过EDM设计器将实体中的某一属性转换为枚举类型](Entity Framework.assets/1007918-20180913155609788-659451707.png)

这时我们就可以在代码中使用枚举类型了：

```csharp
using (var ctx = new SchoolDBEntities())
{
    Teacher tchr = new Teacher();
    tchr.TeacherName = "New Teacher";

    //指定枚举类型
    tchr.TeacherType = TeacherType.Permanent;

    ctx.Teachers.Add(tchr);
    ctx.SaveChanges();
}
```

### 使用已存在的枚举

如果我们已经有了一个枚举类型，我们可以把任意一个实体的属性的数据类型指定为这个枚举类型。

实现方法：在模型设计器中**右键->Add New->Enum Type->填入枚举的名字**(不用添加成员，因为我们在代码中已经有了枚举的成员)，选择 **Refernce external type** ，填入**枚举的命名空间**，点击Ok就可以了。这时模型浏览器的Enum Type文件夹中就有了我们添加的枚举。我们可以把这个枚举指定给任意实体的任意属性。

## 记录和拦截数据库命令

介绍EF6怎么记录和拦截发送给数据库的查询和操作命令。

### 记录EF发送给数据库命令（DbContext.Database.Log）

以前给了查看EF发送给数据库的命令我们需要借助数据库的追踪工具或者第三方追踪工具，现在EF6中提供了DbContext.Database.Log属性（Action<string>类型），使用这个属性我们可以很方便地记录EF发送给数据库的命令。

下边是一个例子：

```csharp
static void Main(string[] args)
{
    using (EFDbContext context=new EFDbContext())
    {
        context.Database.Log = Console.WriteLine;
        var std1 = context.Students.Find(1);
        std1.Name = "newName";
        context.SaveChanges();
        Console.ReadKey();
    }

}
```

输出如下：

![记录EF发送给数据库命令](Entity Framework.assets/1007918-20180913175727161-885876338.png)

在上边的例子中，Console.Write()方法属于`Action<string>`类型，所以可以赋值给Log属性。可以看到EF打开和关闭数据库，执行查询，和使用事务进行CUD都会被记录下来。

我们也可以自定义一个`Action<string>`委托的实例赋值给Log属性：

```csharp
public class Logger
{
    public static void Log(string message)
    {
        Console.WriteLine("EF Message: {0} ", message);
    }
}

class EF6Demo
{
    public static void DBCommandLogging()
    {
        using (var context = new SchoolDBEntities())
        {
                
            context.Database.Log =  Logger.Log;                
            var std1 = context.Students.Find(1);
            std1.Name = "newName";
            context.SaveChanges();
            Console.ReadKey();
        }
    }
}
```

### 拦截EF生成的数据库命令(IDbCommandIntercepter)

EF6提供了拦截数据库的接口**IDbCommandIntercepter**,这个接口提供了**拦截EF发送给数据库的命令**的方法，我们也可以使用这个接口**实现在context的操作执行前或执行后去做一些自定义的操作**（类似mvc/api中的filter）。因为DbContext执行操作的底层实现是利用ADO.NET进行`ExecuteNonQuery`, `ExecuteScalar`, 和`ExecuteReader，所以我们可以通过如NonQueryExecuted、NonQueryExecuting等方法进行拦截。`

为了实现SQL命令拦截我们首先要定义一个实现**IDbCommandIntercepter**接口的类：

```csharp
class EFCommandInterceptor : IDbCommandInterceptor
{
    public void NonQueryExecuted(System.Data.Common.DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
    {
        LogInfo("NonQueryExecuted", String.Format(" IsAsync: {0}, Command Text: {1}", interceptionContext.IsAsync, command.CommandText));
    }

    public void NonQueryExecuting(System.Data.Common.DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
    {
        LogInfo("NonQueryExecuting", String.Format(" IsAsync: {0}, Command Text: {1}", interceptionContext.IsAsync, command.CommandText));
    }

    public void ReaderExecuted(System.Data.Common.DbCommand command, DbCommandInterceptionContext<System.Data.Common.DbDataReader> interceptionContext)
    {
        LogInfo("ReaderExecuted", String.Format(" IsAsync: {0}, Command Text: {1}", interceptionContext.IsAsync, command.CommandText));
    }

    public void ReaderExecuting(System.Data.Common.DbCommand command, DbCommandInterceptionContext<System.Data.Common.DbDataReader> interceptionContext)
    {
        LogInfo("ReaderExecuting", String.Format(" IsAsync: {0}, Command Text: {1}", interceptionContext.IsAsync, command.CommandText));
    }

    public void ScalarExecuted(System.Data.Common.DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
    {
        LogInfo("ScalarExecuted", String.Format(" IsAsync: {0}, Command Text: {1}", interceptionContext.IsAsync, command.CommandText));
    }

    public void ScalarExecuting(System.Data.Common.DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
    {
        LogInfo("ScalarExecuting", String.Format(" IsAsync: {0}, Command Text: {1}", interceptionContext.IsAsync, command.CommandText));
    }

    private void LogInfo(string command, string commandText)
    {
        Console.WriteLine("Intercepted on: {0} :- {1} ", command, commandText);
    }
}
```

可以看出，`IDbCommandInterceptor` 接口提供了6个方法，分别用于在ADO.NET执行ExecuteNonQuery(),ExcuteReader(),ExcuteScalar()方法的执行前/后拦截命令。这个例子目的是：记录context的操作是否是异步和发送到数据库的命令。我们也可以使用这些方法来实现自定义逻辑（和filter简直一模一样有木有）。

接下来把拦截器添加到配置中去，两种实现方式：

① 通过配置文件，在app.config或web.config中添加如下节点

```xml
<entityFramework>
    <interceptors>
        <interceptor type="EF6DBFirstTutorials.EFCommandInterceptor, EF6DBFirstTutorials">
        </interceptor>
    </interceptors>
</entityFramework>
```

 ②代码配置 

```csharp
public class FE6CodeConfig : DbConfiguration
{
    public FE6CodeConfig()
    {
        this.AddInterceptor(new EFCommandInterceptor());
    }
}
```

 配置完成我们就可以记录EF发送给数据库的命令了,一个例子： 

```csharp
var newStudent =  new Student() { FirstName = "Bill" };

using (var context = new SchoolDBEntities())
{
    context.Students.Add(newStudent);
    context.SaveChanges();
}
```

 例子输出为： 

```sql
Intercepted on: ReaderExecuting :- IsAsync: False, Command Text: INSERT [dbo].[Student]([FirstName], [StandardId], [LastName])
VALUES (@0, NULL, NULL)
SELECT [StudentID], [RowVersion] FROM [dbo].[Student]
WHERE @@ROWCOUNT > 0 AND [StudentID] = scope_identity()
Intercepted on: ReaderExecuted :- IsAsync: False, Command Text: INSERT [dbo].[Student]([FirstName], [StandardId], [LastName])
VALUES (@0, NULL, NULL)
SELECT [StudentID], [RowVersion] FROM [dbo].[Student]
WHERE @@ROWCOUNT > 0 AND [StudentID] = scope_identity()
```

## EF6中基于代码进行配置方式

我们以前对EF进行配置时是在app.config/web.config下的`<entityframework>`节点下进行配置的，EF6引进了基于代码的配置方法。我们可以根据喜好来选择使用哪种配置方法，当同时使用代码和配置文件进行配置时，最终使用的是配置文件中的配置（配置文件比代码配置的优先级更高）。

### 代码配置步骤

下边看一个代码配置的例子，首先我们要新建一个集成**DbConfiguration**的类

```csharp
public class FE6CodeConfig : DbConfiguration
{
    public FE6CodeConfig()
    {
        //define configuration here
    }
}
```

使用配置有两种方法：

①修改app.config文件设置codeConfigurationType：

```xml
<entityFramework codeConfigurationType="EF6DBFirstTutorials.FE6CodeConfig, EF6DBFirstTutorials"></entityFramework>
```

通过DbConfigurationType属性标头来配置：

![通过DbConfigurationType属性标头来配置](Entity Framework.assets/1007918-20180914094147850-1201129751.png)

注意：如果我们使用MySql数据库的话，属性标头设置为 **[DbConfigurationType(typeof(MySqlEFConfiguration))]** 

### 几个常用的配置项

```csharp
public class FE6CodeConfig : DbConfiguration
{
    public FE6CodeConfig()
    {
        //设置默认的数据库连接池
        this.SetDefaultConnectionFactory(new System.Data.Entity.Infrastructure.SqlConnectionFactory());
       
        //设置数据库提供者
        this.SetProviderServices("System.Data.SqlClient", System.Data.Entity.SqlServer.SqlProviderServices.Instance);

        //设置数据库初始化器（仅支持codeFirst模式）
        this.SetDatabaseInitializer<SchoolDBEntities>(new CustomDBInitializer<SchoolDBEntities>());
    }
}
```

 对应的配置文件： 

```xml
<entityFramework>
    <defaultConnectionFactory type="System.Data.Entity.Infrastructure.SqlConnectionFactory, EntityFramework" />
    <providers>
        <provider invariantName="System.Data.SqlClient" type="System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer" />
    </providers>
 　　<contexts>
        <context type="EF6DBFirstTutorials.SchoolDBEntities, EF6DBFirstTutorials"  >
            <databaseInitializer type="EF6DBFirstTutorials.CustomDBInitializer , EF6DBFirstTutorials">
            </databaseInitializer>
        </context>
    </contexts>    
</entityFramework>
```

## EF中使用事务

这节介绍EF6中事务的使用。EF core中事务的使用方式和EF6中一模一样。

### EF中的默认的事务

默认情况下，当我们执行一个SaveChanges()方法时就会新建了一个事务，然后将context中的CUD操作都在这个事务中进行。Context中有多个SaveChanges()时，每一个SaveChanges()都会执行一个单独的事务。一个例子：

```csharp
using (var context = new SchoolContext())
{
    context.Database.Log = Console.Write;

    var standard = context.Standards.Add(new Standard() { StandardName = "1st Grade" });

    context.Students.Add(new Student()
    {
        FirstName = "Rama",
        StandardId = standard.StandardId
    });

    context.SaveChanges();

    context.Courses.Add(new Course() { CourseName = "Computer Science" });
    context.SaveChanges();
}
```

 上边的代码执行结果如下： 

 ![EF中的默认的事务](Entity Framework.assets/1007918-20180914112649051-665319446.png) 

从上边的例子我们可以清楚地看到每个SaveChanges()方法都开启了一个事务。这时有一个问题：有没有什么办法让多个SaveChanges()在一个事务中执行呢？这样的话就可以减少事务创建、开启进而提升性能了。

### 一个事务执行多个SaveChanges()方法

EF6和EF core中提供了两种方法实现在一个事务中执行多个SaveChanges()方法。

1. **DbContext.Database.BeginTrasaction()**:新建一个事务，在新建的事务内进行context.SaveChanges()
2. **DbContext.Database.UseTransaction(trans)**:使用一个context作用域外的现有的事务，多个context都可以在通过这个事务一起提交。

#### **DbContext.Database.BeginTrasaction()**

一个例子：

```csharp
using (var context = new SchoolContext())
{
    context.Database.Log = Console.Write;

    using (DbContextTransaction transaction = context.Database.BeginTransaction())
    {
        try
        {
            var standard = context.Standards.Add(new Standard() { StandardName = "1st Grade" });

            context.Students.Add(new Student()
            {
                FirstName = "Rama",
                StandardId = standard.StandardId
            });
            context.SaveChanges();
            // 第一个SaveChanges()方法后抛出异常
            throw new Exception();

            context.Courses.Add(new Course() { CourseName = "Computer Science" });
            context.SaveChanges();

            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            Console.WriteLine("Error occurred.");
        }
    }
}
```

执行结果：

![SaveChanges()都被回滚了](Entity Framework.assets/1007918-20180914115707254-698595016.png)

我们可以看到所有的SaveChanges()都被回滚了。如果把抛出异常的代码注释掉那么执行效果如下：

![把抛出异常的代码注释掉](Entity Framework.assets/1007918-20180914115834713-1559135070.png)

#### DbContext.Database.UseTransaction(trans)

使用**DbContext.Database.UseTransaction(trans)**，我们通过参数传入的是一个作用域外的事务，**注意**：这里EF不会再新建内置的事务，而是使用通过参数传入的事务。

一个例子：

```csharp
private static void Main(string[] args)
{
    string providerName = "System.Data.SqlClient";
    string serverName = ".";
    string databaseName = "SchoolDB";

    // 目标数据库
    SqlConnectionStringBuilder sqlBuilder =new SqlConnectionStringBuilder();
    sqlBuilder.DataSource = serverName;
    sqlBuilder.InitialCatalog = databaseName;
    sqlBuilder.IntegratedSecurity = true;

    using (SqlConnection con = new SqlConnection(sqlBuilder.ToString()))
    {
        con.Open();
　　　　 //在DbContext作用域外新建一个事务
        using (SqlTransaction transaction = con.BeginTransaction())
        {
            try
            {
　　　　　　　　　//使用上边的创建的事务
                using (SchoolContext context = new SchoolContext(con, false))
                {
                    context.Database.UseTransaction(transaction);

                    context.Students.Add(new Student() { Name = "Ravi" });
                    context.SaveChanges();
                }

                using (SchoolContext context = new SchoolContext(con, false))
                {
                    context.Database.UseTransaction(transaction);

                    context.Grades.Add(new Standard() { GradeName = "Grade 1", Section = "A" });
                    context.SaveChanges();
                }
                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();

                Console.WriteLine(ex.InnerException);
            }
        }
    }
}
```

## 官方案例

 https://docs.microsoft.com/zh-cn/ef/core/get-started/?tabs=netcore-cli 

### 初始化项目

```sh
dotnet new console -o EFGetStarted
cd EFGetStarted
dotnet add package Microsoft.EntityFrameworkCore.Sqlite
```
### 创建模型


```csharp
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace EFGetStarted
{
    public class BloggingContext : DbContext
    {
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite("Data Source=./blogging.db");
    }

    public class Blog
    {
        public int BlogId { get; set; }
        public string Url { get; set; }

        public List<Post> Posts { get; } = new List<Post>();
    }

    public class Post
    {
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }

        public int BlogId { get; set; }
        public Blog Blog { get; set; }
    }
}
```

> 注意官方文档中Data Source不正确，需设置成`Data Source=./blogging.db`

### 安装dotnet-ef 

> 注意，最新的dotnet-ef 3.0.1 与 netcoreapp3.0 不兼容，请使用3.0.0版本

```sh
# 注意追加了 --version 3.0.0
dotnet tool install --global dotnet-ef --version 3.0.0
dotnet add package Microsoft.EntityFrameworkCore.Design
dotnet add package Microsoft.EntityFrameworkCore.Tools
dotnet ef migrations add InitialCreate
dotnet ef database update
# 以后可以用dotnet tool list命令查询安装的工具
dotnet tool list -g
```

| 迁移命令描述                           | CMD命令                                 | PMC命令                       |
| -------------------------------------- | --------------------------------------- | ----------------------------- |
| 创建迁移：migrationname为迁移名称      | dotnet ef migrations add migrationname  | add-migration migrationname   |
| 移除迁移(删除最近的一次迁移)           | dotnet ef migrations remove             | remove-migration              |
| 应用最新的迁移(使迁移文件应用到数据库) | dotnet ef database update               | update-database               |
| 应用指定的迁移                         | dotnet ef database update migrationname | update-database migrationname |
| 查看迁移列表                           | dotnet ef migrations list               |                               |
| 查看数据库上下文信息                   | dotnet ef dbcontext info                |                               |

### Sqlite

这时你会发现项目根目录有了blogging.db文件

```sh
# 链接blogging.db
sqlite3 blogging.db
.help
# 查看表结构
.schema
```

### 运行程序

```sh
dotnet run
# 得到结果
Inserting a new blog
Querying for a blog
Updating the blog and adding a post
Delete the blog
# 进入sqlite
sqlite3 blogging.db
```

> 输入sql查询发现啥都查不到，因为新追加的数据被`db.Remove(blog)`删除了
>
>  可以把代码中的Remove屏蔽掉在运行就能看到数据了

### 切换到SqlServer

```sh
dotnet add package Microsoft.EntityFrameworkCore.SqlServer
```

修改代码Model.cs（从UseSqlite替换成UseSqlServer）

```csharp
options.UseSqlServer("Server=(localdb)\\MSSQLLocalDB;Integrated Security=true;Initial Catalog=blogging;");
```

执行命令

```sh
# 首先删除之前生成的Sqlite的Migrations，删除该目录即可
# 然后创建Migrations，然后database update
dotnet ef migrations add InitialCreate
dotnet ef database update
```

### 切换到MySql

```sh
dotnet add package Pomelo.EntityFrameworkCore.MySql
rm Migrations -rf
dotnet ef migrations add InitialCreate
dotnet ef database update
dotnet run
# 查看结果
Inserting a new blog
Querying for a blog
Updating the blog and adding a post
Delete the blog
```

### 修改数据结构

在帖子总追加评论功能，追加comment

```csharp
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace EFGetStarted
{
    public class BloggingContext : DbContext
    {
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseMySql("Server=127.0.0.1;Database=blogging;Uid=root;Pwd=;charset=utf8;");
    }

    public class Blog
    {
        public int BlogId { get; set; }
        public string Url { get; set; }

        public List<Post> Posts { get; } = new List<Post>();
    }

    public class Post
    {
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }

        public int BlogId { get; set; }
        public Blog Blog { get; set; }
    }
    public class Comment
    {
        public int CommentId { get; set; }
        public string Content { get; set; }
        public int PostId { get; set; }
        public Post Post { get; set; }
    }
}
```

执行数据迁移命令
```sh
dotnet ef migrations add Comment
dotnet ef database update
```
查看数据库，发现Comment表已经被添加

![Comment表已经被添加](Entity Framework.assets/image-20191123123727761.png)

> :zap: 所有的migrations【 迁移 】最好都用ef来做，这样才确保没有问题

![](Entity Framework.assets/幻灯片2.png)


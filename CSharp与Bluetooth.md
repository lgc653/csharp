# C#与Bluetooth

本文主要内容是介绍操控蓝牙所需的支持库，以及从哪里下载最新版的支持库，并教你如何自学与蓝牙开发相关的更多知识等。本文涵盖了经典蓝牙(蓝牙2.0版本)和低功耗蓝牙(即BLE，蓝牙4.0版本)。

## 案例讲解

现在很多电脑提供了蓝牙支持，很多笔记本网卡也集成了蓝牙功能，也可以采用USB蓝牙方便的连接手机等蓝牙设备进行通信。

操作蓝牙要使用类库InTheHand.Net.Personal

![](CSharp与Bluetooth.assets/image-20210323210829480.png)

首先在项目中引用该类库。

可以先在项目根目录编辑packages.config

```xml
<?xml version="1.0" encoding="utf-8"?>
<packages>
  <package id="32feet.NET" version="3.5.0.0" targetFramework="net452" />
</packages>
```

然后在项目根目录运行

```sh
nuget restore

# 显示如下信息
MSBuild auto-detection: using msbuild version '16.9.0.11203' from 'C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Current\bin'.
Restoring NuGet package 32feet.NET.3.5.0.
Adding package '32feet.NET.3.5.0' to folder 'd:\百度云同步盘\Dev\lessons\CSharp工控\source\Bluetooth\packages'
Added package '32feet.NET.3.5.0' to folder 'd:\百度云同步盘\Dev\lessons\CSharp工控\source\Bluetooth\packages'

NuGet Config files used:
    C:\Users\lgc653\AppData\Roaming\NuGet\NuGet.Config
    C:\Program Files (x86)\NuGet\Config\Microsoft.VisualStudio.Offline.config

Feeds used:
    C:\Users\lgc653\.nuget\packages\
    https://api.nuget.org/v3/index.json
    C:\Program Files (x86)\Microsoft SDKs\NuGetPackages\

Installed:
    1 package(s) to packages.config projects
```

nuget可以在官网下载：https://www.nuget.org/downloads

![](CSharp与Bluetooth.assets/image-20210323213549018.png)

```c#
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InTheHand.Net.Bluetooth;

namespace Bluetooth
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            BluetoothRadio bluetoothRadio = BluetoothRadio.PrimaryRadio;
            if (bluetoothRadio == null)
            {
                Console.WriteLine("没有找到本机蓝牙设备!");
            }
            else
            {
                Console.WriteLine("ClassOfDevice: " + bluetoothRadio.ClassOfDevice);
                Console.WriteLine("HardwareStatus: " + bluetoothRadio.HardwareStatus);
                Console.WriteLine("HciRevision: " + bluetoothRadio.HciRevision);
                Console.WriteLine("HciVersion: " + bluetoothRadio.HciVersion);
                Console.WriteLine("LmpSubversion: " + bluetoothRadio.LmpSubversion);
                Console.WriteLine("LmpVersion: " + bluetoothRadio.LmpVersion);
                Console.WriteLine("LocalAddress: " + bluetoothRadio.LocalAddress);
                Console.WriteLine("Manufacturer: " + bluetoothRadio.Manufacturer);
                Console.WriteLine("Mode: " + bluetoothRadio.Mode);
                Console.WriteLine("Name: " + bluetoothRadio.Name);
                Console.WriteLine("Remote:" + bluetoothRadio.Remote);
                Console.WriteLine("SoftwareManufacturer: " + bluetoothRadio.SoftwareManufacturer);
                Console.WriteLine("StackFactory: " + bluetoothRadio.StackFactory);
            }

        }
    }
}
```

可以看到输出

```sh
32feet.NET: 'InTheHand.Net.Personal, Version=3.5.605.0, Culture=neutral, PublicKeyToken=ea38caa273134499'
   versions: '3.5.605.0' and '3.5.0605.0'.
ClassOfDevice: 2A010C
HardwareStatus: Running
HciRevision: 256
HciVersion: 8
LmpSubversion: 256
LmpVersion: 8
LocalAddress: D46D6DFC35C2
Manufacturer: Intel
Mode: Connectable
Name: DESKTOP-5QN4IAN
Remote:
SoftwareManufacturer: Microsoft
StackFactory: InTheHand.Net.Bluetooth.BluetoothPublicFactory
```

## WPF版本

WPF的特性之前已经介绍过，这里讲解一个WPF的蓝牙案例

![](CSharp与Bluetooth.assets/image-20210323210616379.png)

### 创建Device类

```c#
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InTheHand.Net.Sockets;

namespace BluetoothUP
{
    class Device
    {
        public string DeviceName { get; set; }
        public bool Authenticated { get; set; }
        public bool Connected { get; set; }
        public ushort Nap { get; set; }
        public uint Sap { get; set; }
        public DateTime LastSeen { get; set; }
        public DateTime LastUsed { get; set; }
        public bool Remembered { get; set; }

        public Device(BluetoothDeviceInfo device_info)
        {
            this.Authenticated = device_info.Authenticated;
            this.Connected = device_info.Connected;
            this.DeviceName = device_info.DeviceName;
            this.LastSeen = device_info.LastSeen;
            this.LastUsed = device_info.LastUsed;
            this.Nap = device_info.DeviceAddress.Nap;
            this.Sap = device_info.DeviceAddress.Sap;
            this.Remembered = device_info.Remembered;
        }

        public override string ToString()
        {
            return this.DeviceName;
        }
    }
}
```

### 主界面逻辑

#### 遍历蓝牙设备

创建后台任务BackgroundWorker，避免界面无响应

```c#
backgroundWorker = new BackgroundWorker();
backgroundWorker.DoWork += new DoWorkEventHandler(backgroundWorker_DoWork);
backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker_RunWorkerCompleted);
```

分别定义DoWork、RunWorkerCompleted事件，对蓝牙设备遍历，遍历成功后对界面进行赋值

```c#
void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
{
    listBoxBTDevices.ItemsSource = (List<String>)e.Result;
    Cursor = Cursors.Arrow;
}

void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
{

    List<String> devicesNames = new List<String>();
    BluetoothClient bc = new InTheHand.Net.Sockets.BluetoothClient();
    BluetoothDeviceInfo[] array = bc.DiscoverDevices(10);
    int count = array.Length;
    for (int i = 0; i < count; i++)
    {
        devices.Add(array[i]);
        devicesNames.Add(array[i].DeviceName);
    }
    e.Result = devicesNames;
}
```

#### 蓝牙设备配对

```c#
StringBuilder deviceInfo = new StringBuilder();
deviceInfo.Append(devices[listBoxBTDevices.SelectedIndex].DeviceName);
deviceInfo.Append("\n");
deviceInfo.Append("Class Of Device: ");
deviceInfo.Append(devices[listBoxBTDevices.SelectedIndex].ClassOfDevice);
deviceInfo.Append("\n");
deviceInfo.Append("Device Address: ");
deviceInfo.Append(devices[listBoxBTDevices.SelectedIndex].DeviceAddress);
deviceInfo.Append("\n");
deviceInfo.Append("Last seen: ");
deviceInfo.Append(devices[listBoxBTDevices.SelectedIndex].LastSeen);
deviceInfo.Append("\n");
deviceInfo.Append("Authenticated: ");
deviceInfo.Append(devices[listBoxBTDevices.SelectedIndex].Authenticated.ToString());
deviceInfo.Append("\n");
deviceInfo.Append("Connected: ");
deviceInfo.Append(devices[listBoxBTDevices.SelectedIndex].Connected.ToString());
textBlockDeviceInfo.Text = deviceInfo.ToString();
if (devices[listBoxBTDevices.SelectedIndex].Authenticated)
    buttonPairDevice.IsEnabled = false;
else
    buttonPairDevice.IsEnabled = true;

private void buttonPairDevice_Click(object sender, RoutedEventArgs e){
    BluetoothSecurity.PairRequest(devices[listBoxBTDevices.SelectedIndex].DeviceAddress, null);
}
```

#### 发送文件

```c#
if (textBoxFilePath.Text == "")
    MessageBox.Show("No path choosen");
else if(listBoxBTDevices.SelectedIndex == -1)
    MessageBox.Show("No device choosen");
else
{
    Cursor = Cursors.Wait;
    MessageBox.Show(sendFile(devices[listBoxBTDevices.SelectedIndex], textBoxFilePath.Text).ToString());
    Cursor = Cursors.Arrow;
}
```

### 运行结果

与手机配对，发送文件

![](CSharp与Bluetooth.assets/image-20210323220209282.png)

发送成功

![](CSharp与Bluetooth.assets/image-20210323220409162.png)
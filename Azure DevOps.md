![](Azure DevOps.assets/幻灯片0.png)

# Azure DevOps

## Azure DevOps的本质

可以理解成`JIRA + GitHub + Jenkins + Test Management + Nuget/Maven/NPM…`它是这大平台的一种合体产品，但又比这几款产品更为灵活和强大，你可以访问 https://azure.microsoft.com/zh-cn/services/devops/ 来进一步的了解。

## Azure DevOps简介

微软 Visual Studio Team Service 自 2005年开始以 Team Foundation Server 企业版本地部署的形态出现以来，已经经历了长达13年的演进，2013年微软正式开始运营此产品的SaaS版本，最早的名称叫做 Visual Studio Online，后改名为Visual Studio Team Services。这一路走来，VSTS产品经历了从企业盒装软件走向云，从集中大一统走向模块化，从只支持微软的技术展走向对开源的广泛支持。

现在，VSTS已经是现在企业级DevOps工具市场上毫无疑问最为强大的产品。今天，微软又将此产品改名为 Azure DevOps，并将其功能模块拆分为 Repo (代码托管）, Board (电子看板），Test Plan （测试管理）, Pipeline （流水线）和 Artifact (交付件管理）; 这个变化不仅仅意味着微软云为先战略在这款产品上的体现，也意味着开发者独立使用以上各个模块变得更加容易，更加轻量。这符合云时代DevOps产品的发展趋势，微软又一次在软件工程工具的发展上站在了最前列。 

DevOps对团队的成功变得越来越重要。 Azure DevOps 在提供支持软件开发团队的工具方面拥有超过15年的投资和经验积累。在过去的一个月中，有超过80,000名微软内部用户和成千上万的客户，无论是小型还是大型团队，都在使用这些服务向您交付产品。

Azure DevOps的服务涵盖了整个开发生命周期，可帮助开发人员更快地高质量地交付软件。它们代表了公有云中最完整的产品。Azure DevOps包括：

## Azure Pipelines 流水线

![Azure Pipelines 流水线](Azure DevOps.assets/screenshot-1574728612090.jpg)  

适用于任何语言，平台和云的CI/CD。能够连接到GitHub或任何Git存储库并持续部署。了解更多：

https://azure.microsoft.com/services/devops/pipelines/

## Azure Boards 电子看板

![Azure Boards 电子看板](Azure DevOps.assets/screenshot-1574728523190.jpg) 

使用看板，Backlogs，团队仪表板和自定义报告进行强大的工作跟踪。了解更多：

https://azure.microsoft.com/services/devops/boards/

## Azure Artifacts 交付件管理

![Azure Artifacts 交付件管理](Azure DevOps.assets/screenshot.png) 

来自公共源和私有源的Maven，npm和NuGet包。 了解更多：

https://azure.microsoft.com/services/devops/artifacts/

## Azure Repos 代码托管

![Azure Repos 代码托管](Azure DevOps.assets/screenshot.jpg)

为您的项目提供无限制的云托管私人Git仓库。协作拉取请求，高级文件管理等。了解更多：

https://azure.microsoft.com/services/devops/repos/

## Azure Test Plans 测试计划

 ![Azure Test Plans 测试计划](Azure DevOps.assets/screenshot-1574728656449.jpg) 

一体化计划和探索性测试解决方案。了解更多：

https://azure.microsoft.com/services/devops/test-plans/

## AzureDevOps服务都是开放且可扩展

每个AzureDevOps服务都是开放且可扩展的。它们都适用于任何类型的应用程序，您可以使用任何框架，平台或者云。您可以将它们作为完整的DevOps解决方案使用，或与其他服务一起使用。如果您要使用Azure Pipeline从GitHub中的存储库构建和测试Node服务并将其部署到AWS中的容器中，完全没有问题。 Azure DevOps支持公共云和私有云配置。您可以在我们的云中或您自己的数据中心中运行它们。无需购买不同的许可证。了解有关AzureDevOps定价的更多信息：

https://azure.microsoft.com/pricing/details/devops/azure-devops-services/


以下是单独使用AzurePipeline构建GitHub存储库的示例：

![单独使用AzurePipeline构建GitHub存储库](Azure DevOps.assets/640-1574606063534.jpg)

此外，这是一个开发人员利用AzureBoards的优势，使用所有AzureDevOps服务的示例：

![使用所有AzureDevOps服务的示例](Azure DevOps.assets/640-1574606051113.jpg)

## 开源项目通过AzurePipeline获得免费的CI/CD功能

微软承诺为所有开发人员提供开放灵活的工具，作为延伸，AzurePipelines为每个开源项目提供无限时和10个并行作业的免费CI/CD。借助云托管的Linux，macOS和Windows池，Azure Pipelines非常适合所有类型的项目。

许多顶级开源项目已经在使用AzurePipelines 作为CI /CD，例如Atom，CPython，Pipenv，Tox，Visual Studio Code和TypeScript - 这个列表每天都在增长。

Azure Pipelines 现在也可以在 GitHub Marketplace 中使用，可以轻松地为您的GitHub存储库，开源项目或其他方式进行设置。

以下是AzurePipeline的简介：https://azure.microsoft.com/en-us/blog/announcing-azure-pipelines-with-unlimited-ci-cd-minutes-for-open-source/

## VSTS的一些变化

Azure DevOps代表了Visual Studio Team Services（VSTS）的演变。 VSTS用户将自动升级到Azure DevOps项目。对于现有用户而言，功能上没有任何损失，提供了更多的选择和控制。作为VSTS标志的端到端可追溯性和集成仍然可以使用。 Azure DevOps服务可以很好地协同工作。今天就是转型的开始，在接下来的几个月里，现有用户将开始看到变化。这意味着什么？

- 网址将从 abc.visualstudio.com 更改为dev.azure.com/abc。我们将支持来自visualstudio.com 网址的重定向，因此不会出现错误的链接。
- 作为此更改的一部分，服务具有更新的用户体验。我们将继续根据预览的反馈迭代体验。今天开始我们默认为新用户启用它。在接下来的几个月中，我们将默认为现有用户启用它。
- 内部部署 Team Foundation Server（TFS）的用户将继续根据 Azure DevOps 中的实时功能接收更新。从下一版本的TFS开始，该产品将被称为Azure DevOps Server，并将通过我们正常的更新频率继续得到改进。

## Azure DevOps版本

Azure DevOps 有云版和本地版，云版叫 Azure DevOps Service；而本地版叫 Azure DevOps Server

* 云版：http://dev.azure.com，使用微软账号登录；
* 本地版需要下载：https://azure.microsoft.com/zh-cn/services/devops/server/，然后自己安装和托管。

云版和本地版有什么区别？

* 首先，云版只有英文语言，不知道以后会不会本地化。本地版可以选择好几种语言；
* 其次，云版本可以管理组织和用户，本地版使用的你安装的Windows系统的用户或者使用自己搭建的 AD 用户。
  某些插件只能给云版进行使用，不过大多数都能两者都支持。
* 云版5人以下是免费的，然后是按人头数按月收费，具体收费请参见：https://azure.microsoft.com/zh-cn/pricing/details/devops/azure-devops-services/；

## Azure DevOps 的文档
这么大的系统，当然必须有文档，只可惜有一点点难度，因为只有英文文档。

* 云版：https://docs.microsoft.com/en-us/azure/devops，
* 本地版：https://docs.microsoft.com/en-us/tfs/

## 国内竞品推荐

* https://www.huaweicloud.com/devcloud/ 
*  https://www.teambition.com/ 
*  https://gitee.com/ 

![](Azure DevOps.assets/幻灯片2.png)
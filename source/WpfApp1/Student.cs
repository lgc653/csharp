using System.ComponentModel;
namespace MyCoreWPF
{
  public class Student : INotifyPropertyChanged
  {
    public int Id { get; set; }

    private string name;

    public event PropertyChangedEventHandler PropertyChanged;

    public string Name
    {
      get { return name; }
      set
      {
        name = value;
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Name"));
      }
    }
    public int Age { get; set; }
  }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
	public partial class Form1 : Form
	{
		List<Product> prods = new List<Product> {
			new NoteBook(),
			new Pad(),
			new Watch(),
			new Mobile()
		};
		public Form1()
		{
			InitializeComponent();
			this.label1.Text = "还剩多少钱";

			foreach (Product prod in prods) {
				this.comboBox1.Items.Add(prod.Name);
			}
		}

		private void button1_Click(object sender, EventArgs e)
		{
			if (this.comboBox1.SelectedItem == null) {
				MessageBox.Show("商品类型必须输入");
			} else if (this.textBoxPrice.Text == "") {
				MessageBox.Show("商品价格必须输入");
			} else if (this.textBoxMoney.Text == "") {
				MessageBox.Show("手里的钱必须输入");
			} else {
				try {
					Buyer buyer = new Buyer(500);
					buyer.setMoney(int.Parse(this.textBoxMoney.Text));
					foreach (Product prod in prods) {
						if (this.comboBox1.SelectedItem.ToString() == prod.Name) {
							// 通过与comboBox中选中值进行匹配，获得当前对象prod
							prod.setPrice(int.Parse(this.textBoxPrice.Text));
							// 由于prod的不同，他的getPrice也是不同的
							this.label1.Text = buyer.buy(prod).ToString();
							break;
						}
					}                                   
				} catch {
					MessageBox.Show("出错了,请检测您输入的参数！");
				}
			}
		}
	}
}

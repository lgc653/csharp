﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class Buyer
    {
        int money = 600;

        public Buyer(int money)
        {
            this.money = money;
        }

        public void setMoney(int moneyIn)
        {
            money = money + moneyIn;
        }
        public int getMoney()
        {
            return money;
        }
        public int buy(Product prod)
        {
            return money - prod.getPrice();
        }
    }
}

﻿/*
 * 由SharpDevelop创建。
 * 用户： lgc653
 * 日期: 2021/3/27
 * 时间: 22:24
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
using System;

namespace WindowsFormsApp1
{
	/// <summary>
	/// Description of Product.
	/// </summary>
	public class Product
	{
        private string name;
        public int price;

		public virtual string Name
		{
			get
			{
				return name;
			}
			set
			{
				name = value;
			}
		}

		public Product()
		{
			
		}
		public virtual int getPrice()
		{
			return 0;
		}

		public void setPrice(int priceIn)
        {
			this.price = priceIn;

		}
	}
}

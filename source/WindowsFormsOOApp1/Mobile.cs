﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
	class Mobile : Product
    {
        private string name = "手机";
        public override string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        public override int getPrice()
        {
            // 手机缺货，报价翻倍
            return price * 2;
        }
    }
}

    

#define TESTCPPDLL_API __declspec(dllexport)

EXTERN_C TESTCPPDLL_API int __stdcall Add(int a, int b);

EXTERN_C TESTCPPDLL_API void __stdcall WriteString(wchar_t* content);

//传入一个整型指针，将其所指向的内容加1

EXTERN_C TESTCPPDLL_API void __stdcall AddInt(int* i);

//传入一个整型数组的指针以及数组长度，遍历每一个元素并且输出

EXTERN_C TESTCPPDLL_API void __stdcall AddIntArray(int* firstElement, int arraylength);

//在C++中生成一个整型数组，并且数组指针返回给C#

EXTERN_C TESTCPPDLL_API int* __stdcall GetArrayFromCPP();



//定义一个函数指针

typedef void(__stdcall* CPPCallback)(int tick);

//定义一个用于设置函数指针的方法,

//并在该函数中调用C#中传递过来的委托

EXTERN_C TESTCPPDLL_API void __stdcall SetCallback(CPPCallback callback);



struct Vector3

{

	float X, Y, Z;

};

EXTERN_C TESTCPPDLL_API void __stdcall SendStructFromCSToCPP(Vector3 vector);
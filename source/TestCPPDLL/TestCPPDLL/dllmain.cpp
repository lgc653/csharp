﻿// dllmain.cpp : 定义 DLL 应用程序的入口点。
#include "pch.h"

#include <iostream>

#include "TestCPPDLL.h"

using namespace std;

TESTCPPDLL_API int __stdcall Add(int a, int b)

{

	return a + b;

}

TESTCPPDLL_API void __stdcall WriteString(wchar_t* content)

{

	wprintf(content);

	printf("\n");

}



TESTCPPDLL_API void __stdcall AddInt(int* i)

{

	(*i)++;

}



TESTCPPDLL_API void __stdcall AddIntArray(int* firstElement, int arrayLength)

{

	int* currentPointer = firstElement;

	for (int i = 0; i < arrayLength; i++)

	{

		cout << *currentPointer;

		currentPointer++;

	}

	cout << endl;

}

int* arrPtr;

TESTCPPDLL_API int* __stdcall GetArrayFromCPP()

{

	arrPtr = new int[10];



	for (int i = 0; i < 10; i++)

	{

		arrPtr[i] = i;

	}



	return arrPtr;

}



TESTCPPDLL_API void __stdcall SetCallback(CPPCallback callback)

{

	int tick = 100;

	//下面的代码是对C#中委托进行调用

	callback(tick);

}



TESTCPPDLL_API void __stdcall SendStructFromCSToCPP(Vector3 vector)

{

	cout << "got vector3 in cpp,x:";

	cout << vector.X;

	cout << ",Y:";

	cout << vector.Y;

	cout << ",Z:";

	cout << vector.Z;

}
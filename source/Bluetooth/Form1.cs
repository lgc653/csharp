﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InTheHand.Net.Bluetooth;

namespace Bluetooth
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            BluetoothRadio bluetoothRadio = BluetoothRadio.PrimaryRadio;
            if (bluetoothRadio == null)
            {
                Console.WriteLine("没有找到本机蓝牙设备!");
            }
            else
            {
                Console.WriteLine("ClassOfDevice: " + bluetoothRadio.ClassOfDevice);
                Console.WriteLine("HardwareStatus: " + bluetoothRadio.HardwareStatus);
                Console.WriteLine("HciRevision: " + bluetoothRadio.HciRevision);
                Console.WriteLine("HciVersion: " + bluetoothRadio.HciVersion);
                Console.WriteLine("LmpSubversion: " + bluetoothRadio.LmpSubversion);
                Console.WriteLine("LmpVersion: " + bluetoothRadio.LmpVersion);
                Console.WriteLine("LocalAddress: " + bluetoothRadio.LocalAddress);
                Console.WriteLine("Manufacturer: " + bluetoothRadio.Manufacturer);
                Console.WriteLine("Mode: " + bluetoothRadio.Mode);
                Console.WriteLine("Name: " + bluetoothRadio.Name);
                Console.WriteLine("Remote:" + bluetoothRadio.Remote);
                Console.WriteLine("SoftwareManufacturer: " + bluetoothRadio.SoftwareManufacturer);
                Console.WriteLine("StackFactory: " + bluetoothRadio.StackFactory);
            }

        }
    }
}

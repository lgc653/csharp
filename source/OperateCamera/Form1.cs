﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing.Imaging;
using System.Text;
using System.Windows;
using System.Windows.Forms;
using System.IO;
using System.Windows.Media.Imaging;
using AForge;
using AForge.Controls;
using AForge.Video;
using AForge.Video.DirectShow;
using Size = System.Drawing.Size;
using System.Diagnostics;



namespace OperateCamera
{
	public partial class Form1 : Form
	{
		private FilterInfoCollection videoDevices;
		private VideoCaptureDevice videoSource;
		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
            
			try {
				// 枚举所有视频输入设备
				videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);

				if (videoDevices.Count == 0)
					throw new ApplicationException();

				foreach (FilterInfo device in videoDevices) {
					tscbxCameras.Items.Add(device.Name);
				}

				tscbxCameras.SelectedIndex = 0;

			} catch (ApplicationException) {
				tscbxCameras.Items.Add("No local capture devices");
				videoDevices = null;
			}
		}

		private void btnConnect_Click(object sender, EventArgs e)
		{
			CameraConn();
           
		}
		//连接摄像头
		private void CameraConn()
		{
			VideoCaptureDevice videoSource = new VideoCaptureDevice(videoDevices[tscbxCameras.SelectedIndex].MonikerString);
			videoSource.DesiredFrameSize = new System.Drawing.Size(320, 240);
			videoSource.DesiredFrameRate = 1;

			videoSourcePlayer.VideoSource = videoSource;
			videoSourcePlayer.Start();
		}

		//关闭摄像头
		private void btnClose_Click(object sender, EventArgs e)
		{
			videoSourcePlayer.SignalToStop();
			videoSourcePlayer.WaitForStop();
		}

		//主窗体关闭
		private void Form1_FormClosing(object sender, FormClosingEventArgs e)
		{
			btnClose_Click(null, null);
		}

		//拍照
		private void Photograph_Click(object sender, EventArgs e)
		{
			try {
				if (videoSourcePlayer.IsRunning) {
					BitmapSource bitmapSource = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
						                            videoSourcePlayer.GetCurrentVideoFrame().GetHbitmap(),
						                            IntPtr.Zero,
						                            Int32Rect.Empty,
						                            BitmapSizeOptions.FromEmptyOptions());
					PngBitmapEncoder pE = new PngBitmapEncoder();
					pE.Frames.Add(BitmapFrame.Create(bitmapSource));
					string picName = GetImagePath() + "\\" + "xiaosy" + ".jpg";
					if (File.Exists(picName)) {
						File.Delete(picName);
					}
					using (Stream stream = File.Create(picName)) {
						pE.Save(stream);
					}
					// 使用ffmpeg将图片压缩成webp格式
					string webpName = GetImagePath() + "\\" + "xiaosy" + ".webp";
					if (File.Exists(webpName)) {
						File.Delete(webpName);
					}
					RunCmd("c:\\Tools\\ffmpeg\\ffmpeg -i " + picName + " " + webpName);
					//拍照完成后关摄像头并刷新同时关窗体
					if (videoSourcePlayer != null && videoSourcePlayer.IsRunning) {
						videoSourcePlayer.SignalToStop();
						videoSourcePlayer.WaitForStop();
					}
                    
					this.Close();
				}
			} catch (Exception ex) {
				MessageBox.Show("摄像头异常：" + ex.Message);
			}
		}

        
		private string RunCmd(string command)
		{
			//例Process
			Process p = new Process();
			p.StartInfo.FileName = "cmd.exe";         //确定程序名
			p.StartInfo.Arguments = "/c " + command;   //确定程式命令行
			p.StartInfo.UseShellExecute = false;      //Shell的使用
			p.StartInfo.RedirectStandardInput = true;  //重定向输入
			p.StartInfo.RedirectStandardOutput = true; //重定向输出
			p.StartInfo.RedirectStandardError = true;  //重定向输出错误
			p.StartInfo.CreateNoWindow = true;        //设置置不显示示窗口
			p.Start();   
			return p.StandardOutput.ReadToEnd();      //输出出流取得命令行结果果
		}

		private string GetImagePath()
		{
			string personImgPath = Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory)
			                       + Path.DirectorySeparatorChar.ToString() + "PersonImg";
			if (!Directory.Exists(personImgPath)) {
				Directory.CreateDirectory(personImgPath);
			}

			return personImgPath;
		}
        
	}
}
